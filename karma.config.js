var webpackConfig = require('./webpack.config.js');

delete webpackConfig.entry;
webpackConfig.plugins.splice(1);
//For debugging only as too slow for tdd, see https://webpack.js.org/configuration/devtool/
//webpackConfig.devtool='inline-source-map';

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],

    reporters: ['progress'],
    port: 9876,
    colors: false,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless', 'FirefoxHeadless'],
    singleRun: false,
    autoWatchBatchDelay: 300,

    files: [
      './node_modules/jquery/dist/jquery.js',
      './node_modules/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './node_modules/babel-polyfill/dist/polyfill.min.js',
      './test/karma/jasminecustommatchers.js',
      './test/karma/mockobjectsbuilder.js',
      './app/assets/javascripts/xccti/js/cti.js',
      './app/assets/javascripts/xccti/js/callback.js',
      './app/assets/javascripts/xccti/js/membership.js',
      './public/javascripts/SIPml-api.js',
      './app/assets/javascripts/xccti/js/xc_webrtc.js',
      './test/karma/bootstrap.js',
      './app/assets/javascripts/ccagent/app.js',
      './app/assets/javascripts/ccmanager/app.js',
      './app/assets/javascripts/ucassistant/app.js',
      {pattern: './public/**/*', watched: false, served: true, included: false},
      './app/assets/javascripts/**/*.html',
      './test/karma/**/*.spec.js'
    ],

    proxies: {
      "/assets/audio/pratix/": "/base/public/audio/pratix/",
      "/assets/i18n/": "/base/public/i18n/",
      "/assets/javascripts/ccagent/": "/base/app/assets/javascripts/ccagent/",
      "/assets/images/": "/base/public/images/"
    },

    preprocessors: {
      './app/assets/javascripts/ccagent/app.js': ['webpack', 'sourcemap'],
      './app/assets/javascripts/ccmanager/app.js': ['webpack', 'sourcemap'],
      './app/assets/javascripts/ucassistant/app.js': ['webpack', 'sourcemap'],
      './test/karma/**/*.spec.js': ['webpack', 'sourcemap'],
      './app/assets/javascripts/**/*.html': ['ng-html2js']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      logLevel: 'error'
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'app/',
      moduleName: 'html-templates'
    }
  });
};
