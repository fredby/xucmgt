var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    agent: ['./app/assets/javascripts/ccagent/app.js'],
    ccmanager: './app/assets/javascripts/ccmanager/app.js',
    ucassistant: './app/assets/javascripts/ucassistant/app.js',
    xuc:  ['xccti/cti-webpack'],
    vendor: ['babel-polyfill']
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'target/web/public/main/javascripts/', 'dist')
  },
  resolve: {
    alias: {
      "ccagent": path.resolve(__dirname, "app/assets/javascripts/ccagent/"),
      "ccmanager": path.resolve(__dirname, "app/assets/javascripts/ccmanager/"),
      "ucassistant": path.resolve(__dirname, "app/assets/javascripts/ucassistant/"),
      "xccti": path.resolve(__dirname, "app/assets/javascripts/xccti/"),
      "xchelper": path.resolve(__dirname, "app/assets/javascripts/xchelper/"),
      "xclogin": path.resolve(__dirname, "app/assets/javascripts/xclogin/"),
      "jquery-ui": 'jquery-ui-dist/jquery-ui.js'
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      moment: 'moment',
      _: 'lodash',
      "window.jQuery":"jquery",
      "window._": "lodash"
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module) {
        return module.context && module.context.indexOf('node_modules') !== -1;
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest'
    }),
    new webpack.optimize.UglifyJsPlugin({
      include: /vendor\.js/
    }),
  ],
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.js$/,
        include: [/ccagent/, /-webpack.js/, /xccti/, /ccmanager/, /ucassistant/, /xchelper/],
        exclude: [/node_modules/, /cti.js/],
        loader: "eslint-loader"
      },
      {
        test: require.resolve('jquery'),
        loader: 'expose-loader?jQuery!expose-loader?$'
      },
      {
        test: require.resolve('angular'),
        loader: 'expose-loader?angular!expose-loader?angular'
      },
      {
        test: /\.js$/,
        include: [/ccagent/, /-webpack.js/, /xccti/, /ccmanager/, /ucassistant/, /xchelper/],
        exclude: [/node_modules/, /cti.js/],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015'],
            cacheDirectory: true
          }
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      }
    ]
  }
};
