#!/usr/bin/env bash
set -e

rm -rf updates
cd electron
rm -rf node_modules dist
if [ ! -z $TARGET_VERSION_SEMVER ]; then
    set +e
    npm --no-git-tag-version -f version $TARGET_VERSION_SEMVER >/dev/null 2>&1
    set -e
fi
npm install
npm run test:all
# Building windows installer (must be run first as it cleans dist folder)"
docker run --rm -i -v ${PWD}:/project -v ${PWD##*/}-node-modules:/project/node_modules -v ~/.electron:/root/.electron electronuserland/electron-builder:wine /bin/bash -c "npm install && npm run dist-win64 && chmod -R 777 dist"
# Build debian package
npm run dist-linux64
# Prepare desktop files distribution
cd ..
mkdir -p updates
cp -aR electron/dist/squirrel-windows updates/win64
# Generate repropro requirements
mkdir -p updates/debian/conf
cat > updates/debian/conf/distributions << EOF
Origin: Xivo.solutions
Label: Xivo.solutions
Suite: stable
Codename: jessie
Architectures: amd64 i386
Components: contrib
Description: XivoCC local repo
EOF
mkdir -p updates/debian/incoming
cp -a electron/dist/*.deb updates/debian/incoming
cd updates/debian/
reprepro -Vb . includedeb jessie incoming/*.deb
cd ../..

# Clean incoming (for space usage sake in docker image)
rm updates/debian/incoming/*.deb

sbt clean test docker:publishLocal

