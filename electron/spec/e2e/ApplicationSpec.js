const Application = require('spectron').Application
const tmp = require('tmp')

tmp.setGracefulCleanup()

const tmpdir = tmp.dirSync()

describe("desktop application", () => {

  jasmine.default_timeout_interval = 10000;

  let app

  const before = (async () => {
    app = new Application({ path: './node_modules/electron/dist/electron', args: ['.'],
      env: {"CUSTOM_USER_DATA": tmpdir.name} })
    await app.start()
    await app.client.waitUntilWindowLoaded()
  })
  const after = () => {
    (app && app.isRunning()) ? app.mainProcess.exit(0) : null
  }

  beforeAll(before)
  afterAll(after)

  it('opens the window', () => app.client.waitUntilWindowLoaded())

  it('loads index page', () => app.client.waitUntilWindowLoaded()
    .then(() => app.webContents.getURL())
    .then(url => expect(url.slice(-2)).toBe('#/')))

  it('check for new version available', (done) => {
    app.client.waitUntilWindowLoaded()
    .then(() => {
      app.electron.ipcRenderer.sendSync('setUpdateUrl', 'localhost', false).then((result) => {
        expect(result).toBe('localhost/updates/win64')
        done()
      })
    })
  })
});
 
