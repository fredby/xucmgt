"use strict";

require("js/angular-app.js");


describe("MainController", function() {
  var $scope;

  beforeEach(() => {
    ngModule("desktopApp");
  });
  
  var $controller;
  beforeEach(inject(function(_$controller_, $injector, _updateService_, _$rootScope_) {
    $controller = _$controller_;
    $scope = _$rootScope_.$new();
    spyOn(_updateService_, 'setUpdateShortcut');
    spyOn(_updateService_, 'setUpdateCloseTray');
    spyOn(_updateService_, 'setUpdateStartUp');
  }));

  beforeEach(inject(function() {
    var html = '<webview src="localhost"></webview>';
    angular.element(document.body).append(html);
  }));

  it("can be started", inject(function() {
    $scope = {};
    var ctrl = $controller("MainController", {'$scope':$scope});
    expect(ctrl).toBeDefined();
  }));

  it("redirect to settings page if remote url is not set", inject(function(_desktopSettings_, _$state_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.returnValue(null);
    spyOn(_$state_, "go");
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_, $state: _$state_});
    expect(_$state_.go).toHaveBeenCalledWith('main.settings');
  }));

  it("set url if remote url is set", inject(function(_desktopSettings_, _updateService_) {
    var $scope = {};
    spyOn(_updateService_, 'setUpdateUrl');
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
      case 'APP_PROTOCOL': return 'http';
      case 'APP_DOMAIN': return 'go.to.somewhere';
      }
      return null;
    });
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    expect($scope.url).toBe('http://go.to.somewhere');
    expect(_updateService_.setUpdateUrl).toHaveBeenCalledWith($scope.url, false);
  }));

  it("set shortcut to default if not defined", inject(function(_desktopSettings_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return null;
      }
      return null;
    });
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect(_updateService_.setUpdateShortcut).toHaveBeenCalledWith(_desktopSettings_.DEFAULT_SHORTCUT);
  }));

  it("set shortcut to what have been saved", inject(function(_desktopSettings_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return "Ctrl-X";
      }
      return null;
    });
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect(_updateService_.setUpdateShortcut).toHaveBeenCalledWith("Ctrl-X");
  }));

  it("set shortcut to null if disabled", inject(function(_desktopSettings_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return "null";
      }
      return null;
    });
    var ctrl = $controller("MainController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect(_updateService_.setUpdateShortcut).toHaveBeenCalledWith('null');
  }));

  it("Error should be throwed webview failed to load", inject((_updateService_, _$state_) => {
    const $scope = {};
    spyOn(_updateService_, "setError");
    spyOn(_updateService_, "throwUrlError");
    $controller("MainController", {$scope:$scope});
    $scope.throwWebviewError();
    expect(_updateService_.setError).toHaveBeenCalled();
  }));

  it("Error should be cleaned after webview success to load", inject((_updateService_, _$state_) => {
    const $scope = {};
    spyOn(_updateService_, "clearError");
    $controller("MainController", {$scope:$scope});
    $scope.successWebviewLoad();
    expect(_updateService_.clearError).toHaveBeenCalled();
  }));
}); 
