"use strict";

const jsdom = require('jsdom').jsdom;

var document = global.document = jsdom('<html><head></head><body></body></html>');
var window = global.window = global.document.defaultView;
global.navigator = window.navigator = {};
global.Node = window.Node;

global.window.jasmine = {};
global.window.beforeEach = global.beforeEach
global.window.afterEach = global.afterEach

global.$ = global.jQuery = require('jquery')
require('angular/angular')
global.angular = window.angular;
require('angular-mocks/angular-mocks')

global.inject = global.angular.mock.inject;
global.ngModule =  global.angular.mock.module;


class MockStorage {
  constructor() {
    this._storage = {};
  }

  getItem(key) {
    if(typeof(this._storage[key]) !== "undefined") {
      return this._storage[key];
    } else return null
  }

  setItem(key, value) {
    this._storage[key] = value;
  }

  clear() {
    this._storage = {};
  }
}

global.localStorage = window.localStorage = window.sessionStorage = new MockStorage();
global.window.require = () => {
  return {
    ipcRenderer: {
      on: () => {}
    }
  }
};
