# Electron Application

# Prerequisites #

* npm 
  * sudo apt-get install nodejs npm
  * alternate solutions : https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
* Tools for packaging
  * sudo apt-get install --no-install-recommends -y icnsutils graphicsmagick xz-utils reprepro
* wine (to build windows version)
  * sudo add-apt-repository  ppa:ubuntu-wine/ppa -y
  * sudo apt-get update
  * sudo apt-get install --no-install-recommends -y wine1.8
* mono (to build windows version)
  * sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
  * echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list
  * sudo apt-get update
  * sudo apt-get install --no-install-recommends -y mono-devel ca-certificates-mono

## Development

First you need to initialize dependencies: `npm install`

* Start a development version: `npm start`, if you need some args you must set them in the scripts/start in the package.json
* Run unit tests: `npm test`
* Run integration tests: `npm run test:e2e`
* Run all tests (unit & integration): `npm run test:all`

## Building

* To build a linux version (w/o package): `npm run pack` (not required to build installer package but could be useful to debug packing problems)
* Still for the Linux version, you need to do `apt install libgnome-keyring-dev icnsutils graphicsmagick xz-utils rpm bsdtar`
* To build a debian installer package: `npm run dist-linux64`
* To build a windows installer package: `npm run dist-win64`
* Alternate build windows within docker: `docker run --rm -i -v ${PWD}:/project -v ${PWD##*/}-node-modules:/project/node_modules -v ~/.electron:/root/.electron electronuserland/electron-builder:wine /bin/bash -c "npm install && npm run dist-win64 && chmod -R 777 dist"`

### Auto-update

## Install & Update

### Windows 

Install from a running xucmgt instance by opening the following url: http://xucmgt-host:port/install/win64 where `xucmgt-host` is the host name or ip address of the xuc and `port` is the port it's available on.

### Debian

Add the following line to your /etc/apt/sources.list file :
```
deb http://xucmgt-host:port/updates/debian jessie contrib
```
where `xucmgt-host` is the host name or ip address of the xuc and `port` is the port it's available on.

### Update

Update mechanism is automatic. On debian, the update rely on the apt update process. On windows, when the application starts, it should check if a new version is available and offer to update if one is available.

## Options

Currently only two startup switches are supported:
-d - enable debug tools
--ignore-certificate-errors - activate internal chrome option allowing use with invalid (autosigned) certificates

