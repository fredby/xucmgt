var i18n = require('i18n');

i18n.configure({
  locales:['en', 'fr'],
  directory: __dirname + '/../locales',

  logWarnFn: function (msg) {
    console.log('warn', msg);
  },

  logErrorFn: function (msg) {
    console.log('error', msg);
  }
});

i18n.setLocale('fr');

module.exports = i18n;
