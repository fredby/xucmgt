const ELECTRON_DEFAULT_WIDTH = 360;
const ELECTRON_DEFAULT_HEIGHT = 824;
const ELECTRON_TITLE_BAR_HEIGHT = 24;

module.exports = {
  ELECTRON_DEFAULT_WIDTH,
  ELECTRON_DEFAULT_HEIGHT,
  ELECTRON_TITLE_BAR_HEIGHT
}

const { ipcRenderer } = require('electron')

global.setElectronConfig = (data) => {
  ipcRenderer.sendToHost(data)
}