if (require('electron-squirrel-startup')) return;

const {app, BrowserWindow, Menu, Tray, autoUpdater, globalShortcut, clipboard, nativeImage} = require('electron');
const AutoLaunch = require('auto-launch-patched');
const openAboutWindow = require('about-window').default;
const path = require('path');
const url = require('url');
const isDev = require('electron-is-dev');
const os = require('os');
const {ipcMain, shell} = require('electron');
const _ = require('lodash');
const PositionMgt = require('./position-mgt');
const Registry = require('rage-edit').Registry;

const i18n = require('./translate.js');
const {ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT, ELECTRON_TITLE_BAR_HEIGHT} = require('./preload-webview.js');

const commandLineArgs = require('command-line-args');
const optionDefinitions = [
  {name: 'debug', alias: 'd', type: Boolean, defaultValue: false},
  {name: 'ignore-certificate-errors', type: Boolean, defaultValue: false}
];

const acceptedProtocols = ["tel", "callto"];
const protocolSep = ":";
const appName = app.getName();

let options = {debug: false};
let quit = false;
let closeInTray = false;
let launch;
let webview;

const phoneEvent = 'PHONE_EVENT';

class Message {
  constructor(type, value) {
    this.type = type;
    this.value = value;
  }

  toJSON() {
    return {
      type: this.type,
      value: this.value
    };
  }
}

function setArgs() {
  try {
    options = commandLineArgs(optionDefinitions, {'argv': process.argv});
    console.log('Starting with options: ', options, ' on :', process.argv);
    if (options.ignoreCertificateErrors) {
      app.commandLine.appendSwitch('ignore-certificate-errors');
    }
  }
  catch (err) {
    console.log('Unable to parse arguments, ignoring all.');
    console.log('Details: ', err);
  }

  if (typeof(process.env.CUSTOM_USER_DATA) !== "undefined") {
    app.setPath('userData', process.env.CUSTOM_USER_DATA);
  }
}

function getApplicationUrl(view) {
  return url.format({
    pathname: path.join(__dirname, '/../index.html'),
    protocol: 'file:',
    slashes: true,
    hash: view
  });
}

function setGlobalShortcut(browserWindow, keyCombination) {
  globalShortcut.register(keyCombination, () => {
    let msg = clipboard.readText('selection');
    console.log('received global key with content', msg);
    forwardMessage(browserWindow, msg, phoneEvent);
  });
}

function forwardMessage(browserWindow, msg, type) {
  try {
    if (msg) {
      let data = JSON.stringify(new Message(type, msg));
      win.webContents.send('msg', data);
    }
  }
  catch (exception) {
    console.log(exception);
  }
}

function extractMessageFromURL(data) {
  if (_.find(acceptedProtocols, function (p) {
    return _.startsWith(data, p + protocolSep);
  })) {
    return _.split(data, protocolSep, 2)[1];
  }
}

function forceQuit() {
  console.log("Closing app...");
  quit = true;
  // Unregister all shortcuts.
  globalShortcut.unregisterAll();
  app.quit();
}

function setFocus(browserWindow) {
  if (browserWindow) {
    browserWindow.restore();
    browserWindow.show();
    browserWindow.focus();
  }
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;
let tray;
let positionMgt;

function createWindow() {

  // Create a default browser window.
  win = new BrowserWindow({
    width: ELECTRON_DEFAULT_WIDTH,
    height: ELECTRON_DEFAULT_HEIGHT,
    frame: false,
    backgroundColor: '#eeeeee',
    show: false,
    useContentSize: true,
    webPreferences: {experimentalFeatures: true}
  });

  let trayImage = path.join(__dirname, '/../img/xivo_headset');
  trayImage += (os.platform() === 'win32') ? '.ico' : '.png';
  tray = new Tray(nativeImage.createFromPath(trayImage));
  const contextMenu = Menu.buildFromTemplate([
    {
      label: i18n.__('MENU_NAVIGATION_SHOW'),
      click() {
        setFocus(win);
      }
    },
    {
      label: i18n.__('MENU_NAVIGATION_SETTINGS'),
      click() {
        win.webContents.send('sref', 'main.settings');
        setFocus(win);
      }
    },
    {
      type: 'separator'
    },
    {
      label: i18n.__('MENU_NAVIGATION_QUIT'),
      click() {
        forceQuit();
      }
    }]);
  tray.setToolTip(i18n.__('APP_NAME'));
  tray.setContextMenu(contextMenu);

  launch = new AutoLaunch({
    name: app.getName()
  });


  // and load the index.html of the app.
  win.loadURL(getApplicationUrl('/'));

  const debugMenu = [
    {
      label: i18n.__('MENU_DEBUG'),
      submenu: [
        {
          label: i18n.__('MENU_DEBUG_RELOAD'),
          accelerator: 'CmdOrCtrl+R',
          click() {
            win.reload();
          }
        },
        {
          label: i18n.__('MENU_DEBUG_DEVTOOL'),
          click() {
            win.webContents.toggleDevTools();
          }
        },
        {
          label: i18n.__('MENU_DEBUG_DEVTOOL_WEBVIEW'),
          click() {
            if (webview) {
              webview.toggleDevTools();
            }
          }
        }
      ]
    }
  ];

  const defaultMenu = [
    {
      label: i18n.__('MENU_NAVIGATION'),
      submenu: [
        {
          label: i18n.__('MENU_NAVIGATION_APPLICATION'),
          click() {
            win.webContents.send('sref', 'main');
          }
        },
        {
          label: i18n.__('MENU_NAVIGATION_SETTINGS'),
          click() {
            win.webContents.send('sref', 'main.settings');
          }
        },
        {
          type: 'separator'
        },
        {
          label: i18n.__('MENU_NAVIGATION_QUIT'),
          click() {
            forceQuit();
          }
        }
      ]
    }
  ];

  const aboutMenu = [{
    label: i18n.__('MENU_ABOUT'),
    submenu: [{
      label: i18n.__('MENU_NAVIGATION_ABOUT'),
      click() {
        openAboutWindow({
          icon_path: path.join(__dirname, '/../img/xivo_headset.png'),
          copyright: i18n.__('LICENSING')
        });
      }
    }]
  }];

  let menu = null;
  if (isDev || options.debug) {
    menu = Menu.buildFromTemplate(_.concat(defaultMenu, debugMenu, aboutMenu));
  } else {
    menu = Menu.buildFromTemplate(_.concat(defaultMenu, aboutMenu));
  }
  win.webContents.on('context-menu', function (e, params) {
    menu.popup(win, {'x': params.x, 'y': params.y});
  });
  win.setResizable(false);
  win.setMaximizable(false);

  positionMgt = new PositionMgt(win);

  const firstInstance = app.requestSingleInstanceLock();
  app.on('second-instance', function (event, commandLine, workingDirectory) {
    setFocus(win);
    if (commandLine[1]) {
      console.log('received URL to open', commandLine[1]);
      forwardMessage(win, extractMessageFromURL(commandLine[1]), phoneEvent);
    }
  });
  if (!firstInstance) {
    forceQuit();
  }

  tray.on('click', () => {
    win.isVisible() ? win.hide() : win.show();
  });

  win.once('ready-to-show', () => {
    win.show();
  });

  win.on('new-window', (event, url) => {
    event.preventDefault();
    const popup = new BrowserWindow({show: false});
    popup.once('ready-to-show', () => popup.show());
    popup.loadURL(url);
    event.newGuest = popup;
  });
}

// register protocols
if(process.platform === 'win32') {
  (async () => {
    await Registry.set('HKCU\\Software\\'+appName+'\\Capabilities', 'ApplicationName', appName);
    await Registry.set('HKCU\\Software\\'+appName+'\\Capabilities', 'ApplicationDescription', appName);

    for (p of acceptedProtocols) {
      await Registry.set('HKCU\\Software\\'+appName+'\\Capabilities\\URLAssociations', p, appName+'.'+p);
      await Registry.set('HKCU\\Software\\Classes\\'+appName+'.'+p+'\\DefaultIcon', '', process.execPath);
      await Registry.set('HKCU\\Software\\Classes\\'+appName+'.'+p+'\\shell\\open\\command', '', `"${process.execPath}" "%1"`);
    }

    await Registry.set('HKCU\\Software\\RegisteredApplications', appName, 'Software\\'+appName+'\\Capabilities');
  })();
} else {
  acceptedProtocols.forEach((p) => {
    app.setAsDefaultProtocolClient(p);
  });
}

// fix tray win32 notifications
app.setAppUserModelId('com.squirrel.' + appName + '.' + appName);

// Set electron arguments according to the cli args
setArgs();

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  forceQuit();
});

//Handle popup in webview to open in default browser
app.on('web-contents-created', function (e, contents) {
  if (contents.getType() === 'webview') {
    webview = contents;
    webview.on('new-window', function (e, url) {
      shell.openExternal(url);
      e.preventDefault();
    });
  }
});

ipcMain.on('setUpdateUrl', (event, url, reload) => {
  const os = require('os');
  const feedURL = url + '/updates/win' + (os.arch() === 'x64' ? '64' : '32');
  console.log("setUpdateUrl with reload", feedURL, reload);
  event.returnValue = feedURL;
  autoUpdater.setFeedURL(feedURL);
  autoUpdater.checkForUpdates();
  if (reload) {
    win.loadURL(getApplicationUrl('/'));
  }
});

ipcMain.on('urlError', (event, arg) => {
  console.log("Fail to load URL", arg);
  win.setContentSize(ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT);
  win.loadURL(getApplicationUrl('/loadingerror'));
});

ipcMain.on('setUpdateGlobalShortcut', (event, arg) => {
  globalShortcut.unregisterAll();
  if (arg != null) {
    setGlobalShortcut(win, arg);
  }
});

ipcMain.on('setUpdateCloseTray', (event, arg) => {
  closeInTray = arg;
});

ipcMain.on('setUpdateStartUp', (event, arg) => {
  launch.isEnabled()
    .then(function (isEnabled) {
      if (arg !== isEnabled) {
        (arg) ? launch.enable() : launch.disable();
      }
    })
    .catch(function (err) {
      console.log(err);
    });
});

ipcMain.on('updateBrowserWindowSize', (event, arg) => {
  if (arg.h) {
    arg.h += ELECTRON_TITLE_BAR_HEIGHT;
  }
  let align = positionMgt.getAlign();
  win.setContentSize(arg.w, arg.h);
  positionMgt.setAlign(align);
  positionMgt.repositioning();
  win.setMinimizable(!arg.mini);
  win.setAlwaysOnTop(arg.mini);
});

ipcMain.on('close', (event, arg) => {
  if (closeInTray && !quit) {
    event.preventDefault();
    win.hide();
  } else {
    forceQuit();
  }
});

ipcMain.on('minimize', () => {
  win.minimize();
});

ipcMain.on('config', () => {
  win.webContents.send('sref', 'main.settings');
});

ipcMain.on('focusBrowserWindow', () => {
  setFocus(win);
});

ipcMain.on('quitAndInstall', () => {
  quit = true;
  autoUpdater.quitAndInstall();
});

autoUpdater.addListener("update-available", function () {
  console.log("update-available");
});

autoUpdater.addListener("update-downloaded", function () {
  console.log("update-downloaded");
  win.setContentSize(ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT);
  win.webContents.send('sref', 'main.updateavailable');
});

autoUpdater.addListener("error", function (error) {
  console.log("update-error: " + error);
});

autoUpdater.addListener("checking-for-update", function () {
  console.log("update-checking");
});

autoUpdater.addListener("update-not-available", function () {
  console.log("update-not-available");
});
