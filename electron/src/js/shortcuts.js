const COMMON_KEYS = [
  '0','1','2','3','4','5','6','7','8','9',
  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
  'F1','F2','F3','F4','F5','F6','F7','F8','F9','F10','F11','F12',
  '!','$','%','^','&','*','(',')',':','<','_','>','?','{','|','}','"',';','=',',','-','.','/','~','@','#',
];

const LOCALIZED_KEYS = [
  'Space','Tab','Backspace','Delete','Insert','Enter','Up','Down','Left','Right','Home','End','PageUp','PageDown','Esc'
];


const LOCALIZED_MODIFIER_KEYS = [
  'Cmd','Ctrl','CmdOrCtrl','Alt','Option','AltGr','Shift','Super'
];

module.exports = {
  COMMON_KEYS,
  LOCALIZED_KEYS,
  LOCALIZED_MODIFIER_KEYS
}