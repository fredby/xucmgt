const DEFAULT_TOLERANCE = 10;
const electron = require('electron');
const os = require('os');
let moving = null;

module.exports = class PositionMgt {

  constructor(win) {
    this.win = win;
    if(os.platform() == 'win32'){
      this.win.on('move', this.onMove.bind(this));
    }
    else{
      this.win.on('moved', this.onMove.bind(this));
    }
  }

  //Simulate moved event in win32
  onMove(){
    if(moving){
      clearTimeout(moving);
    }
    moving = setTimeout(this.onMoved.bind(this),200);
  }

  onMoved(){
    this.repositioning();
  }

  getAlign() {
    let winBox = new Box(this.win.getBounds());
    //Get the display in which the window is
    let display = this.getDisplay();
    let displayBox = new Box(display.bounds);
    let align = undefined;
    let verticalAlign = undefined;
    if (displayBox.getLeft() === winBox.getLeft()) {
      align = 'left';
    }
    else if (displayBox.getRight() === winBox.getRight()) {
      align = 'right';
    }
    if (displayBox.getTop() === winBox.getTop()) {
      verticalAlign = 'top';
    }
    else if (displayBox.getBottom() === winBox.getBottom()) {
      verticalAlign = 'bottom';
    }
    return new Align(display.id, align, verticalAlign);
  }


  getDisplay(id = undefined) {
    if (id === undefined) {
      return electron.screen.getDisplayMatching(this.win.getBounds());
    }
    else {
      let displays = electron.screen.getAllDisplays();
      for (let d in displays) {
        if (displays[d].id === id) {
          return displays[d];
        }
      }
    }
    return undefined;
  }

  setAlign(align) {
    let display = this.getDisplay(align.displayId);
    let displayBox = new Box(display.bounds);
    let winBox = new Box(this.win.getBounds());

    if(display){
      if(align.align){
          if(align.align === "left"){
            winBox.setLeft(displayBox.getLeft());
          }
          else if(align.align === "right"){
            winBox.setRight(displayBox.getRight());
          }
      }
      if(align.verticalAlign){
        if(align.verticalAlign === "top"){
          winBox.setTop(displayBox.getTop());
        }
        else if(align.verticalAlign === "bottom"){
          winBox.setBottom(displayBox.getBottom());
        }
      }
      this.win.setBounds(winBox.rect);
    }
  }

  repositioning(tolerance = DEFAULT_TOLERANCE) {
    let winBox = new Box(this.win.getBounds());

    //Get the display in which the window is
    let display = this.getDisplay();
    if(!display){
      return;
    }
    let displayBox = new Box(display.bounds);

    //If box is not on display or near the border, position it just on the border
    if (winBox.getLeft() - displayBox.getLeft() < tolerance) {
      winBox.setLeft(displayBox.getLeft());
    }
    else if (displayBox.getRight() - winBox.getRight() < tolerance) {
      winBox.setRight(displayBox.getRight());
    }

    if (winBox.getTop() - displayBox.getTop() < tolerance) {
      winBox.setTop(displayBox.getTop());
    }
    else if (displayBox.getBottom() - winBox.getBottom() < tolerance) {
      winBox.setBottom(displayBox.getBottom());
    }
    this.win.setBounds(winBox.rect);
  }
}


class Align {
  constructor(displayId, align, verticalAlign) {
    this.displayId = displayId;
    this.align = align;
    this.verticalAlign = verticalAlign;
  }
}

class Box {

  constructor(rect) {
    this.rect = rect;
  }

  getLeft() {
    return this.rect.x;
  }

  setLeft(val) {
    this.rect.x = val;
  }

  getRight() {
    return this.rect.x + this.rect.width;
  }

  setRight(val) {
    return this.rect.x = val - this.rect.width;
  }

  getTop() {
    return this.rect.y;
  }

  setTop(val) {
    return this.rect.y = val;
  }

  getBottom() {
    return this.rect.y + this.rect.height;
  }

  setBottom(val) {
    return this.rect.y = val - this.rect.height;
  }
}