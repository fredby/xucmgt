"use strict";

window.$ = window.jQuery = require('jquery');
require('bootstrap');
require('angular');
require('angular-translate');
require('angular-ui-router');
require('ui-select');
require('module').globalPaths.push(__dirname);
const ipcRenderer = window.require('electron').ipcRenderer; // to be able to mock it (see angular-helper)
const isAccelerator = require("electron-is-accelerator");
const _ = require('lodash');
const shortcuts = require('js/shortcuts');

var i18n = require('js/translate.js');

var desktopApp = angular.module('desktopApp', ['pascalprecht.translate', 'ui.router','ui.select'])
    .config(function($translateProvider) {
      $translateProvider.useMissingTranslationHandler('i18nProxy');
      $translateProvider.determinePreferredLanguage();
    })
    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('main', {
          url: '/',
          templateUrl: 'view/main.tpl.html',
          controller: 'MainController'
        })
        .state('main.settings', {
          templateUrl: 'view/main.settings.tpl.html',
          controller: 'SettingsController'
        })
        .state('main.updateavailable', {
          templateUrl: 'view/main.updateavailable.tpl.html',
          controller: 'UpdateAvailableController'
        })
        .state('loadingerror', {
          url: '/loadingerror',
          templateUrl: 'view/loadingerror.tpl.html',
          controller: 'LoadingErrorController'
        });

      $urlRouterProvider.otherwise('/');
    }).run(function($rootScope) {
      $rootScope.title = i18n.__('APP_NAME');
    });


desktopApp.factory('i18nProxy', function () {
  return function (translationId, $uses, interpolateParams, defaultTranslationText, sanitizeStrategy) {
    return i18n.__(translationId, interpolateParams);
  };
});

desktopApp.factory('desktopSettings', function () {

  const _KEY_APP_PROTOCOL = 'APP_PROTOCOL';
  const _KEY_APP_DOMAIN = 'APP_DOMAIN';
  const _KEY_APP_SHORTCUT = 'APP_SHORTCUT';
  const _KEY_APP_CLOSE = 'APP_CLOSE';
  const _KEY_APP_STARTUP = 'APP_STARTUP';

  const _DEFAULT_SHORTCUT = "CmdOrCtrl+Space";

  const _get = function(key) {
    return localStorage.getItem(key);
  };

  const _set = function(key, value) {
    return localStorage.setItem(key, value);
  };
  
  return {
    KEY_APP_PROTOCOL: _KEY_APP_PROTOCOL,
    KEY_APP_DOMAIN: _KEY_APP_DOMAIN,
    KEY_APP_SHORTCUT: _KEY_APP_SHORTCUT,
    KEY_APP_CLOSE: _KEY_APP_CLOSE,
    KEY_APP_STARTUP: _KEY_APP_STARTUP,
    DEFAULT_SHORTCUT: _DEFAULT_SHORTCUT,
    get: _get,
    set: _set
  };
});

desktopApp.factory('updateService',  ($rootScope) => {
  var error = false;
  
  const _setUpdateUrl = (url, reload) => {
    ipcRenderer.send('setUpdateUrl', url, reload);
  };

  const _setUpdateShortcut = (keyCombination) => {
    ipcRenderer.send('setUpdateGlobalShortcut', keyCombination != 'null' ? keyCombination : null);
  };

  const _setUpdateCloseTray = (isTray) => {
    ipcRenderer.send('setUpdateCloseTray', isTray);
  };

  const _setUpdateStartUp = (isBoot) => {
    ipcRenderer.send('setUpdateStartUp', isBoot);
  };

  const _throwUrlError = (url) => {
    ipcRenderer.send('urlError', url);
  };

  const _setError = () => {
    error = true;
  };

  const _hasError = () => {
    return error;
  };

  const _clearError = () => {
    error = false;
  };

  const _getUpdateShortcut = (shortcut) => {
    return _.join(_.map(shortcut, (key) => { return key.id; }), '+');
  };

  const _displayShortcut = (shortcut) => {
    return _.join(_.map(shortcut, (key) => { return key.label; }), ' ');
  };

  const _buildKeyList = () => {
    var localizedModifiers = _.map(shortcuts.LOCALIZED_MODIFIER_KEYS, (value) => {
      return { id: value, label: i18n.__('SETTINGS_SHORTCUT_KEY_'+value) + " +" };
    });
    var localizedKeys = _.map(shortcuts.LOCALIZED_KEYS, (value) => {
      return { id: value, label: i18n.__('SETTINGS_SHORTCUT_KEY_'+value) };
    });
    var commonKeys = _.map(shortcuts.COMMON_KEYS, (value) => {
      return { id: value, label: value };
    });

    return _.concat(localizedModifiers, localizedKeys, commonKeys);
  };

  const _quitAndInstall = () => {
    ipcRenderer.send('quitAndInstall');
  };

  const _updateBrowserWindow = (data) => {
    if (data.title) {
      $rootScope.title = i18n.__('APP_NAME',{'name': data.title});
      $rootScope.$apply();
    }
    if (data.size) {
      ipcRenderer.send('updateBrowserWindowSize',data.size);
    }
    if (data.focus) {
      ipcRenderer.send('focusBrowserWindow');
    }
  };
  
  return {
    setUpdateUrl: _setUpdateUrl,
    setUpdateShortcut: _setUpdateShortcut,
    setUpdateCloseTray: _setUpdateCloseTray,
    setUpdateStartUp: _setUpdateStartUp,
    getUpdateShortcut: _getUpdateShortcut,
    displayShortcut: _displayShortcut,
    buildKeyList: _buildKeyList,
    quitAndInstall: _quitAndInstall,
    throwUrlError: _throwUrlError,
    hasError: _hasError,
    setError: _setError,
    clearError: _clearError,
    updateBrowserWindow: _updateBrowserWindow
  };
});

desktopApp.directive('validShortcut', function (updateService) {
  return {
    require: 'ngModel',
    link: function ($scope, elm, attrs, ctrl) {
      $scope.$watch(attrs.ngModel, function(viewValue) {
        ctrl.$setValidity('format', _.isEmpty(_.trim(viewValue)) || isAccelerator(updateService.getUpdateShortcut(viewValue)));
      });
    }
  };
});

desktopApp.controller('MainController', function MainController($scope, desktopSettings, $state, updateService, $log) {
  const webview = document.querySelector('webview');

  $scope.throwWebviewError = () => {
    $log.error('Error failed to load webview - ' + webview.src);
    updateService.setError();
    if ($state.current.name != 'main.settings') {
      updateService.throwUrlError(webview.src);
    }
  };

  $scope.successWebviewLoad = () => {
    $log.info('Webview is loaded');
    updateService.clearError();
  };
  
  webview.addEventListener('did-fail-load', () => {
    $scope.throwWebviewError();
  });

  webview.addEventListener('load-commit', () => {
    $scope.successWebviewLoad();
  });

  webview.addEventListener('console-message', (e) => {
    $log.info('<webview>:', e.message);
  });

  webview.addEventListener('ipc-message', (e) => {
    if (e.channel) updateService.updateBrowserWindow(e.channel);
  });

  ipcRenderer.on('sref' , (event , location) => {
    $state.go(location);
  });

  ipcRenderer.on('msg' , (event , data) => {
    webview.executeJavaScript('window.postMessage('+ data +', "/")');
  });

  let close = (desktopSettings.get(desktopSettings.KEY_APP_CLOSE) == 'true');
  updateService.setUpdateCloseTray(close);

  let protocol = desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL);
  let domain = desktopSettings.get(desktopSettings.KEY_APP_DOMAIN);
  if(protocol !== null && domain !== null) {
    $scope.url = protocol + '://' + domain;
    updateService.setUpdateUrl($scope.url, false);
  } else {
    $state.go('main.settings');
  }

  let shortcut = desktopSettings.get(desktopSettings.KEY_APP_SHORTCUT) || desktopSettings.DEFAULT_SHORTCUT;
  updateService.setUpdateShortcut(shortcut);

  $scope.showWebApp = () => {
    return $state.current.name === 'main';
  };
});

desktopApp.controller('SettingsController', function SettingsController($scope, desktopSettings, $state, updateService) {
  $scope.protocol = desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL);
  $scope.domain = desktopSettings.get(desktopSettings.KEY_APP_DOMAIN);
  $scope.close = (desktopSettings.get(desktopSettings.KEY_APP_CLOSE) == 'true');
  $scope.startup = (desktopSettings.get(desktopSettings.KEY_APP_STARTUP) == 'true');
  $scope.keyList = updateService.buildKeyList();
  $scope.shortcut = {};

  let shortcut = desktopSettings.get(desktopSettings.KEY_APP_SHORTCUT) || desktopSettings.DEFAULT_SHORTCUT;
  if (shortcut == 'null' || !isAccelerator(shortcut)){
    $scope.shortcut.current = null;
  }
  else {
    $scope.shortcut.current = _.map(_.split(shortcut,'+'),
      function(key) { return _.find($scope.keyList,
        function(item){
          return key == item.id;
        });
      }
    );
  }

  $scope.save = function() {
    let shortcut = updateService.getUpdateShortcut($scope.shortcut.current);
    if (_.isEmpty(_.trim(shortcut))){
      desktopSettings.set(desktopSettings.KEY_APP_SHORTCUT, 'null');
    }
    if (isAccelerator(shortcut)) {
      desktopSettings.set(desktopSettings.KEY_APP_SHORTCUT, shortcut);
      updateService.setUpdateShortcut(shortcut);
    }
    desktopSettings.set(desktopSettings.KEY_APP_STARTUP, $scope.startup);
    updateService.setUpdateStartUp($scope.startup);
    desktopSettings.set(desktopSettings.KEY_APP_CLOSE, $scope.close);
    updateService.setUpdateCloseTray($scope.close);
    desktopSettings.set(desktopSettings.KEY_APP_PROTOCOL, $scope.protocol);
    desktopSettings.set(desktopSettings.KEY_APP_DOMAIN, $scope.domain);
    let url = $scope.protocol + '://' + $scope.domain;
    if ($scope.url != url || updateService.hasError()) {
      updateService.setUpdateUrl(url, true);
    }
    $state.go('main');
  };

  $scope.displayShortcutError = function() {
    return updateService.displayShortcut($scope.shortcut.current) + ' ' + i18n.__('SETTINGS_SHORTCUT_WRONG_FORMAT');
  };

  $scope.groupKey = function(key) {
    return (shortcuts.LOCALIZED_MODIFIER_KEYS.indexOf(key.id) > -1) ? i18n.__('SETTINGS_SHORTCUT_MODIFIERS') : i18n.__('SETTINGS_SHORTCUT_KEYS');
  };

});

desktopApp.controller('LoadingErrorController', function SettingsController($scope, desktopSettings, $state, $timeout) {
  var _retryTimeout = 60;
  $scope.retry = { delay: _retryTimeout };
  
  var _retryCountdown = function() {
    $scope.retry.delay--;
    if($scope.retry.delay <= 0 && $state.current.name !== 'main.settings') {
      $state.go('main');
    } else {
      $timeout(_retryCountdown, 1000);
    }
  };

  ipcRenderer.on('sref' , (event , location) => {    
    $state.go(location);
  });

  $scope.remoteUrl = desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL) + '://' + desktopSettings.get(desktopSettings.KEY_APP_DOMAIN);
  $timeout(_retryCountdown, 1000);
});

desktopApp.controller('UpdateAvailableController', function UpdateAvailableController($scope, updateService) {
  $scope.quitAndInstall = function() { updateService.quitAndInstall(); };
});

desktopApp.filter('trusted', ['$sce', function ($sce) {
  return function(url) {
    return $sce.trustAsResourceUrl(url);
  };
}]);
