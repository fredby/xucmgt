package model

import models.ConfigurationEntry
import org.scalatest.{Matchers, WordSpec}
import play.api.libs.json.Json

class ConfigurationEntrySpec extends WordSpec with Matchers {
  "ConfigurationEntry" should {
    "convert to json" in {
      val c = ConfigurationEntry("titi", "toto")
      Json.toJson(c) should be(Json.parse("""{"name":"titi", "value":"toto"}"""))
    }
  }
}
