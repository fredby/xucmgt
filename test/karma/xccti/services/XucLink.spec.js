'use strict';

describe('Xuc service link', function() {
  var xucLink;
  var rootScope;
  var locationValue = {
    origin: 'test',
    protocol: 'http:',
    href: 'http://myxucmgt/',
    replace: function() {}
  };
  var origLocalStorage;
  var windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'}};
    }
  };

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
    angular.mock.module(function($provide) {
      $provide.provider('$window', windowProvider);
    });
    angular.mock.module('xcCti', function ($translateProvider) {
      $translateProvider.preferredLanguage('en');
    });
    Cti.Topic(Cti.MessageType.LOGGEDON).clear();
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).clear();
    Cti.Topic(Cti.MessageType.RIGHTPROFILE).clear();

    var localStorageMock = (function() {
      return {
        getItem: function() { return 'isSpiedAfterWithASpecificReturnValue';},
        setItem: jasmine.createSpy(),
        removeItem: jasmine.createSpy(),
      };
    })();
    origLocalStorage = window.localStorage;
    Object.defineProperty(window, 'localStorage', { value: localStorageMock,configurable:true,enumerable:true,writable:true });
  });

  beforeEach(angular.mock.inject(function(_XucLink_, _$rootScope_) {
    xucLink = _XucLink_;
    rootScope = _$rootScope_;
    spyOn(rootScope, '$broadcast');
  }));

  afterEach(function(){
    Object.defineProperty(window, 'localStorage', {value: origLocalStorage});
  });

  it('should be able to start cti using http', function() {
    spyOn(Cti.WebSocket,'init');
    locationValue.protocol = 'http:';

    xucLink.setHostAndPort('host:8090');

    xucLink.initCti("john","doe",44500);

    var wsurl = "ws://host:8090/xuc/ctichannel?username=john&amp;agentNumber=44500&amp;password=doe";

    expect(Cti.WebSocket.init).toHaveBeenCalledWith(wsurl, "john", 44500);
  });

  it('should be able to start cti using https', function() {
    spyOn(Cti.WebSocket,'init');
    locationValue.protocol = 'https:';

    xucLink.setHostAndPort('host:8090');

    xucLink.initCti("john","doe",44500);

    var wsurl = "wss://host/xuc/ctichannel?username=john&amp;agentNumber=44500&amp;password=doe";

    expect(Cti.WebSocket.init).toHaveBeenCalledWith(wsurl, "john", 44500);
  });


  it('should broadcast ctiLoggedOn and set logged to true on user login', function() {
    var whenLogged = jasmine.createSpy('whenLogged');
    xucLink.whenLogged().then(whenLogged);
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");

    expect(xucLink.isLogged()).toEqual(true);
    expect(rootScope.$broadcast).toHaveBeenCalledWith('ctiLoggedOn', {});
    rootScope.$digest();
    expect(whenLogged).toHaveBeenCalled();
  });

  it('should resolve whenLogged promise after failed and then successfull login', function() {
    var whenLogged = jasmine.createSpy('whenLogged');
    xucLink.whenLogged().then(whenLogged);
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");

    expect(xucLink.isLogged()).toEqual(true);
    expect(rootScope.$broadcast).toHaveBeenCalledWith('ctiLoggedOn', {});
    rootScope.$digest();
    expect(whenLogged).toHaveBeenCalled();
  });

  it('should login using new api', angular.mock.inject(function($httpBackend) {
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    expect(localStorage.setItem).not.toHaveBeenCalled();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('ws://xuc:9000/xuc/api/2.0/cti?token=' + token, "jbond", "1001");
  }));

  it('should get user when logged', angular.mock.inject(function($httpBackend) {
    spyOn(Cti.WebSocket, "init");
    var whenLoggedCB = jasmine.createSpy('whenLoggedCB');
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('ws://xuc:9000/xuc/api/2.0/cti?token=' + token, "jbond", "1001");
    // mock login event
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    xucLink.whenLogged().then(whenLoggedCB);
    rootScope.$digest();
    expect(whenLoggedCB).toHaveBeenCalledWith({username: "jbond", phoneNumber: "1001", token: token});
  }));

  it('should fails if server respond 502 with html', angular.mock.inject(function($httpBackend) {

    var errorCallback = jasmine.createSpy('errorCallback');
    
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false).catch(errorCallback);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234"}).respond(502, '<html><head><title>ERROR</title></head><body>Some message</body></html>');
    $httpBackend.flush();

    expect(errorCallback).toHaveBeenCalledWith({error: 'NoResponse', message: 'No response from server' });
    
  }));

  it('should fail if server responds user not found', angular.mock.inject(function($httpBackend) {
    spyOn(Cti.WebSocket, "init");
    var errorCallback = jasmine.createSpy('errorCallback');
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false).catch(errorCallback);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234"}).respond(403, '{"error":"UserNotFound","message":"User not found"}');
    $httpBackend.flush();

    expect(errorCallback).toHaveBeenCalledWith({error: 'UserNotFound', message: 'User not found' });
  }));

  it('should fail if server responds invalid password', angular.mock.inject(function($httpBackend) {
    spyOn(Cti.WebSocket, "init");
    var errorCallback = jasmine.createSpy('errorCallback');
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false).catch(errorCallback);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234"}).respond(403, '{"error":"InvalidPassword","message":"Invalid password"}');
    $httpBackend.flush();
    expect(errorCallback).toHaveBeenCalledWith({error: 'InvalidPassword', message: 'Invalid password' });
  }));

  it('should login using wss when using https', angular.mock.inject(function($httpBackend) {
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "https:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false);
    $httpBackend.expectPOST('https://xuc/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    expect(localStorage.setItem).not.toHaveBeenCalled();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('wss://xuc/xuc/api/2.0/cti?token=' + token, "jbond", "1001");
  }));

  it('should persist credentials when asked to', angular.mock.inject(function($httpBackend) {
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", true);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    var expected = JSON.stringify({login: "jbond", token: token, phoneNumber: "1001"});
    expect(localStorage.setItem).toHaveBeenCalledWith("credentials", expected);
  }));

  it('when login with stored credentials, check token validity and renew using new api', angular.mock.inject(function($httpBackend) {
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    var newToken = "eeee-ffff-gggg-hhhh-5678";
    spyOn(Cti.WebSocket, "init");
    spyOn(localStorage, "getItem").and.returnValue(JSON.stringify(credentials));
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.loginWithStoredCredentials();
    expect(localStorage.getItem).toHaveBeenCalledWith("credentials");
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/check', undefined, function(headers) {
      return headers['Authorization'] === ('Bearer ' + credentials.token) ;
    }).respond(201, {login: "jbond", token: newToken});
    $httpBackend.flush();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('ws://xuc:9000/xuc/api/2.0/cti?token=' + newToken, "jbond", "1001");
  }));

  it('should clear credentials upon connection error', angular.mock.inject(function($httpBackend) {
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    spyOn(Cti.WebSocket, "init");
    spyOn(localStorage, "getItem").and.returnValue(JSON.stringify(credentials));
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.loginWithStoredCredentials().catch(function(){});
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/check', undefined, function(headers) {
      return headers['Authorization'] === ('Bearer ' + credentials.token) ;
    }).respond(403, {error: "TokenExpired", message: "Token expired"});
    $httpBackend.flush();
    expect(localStorage.removeItem).toHaveBeenCalledWith("credentials");
    expect(Cti.WebSocket.init).not.toHaveBeenCalled();
  }));

  it('(CAS Auth) should redirect to cas server when no ticket is found', function() {
    locationValue.search = '';
    locationValue.href = 'http://xuc.org';
    xucLink.getCasCredentials('https://cas.server.org/cas');
    expect(locationValue.href).toEqual('https://cas.server.org/cas/login?service=' + escape('http://xuc.org'));
  });

  it('(CAS Auth) should redirect to cas server with given parameters when no ticket is found', function() {
    locationValue.search = '';
    locationValue.href = 'http://xuc.org';
    xucLink.getCasCredentials('https://cas.server.org/cas', '?lastError=Logout');
    expect(locationValue.href).toEqual('https://cas.server.org/cas/login?service=' + escape('http://xuc.org?lastError=Logout'));
  });

  it('(CAS Auth) should validate ticket against xuc', angular.mock.inject(function($httpBackend) {
    var service = escape('http://xuc.org');
    var ticket = 'ST-27-abcd-efgh';
    locationValue.search = '?ticket=' + ticket;
    locationValue.href = 'http://xuc.org';
    xucLink.setHostAndPort("xuc:9000");
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/cas?service=' + service + '&ticket=' + ticket).respond(201, {login: "jbond", token: "abcd-efgh"});
    xucLink.getCasCredentials('https://cas.server.org/cas').catch(function() {});
    $httpBackend.flush();
  }));

  it('(CAS Auth) should request new ticket when ticket is invalid (expired)', angular.mock.inject(function($httpBackend) {
    var service = escape('http://xuc.org');
    var ticket = 'ST-27-abcd-efgh';
    locationValue.search = '?ticket=' + ticket;
    locationValue.href = 'http://xuc.org';
    xucLink.setHostAndPort("xuc:9000");
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/cas?service=' + service + '&ticket=' + ticket)
      .respond(403, {error: "CasServerInvalidTicket", message: "Ticket is invalid"});
    xucLink.getCasCredentials('https://cas.server.org/cas').catch(function() {});
    $httpBackend.flush();
    expect(locationValue.href).toEqual('https://cas.server.org/cas/login?service=' + escape('http://xuc.org'));
  }));

  it('can resolve rightProfile promise when receiving RIGHTPROFILE event', function(done) {
    var rightProfile = {profile: 'Supervisor', rights: 'recording'};
    spyOn(xucLink, 'getProfileRightAsync').and.callThrough();

    var p = xucLink.getProfileRightAsync();

    p.then((value) => {
      expect(value).toEqual(rightProfile);
      done();
    });

    Cti.Topic(Cti.MessageType.RIGHTPROFILE).publish(rightProfile);
    rootScope.$digest();
  });

  it('should answer if right is allowed in RightProfile', function() {
    var rightProfile = {
      profile: "myProfile",
      rights: 'recording'
    };
    expect(xucLink.isRightAllowed(rightProfile, 'recording')).toEqual(true);
    expect(xucLink.isRightAllowed(rightProfile, 'other')).toEqual(false);
  });

});
