describe('CtiProxy', function() {

  var ctiMethods = ['dial', 'hangup', 'answer', 'hold', 'attendedTransfer', 'completeTransfer', 'cancelTransfer', 'conference', 'unsetHandler', 'getConfig'];
  var xcWebrtcMethods = ['dial', 'answer', 'attendedTransfer', 'hold', 'dtmf', 'clearHandlers', 'setHandler', 'disableICE', 'initByLineConfig', 'stop'];

  var xcWebrtcMock;
  var oldXcWebrtc;
  var ctiMock;
  var oldCti;
  var lineCfgCallback;

  var XucPhoneState;
  var phoneCalls;
  var remoteConfiguration;
  var $q;
  var $rootScope;
  var XucPhoneEventListener;

  var webRtcAudio;

  var hostnameValue = 'testName';
  var locationValue = { hostname: hostnameValue, protocol: 'https:', port: ''};
  var disableDefer;
  var windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'}
      };
    }
  };

  function disableWebRtc(disable) {
    disableDefer.resolve(disable);
    $rootScope.$digest();
  }

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(function() {
    xcWebrtcMock = jasmine.createSpyObj('xc_webrtc', xcWebrtcMethods);
    xcWebrtcMock.MessageType = xc_webrtc.MessageType;
    oldXcWebrtc = xc_webrtc;
    xc_webrtc = xcWebrtcMock;

    ctiMock = jasmine.createSpyObj('Cti', ctiMethods);
    ctiMock.MessageType = Cti.MessageType;
    ctiMock.setHandler = function(msgType, callback) {
      if(msgType === Cti.MessageType.LINECONFIG) {
        lineCfgCallback = callback;
      }
    };
    oldCti = Cti;
    Cti = ctiMock;
  });

  beforeEach(function() {
    angular.mock.module(function($provide) {
      $provide.provider('$window', windowProvider);
    });
    angular.mock.module('xcCti');
    angular.mock.module('xcHelper');
    angular.mock.inject(function(_XucPhoneState_, _XucPhoneEventListener_, _remoteConfiguration_, _$q_, _$rootScope_) {
      XucPhoneState = _XucPhoneState_;
      XucPhoneEventListener = _XucPhoneEventListener_;
      remoteConfiguration = _remoteConfiguration_;
      $q = _$q_;
      $rootScope = _$rootScope_;
      disableDefer = $q.defer();
      spyOn(remoteConfiguration, 'getBooleanOrElse').and.returnValue(disableDefer.promise);
      spyOn(XucPhoneState, 'getCalls').and.callFake(function() { return phoneCalls; });
      phoneCalls = [];
      spyOn(XucPhoneEventListener, 'addHandlerCustom').and.callThrough();
      spyOn($rootScope, '$broadcast').and.callThrough();
    });
  });

  beforeEach(angular.mock.inject(function(_webRtcAudio_) {
    webRtcAudio = _webRtcAudio_;
    spyOn(webRtcAudio, 'enable');
    spyOn(webRtcAudio, 'disable');
  }));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function($httpBackend) {
    $httpBackend.whenGET("assets/i18n/phonebar-en.json").respond({});
    $httpBackend.whenGET("/ucassistant/login.html").respond("");
    $httpBackend.whenGET("/config/disableWebRtc").respond('{"name":"disableWebRtc","value":"false"}');
  }));

  afterEach(function() {
    xc_webrtc = oldXcWebrtc;
    Cti = oldCti;
  });

  it('can instanciate service', angular.mock.inject(function(CtiProxy) {
    disableWebRtc(false);
    expect(CtiProxy).not.toBeUndefined();
  }));

  it('is using Cti in default state', angular.mock.inject(function(CtiProxy) {
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toBe(false);
    expect(CtiProxy.isUsingCti()).toBe(true);
  }));

  var verifyProxyCalls = function(CtiProxy, target, hangupTarget) {
    CtiProxy.hangup();
    expect(hangupTarget.hangup).toHaveBeenCalledWith();

    var methods = ['dial', 'answer', 'hold', 'attendedTransfer'];
    for (var idx in methods) {
      var method = methods[idx];
      CtiProxy[method]();
      expect(target[method]).toHaveBeenCalled();
    }
  };

  it('proxy calls to Cti in default state', angular.mock.inject(function(CtiProxy) {
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toBe(false);
    verifyProxyCalls(CtiProxy, ctiMock, ctiMock);
  }));


  it('on ctiLoggedOn registers Cti handler and requests line config', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    disableWebRtc(false);
    expect(CtiProxy).not.toBeUndefined();
    expect(lineCfgCallback).not.toBeUndefined();
    expect(ctiMock.getConfig).toHaveBeenCalledWith('line');
  }));

  it('on lineConfig with webRtc and no device true inits webRTC', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      hasDevice: false,
      webRtc: true
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toEqual(true);
    expect(XucPhoneEventListener.addHandlerCustom).toHaveBeenCalled();
    expect(xc_webrtc.initByLineConfig)
      .toHaveBeenCalledWith(lineCfg, 'XiVO Assistant', true, 443, 'audio_remote', hostnameValue);
  }));

  it('on lineConfig with webrtc false and some line id use Cti', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      hasDevice: true,
      webRtc: false,
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);
    expect(CtiProxy.isUsingCti()).toBe(true);
    verifyProxyCalls(CtiProxy, ctiMock, ctiMock);
  }));

  it('on lineConfig with webrtc false, without device and with name starting by Local/ report custom line', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      hasDevice: false,
      webRtc: false,
      name: 'Local/01230041301'
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toEqual(false);
    expect(CtiProxy.isCustomLine()).toEqual(true);
  }));

  it('on lineConfig with webrtc false, without device and with name starting by local/ (any case)  report custom line', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      hasDevice: false,
      webRtc: false,
      name: 'loCal/01230041301'
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toEqual(false);
    expect(CtiProxy.isCustomLine()).toEqual(true);
  }));


  it('Allow conference on Snom device when using Cti', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      id:"1000",
      hasDevice: true,
      webRtc: false,
      vendor: "Snom"
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);

    expect(CtiProxy.isUsingCti()).toBe(true);
    expect(CtiProxy.getDeviceVendor()).toBe("Snom");
    expect(CtiProxy.isConferenceCapable()).toBe(true);
  }));

  it('Prevent conference on Non-Snom device when using Cti', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      id:"1000",
      hasDevice: true,
      webRtc: false,
      vendor: "Yealink"
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);

    expect(CtiProxy.isUsingCti()).toBe(true);
    expect(CtiProxy.getDeviceVendor()).toBe("Yealink");
    expect(CtiProxy.isConferenceCapable()).toBe(false);
  }));

  it('Prevent conference when using webRTC', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      id:"1000",
      hasDevice: true,
      webRtc: false,
      vendor: "Snom"
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);


    $rootScope.$broadcast('ctiLoggedOn');
    lineCfg = {
      hasDevice: false,
      webRtc: true,
      vendor: null
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);

    expect(CtiProxy.isUsingWebRtc()).toBe(true);
    expect(CtiProxy.getDeviceVendor()).toBe(CtiProxy.UnknownDeviceVendor);
    expect(CtiProxy.isConferenceCapable()).toBe(false);
  }));

  it('Reset device vendor to unknown after switch from Snom to webRTC', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');

    var lineCfg = {
      id:"1000",
      hasDevice: true,
      webRtc: false,
      vendor: "Snom"
    };
    lineCfgCallback(lineCfg);
    disableWebRtc(false);

    expect(CtiProxy.getDeviceVendor()).toBe("Snom");

    lineCfg = {
      hasDevice: false,
      webRtc: true,
      vendor: null
    };
    lineCfgCallback(lineCfg);

    expect(CtiProxy.getDeviceVendor()).toBe(CtiProxy.UnknownDeviceVendor);
  }));

  it('processLineCfg on line with webrtc false and some line id initializes with Cti', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ webRtc: false, hasDevice: true, id: '12' });
    disableWebRtc(false);
    expect(CtiProxy.isUsingCti()).toBe(true);
    expect(webRtcAudio.disable).toHaveBeenCalled();
    verifyProxyCalls(CtiProxy, ctiMock, ctiMock);
  }));

  it('processLineCfg on line without device and with id '-' refuses to init', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ webRtc: false, hasDevice: false, id: '-' });
    disableWebRtc(false);
    expect($rootScope.$broadcast).toHaveBeenCalledWith(CtiProxy.FatalError, CtiProxy.FatalErrors.MISSING_LINE);
  }));

  it('processLineCfg on line without device but without SSL refuses to init', angular.mock.inject(function(CtiProxy, $rootScope) {
    CtiProxy._setProtocolForTest('http:');
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toBe(false);
    expect($rootScope.$broadcast).toHaveBeenCalledWith(CtiProxy.FatalError, CtiProxy.FatalErrors.WEBRTC_REQUIRES_SSL);
    expect(webRtcAudio.disable).toHaveBeenCalled();
  }));

  it('when switched to webRtc, processLineCfg on line with device does switch to Cti', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toBe(true);
    verifyProxyCalls(CtiProxy, xcWebrtcMock, ctiMock);
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    $rootScope.$digest();
    expect(CtiProxy.isUsingCti()).toBe(true);
    verifyProxyCalls(CtiProxy, ctiMock, ctiMock);
  }));

  it('when using WebRtc, it sends dtmf', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    CtiProxy.dtmf('#');
    expect(xcWebrtcMock.dtmf).toHaveBeenCalledWith('#');
  }));

  it('when using Cti, it does not send dtmf', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    disableWebRtc(false);
    CtiProxy.dtmf('#');
    expect(xcWebrtcMock.dtmf).not.toHaveBeenCalled();
  }));

  it('when using Cti and stopUsingWebRtc() is called, it continues using Cti', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    disableWebRtc(false);
    expect(CtiProxy.isUsingCti()).toBe(true);
    CtiProxy.stopUsingWebRtc();
    expect(xcWebrtcMock.stop).not.toHaveBeenCalledWith();
    expect(webRtcAudio.disable).toHaveBeenCalled();
    expect(CtiProxy.isUsingCti()).toBe(true);
  }));

  it('when using WebRtc and stopUsingWebRtc() is called, it switch to Cti', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toBe(true);
    CtiProxy.stopUsingWebRtc();
    expect(xcWebrtcMock.stop).toHaveBeenCalledWith();
    expect(webRtcAudio.disable).toHaveBeenCalled();
    expect(CtiProxy.isUsingCti()).toBe(true);
  }));

  function expectCtiDialAllowed(CtiProxy, _phoneCalls) {
    phoneCalls = _phoneCalls;
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    disableWebRtc(false);
    CtiProxy.dial('12345', {foo: "bar"});
    expect(ctiMock.dial).toHaveBeenCalledWith('12345', {foo: "bar"}, false);
  }

  it('allow dial when there are no ongoing calls and not using WebRtc', angular.mock.inject(function(CtiProxy) {
    expectCtiDialAllowed(CtiProxy, []);
  }));

  it('allow dial when there are one ongoing calls and not using WebRtc', angular.mock.inject(function(CtiProxy) {
    expectCtiDialAllowed(CtiProxy, [{}]);
  }));

  it('allow dial when there are two ongoing calls and not using WebRtc', angular.mock.inject(function(CtiProxy) {
    expectCtiDialAllowed(CtiProxy, [{}, {}]);
  }));

  function testDialWebRtc(CtiProxy, _phoneCalls, video) {
    phoneCalls = _phoneCalls;
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true });
    disableWebRtc(false);
    CtiProxy.dial('12345', {foo: "bar"}, typeof video === 'undefined' ? false : video);
  }

  it('allow dial when there is no ongoing call and using WebRtc', angular.mock.inject(function(CtiProxy) {
    testDialWebRtc(CtiProxy, []);
    expect(xcWebrtcMock.dial).toHaveBeenCalledWith('12345', false);
  }));

  it('allow dial when there is one ongoing call and using WebRtc', angular.mock.inject(function(CtiProxy) {
    testDialWebRtc(CtiProxy, [{}]);
    expect(xcWebrtcMock.dial).toHaveBeenCalledWith('12345', false);
  }));

  it('propagates video call parameter when using WebRTC', angular.mock.inject(function(CtiProxy) {
    testDialWebRtc(CtiProxy, [{}], true);
    expect(xcWebrtcMock.dial).toHaveBeenCalledWith('12345', true);
  }));

  it('refuse dial when there are two ongoing calls and using WebRtc', angular.mock.inject(function(CtiProxy) {
    testDialWebRtc(CtiProxy, [{}, {}]);
    expect(xcWebrtcMock.dial).not.toHaveBeenCalled();
  }));

  it('ask for line config on updateLine', angular.mock.inject(function(CtiProxy) {
    ctiMock.getConfig = jasmine.createSpy();
    CtiProxy.updateLine();
    expect(ctiMock.getConfig).toHaveBeenCalledWith('line');
  }));

  it('returns 1 as answerable calls for Cti and 2 for WebRTC', angular.mock.inject(function(CtiProxy) {
    expect(CtiProxy.getMaxAnswerableCalls()).toBe(1);
    switchToWebRTC(CtiProxy);
    expect(CtiProxy.getMaxAnswerableCalls()).toBe(2);
  }));

  it('forward conferenceMuteMe to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceMuteMe = function() {};
    spyOn(ctiMock, 'conferenceMuteMe');
    CtiProxy.conferenceMuteMe('4000');
    expect(ctiMock.conferenceMuteMe).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceUnmuteMe to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceUnmuteMe = function() {};
    spyOn(ctiMock, 'conferenceUnmuteMe');
    CtiProxy.conferenceUnmuteMe('4000');
    expect(ctiMock.conferenceUnmuteMe).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceMuteAll to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceMuteAll = function() {};
    spyOn(ctiMock, 'conferenceMuteAll');
    CtiProxy.conferenceMuteAll('4000');
    expect(ctiMock.conferenceMuteAll).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceUnmuteAll to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceUnmuteAll = function() {};
    spyOn(ctiMock, 'conferenceUnmuteAll');
    CtiProxy.conferenceUnmuteAll('4000');
    expect(ctiMock.conferenceUnmuteAll).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceMute to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceMute = function() {};
    spyOn(ctiMock, 'conferenceMute');
    CtiProxy.conferenceMute('4000', 1);
    expect(ctiMock.conferenceMute).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceUnmute to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceUnmute = function() {};
    spyOn(ctiMock, 'conferenceUnmute');
    CtiProxy.conferenceUnmute('4000', 1);
    expect(ctiMock.conferenceUnmute).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceKick to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceKick = function() {};
    spyOn(ctiMock, 'conferenceKick');
    CtiProxy.conferenceKick('4000', 1);
    expect(ctiMock.conferenceKick).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceMuteMe to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    ctiMock.conferenceMuteMe = function() {};
    spyOn(ctiMock, 'conferenceMuteMe');
    CtiProxy.conferenceMuteMe('4000');
    expect(ctiMock.conferenceMuteMe).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceUnmuteMe to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    ctiMock.conferenceUnmuteMe = function() {};
    spyOn(ctiMock, 'conferenceUnmuteMe');
    CtiProxy.conferenceUnmuteMe('4000');
    expect(ctiMock.conferenceUnmuteMe).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceMuteAll to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    ctiMock.conferenceMuteAll = function() {};
    spyOn(ctiMock, 'conferenceMuteAll');
    CtiProxy.conferenceMuteAll('4000');
    expect(ctiMock.conferenceMuteAll).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceUnmuteAll to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    ctiMock.conferenceUnmuteAll = function() {};
    spyOn(ctiMock, 'conferenceUnmuteAll');
    CtiProxy.conferenceUnmuteAll('4000');
    expect(ctiMock.conferenceUnmuteAll).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceMute to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    ctiMock.conferenceMute = function() {};
    spyOn(ctiMock, 'conferenceMute');
    CtiProxy.conferenceMute('4000', 1);
    expect(ctiMock.conferenceMute).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceUnmute to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    ctiMock.conferenceUnmute = function() {};
    spyOn(ctiMock, 'conferenceUnmute');
    CtiProxy.conferenceUnmute('4000', 1);
    expect(ctiMock.conferenceUnmute).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceKick to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    ctiMock.conferenceKick = function() {};
    spyOn(ctiMock, 'conferenceKick');
    CtiProxy.conferenceKick('4000', 1);
    expect(ctiMock.conferenceKick).toHaveBeenCalledWith('4000', 1);
  }));

  function switchToWebRTC(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    disableWebRtc(false);
    expect(CtiProxy.isUsingWebRtc()).toBe(true);
  }
});
