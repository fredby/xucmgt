
describe('Xuc service agent user', function() {
  var xucUser;
  var xucAgentUser;
  var xucQueue;
  var xucAgent;
  var $rootScope;
  var $timeout;

  var failTest = function(error) {
    expect(error).toBeUndefined();
  };
  
  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData :  customMatchers.toEqualData
    });
    spyOn(Cti,'setHandler').and.callThrough();
    Cti.debugMsg = true;
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucAgentUser_,_$rootScope_, _XucUser_, _XucQueue_, _XucAgent_, _$timeout_, $q) {
    $rootScope = _$rootScope_;
    xucUser = _XucUser_;
    xucAgentUser = _XucAgentUser_;
    xucQueue = _XucQueue_;
    xucAgent = _XucAgent_;
    $timeout = _$timeout_;
    spyOn($rootScope, '$broadcast');
    spyOn(Cti, 'sendCallback');

    var userPromise = $q.defer();
    var user = {
      id:5,
      userId: 1,
      agentId: 5,
      firstName: "James",
      lastName: "Bond",
      fullName: "James Bond"
    };
    userPromise.resolve(user);
    spyOn(xucUser, 'getUserAsync').and.callFake(() => {
      return userPromise.promise;
    });

  }));


  it('setup handlers when starting', function() {
    expect(Cti.setHandler).toHaveBeenCalled();
  });

  it('get queues asynchronously', function() {

    var cb = {getQueuesCb: function() {}};
    spyOn(cb, 'getQueuesCb');

    var userConfig = {
      id:5,
      userId: 1,
      agentId: 5,
      firstName: "James",
      lastName: "Bond",
      fullName: "James Bond"
    };

    var agentList = [userConfig];
    var queueList = [{'id':17, 'name' : 'Hello'}, {'id':18, 'name' : 'World'}];
    var queueMembers = [{'agentId' : 5, 'queueId' : 17, 'penalty' : 9 },{'agentId' : 5, 'queueId' : 18, 'penalty' : 2 }];

    var expected = [{'id':17, 'name' : 'Hello', LongestWaitTime: '-', WaitingCalls: 0, 'penalty': 9}, {'id':18, 'name' : 'World', LongestWaitTime: '-', WaitingCalls: 0, 'penalty': 2}];

    xucAgentUser.getQueuesAsync().then(cb.getQueuesCb);

    xucAgent.onAgentList(agentList);
    xucAgent.onQueueMemberList(queueMembers);
    xucQueue.onQueueList(queueList);

    $rootScope.$digest();
    expect(cb.getQueuesCb).toHaveBeenCalledWith(expected);

  });

  it('check if member of a queue asynchronously', function() {

    var cb = {isMemberCb: function() {}};
    spyOn(cb, 'isMemberCb');

    var userConfig = {
      id:5,
      userId: 1,
      agentId: 5,
      firstName: "James",
      lastName: "Bond",
      fullName: "James Bond"
    };

    var agentList = [userConfig];
    var queueList = [{'id':17, 'name' : 'Hello'}, {'id':18, 'name' : 'World'}];
    var queueMembers = [{'agentId' : 5, 'queueId' : 17, 'penalty' : 9 },{'agentId' : 5, 'queueId' : 18, 'penalty' : 2 }];

    xucAgentUser.isMemberOfQueueAsync(17).then(cb.isMemberCb);

    xucAgent.onAgentList(agentList);
    xucAgent.onQueueMemberList(queueMembers);
    xucQueue.onQueueList(queueList);

    $rootScope.$digest();
    expect(cb.isMemberCb).toHaveBeenCalledWith(true);

    xucAgentUser.isMemberOfQueueAsync(99).then(cb.isMemberCb);
    $rootScope.$digest();
    expect(cb.isMemberCb).toHaveBeenCalledWith(false);

  });

  it('allow to join a queue', function() {
    spyOn(Cti, 'setAgentQueue');

    xucAgentUser.joinQueue(1, 2);
    $rootScope.$digest();

    expect(Cti.setAgentQueue).toHaveBeenCalledWith(5, 1, 2);
  });

  it('allow to leave a queue', function() {
    spyOn(Cti, 'removeAgentFromQueue');

    xucAgentUser.leaveQueue(42);
    $rootScope.$digest();

    expect(Cti.removeAgentFromQueue).toHaveBeenCalledWith(5, 42);
  });


  it('allow to join a queue asynchronously', function() {
    spyOn(Cti, 'setAgentQueue');

    xucAgentUser.joinQueueAsync(1, 2);
    $rootScope.$digest();

    expect(Cti.setAgentQueue).toHaveBeenCalledWith(5, 1, 2);
  });

  it('allow to leave a queue asynchronously', function() {
    spyOn(Cti, 'removeAgentFromQueue');

    xucAgentUser.leaveQueueAsync(42);
    $rootScope.$digest();

    expect(Cti.removeAgentFromQueue).toHaveBeenCalledWith(5, 42);
  });

  it('should fail when timeout while trying to enter a queue', function(done) {
    spyOn(Cti, 'setAgentQueue').and.callThrough();

    xucAgentUser.setQueueTimeoutMs(100);
    var p = xucAgentUser.joinQueueAsync(1, 2);

    p.then(failTest, function(e) {
      expect(e.error).toEqual("timeout");
      done();
    });

    $timeout.flush();

    expect(Cti.setAgentQueue).toHaveBeenCalledWith(5, 1, 2);
  });

  it('should fail when timeout while trying to enter a queue', function(done) {
    spyOn(Cti, 'removeAgentFromQueue').and.callThrough();

    xucAgentUser.setQueueTimeoutMs(100);
    var p = xucAgentUser.leaveQueueAsync(1);

    p.then(failTest, function(e) {
      expect(e.error).toEqual("timeout");
      done();
    });

    $timeout.flush();

    expect(Cti.removeAgentFromQueue).toHaveBeenCalledWith(5, 1);
  });

  it('should detect when an agent call is monitored', function() {

    var callback = jasmine.createSpy('callback');

    var scope = $rootScope.$new();

    xucAgentUser.subscribeToListenEvent(scope, callback);
    var evt = {started: true};
    Cti.Topic(Cti.MessageType.AGENTLISTEN).publish(evt);
    $rootScope.$digest();
    
    expect(callback).toHaveBeenCalledWith(evt);
    scope.$destroy();
  });


  it('should reset last agent state when logging out', function() {

    var callback = jasmine.createSpy('callback');
    var scope = $rootScope.$new();

    // init XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish({});
    // Make agent Ready
    var evt = {agentId: 5, name: "AgentReady"};
    Cti.Topic(Cti.MessageType.AGENTSTATEEVENT).publish(evt);
    $rootScope.$digest();

    // Close XucLink
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    $rootScope.$digest();

    // Reinit without sending agent state
    Cti.Topic(Cti.MessageType.LOGGEDON).publish({});
    $rootScope.$digest();

    xucAgentUser.subscribeToAgentState(scope, callback);

    // Ensure we do not get the previous agent state
    expect(callback).not.toHaveBeenCalled();
    scope.$destroy();
  });

  it('list possible agent states based on current state', function() {
    var onPauseAndLoggedOut = [ { name: 'AgentOnPause', userStatus: null }, { name: 'AgentLoggedOut', userStatus: null } ];
    var agentStateCases = [
      {
        state: "AgentOnPause",
        expected: [ { name: 'AgentReady', userStatus: null }, { name: 'AgentLoggedOut', userStatus: null } ]
      },
      {
        state: "AgentOnWrapup",
        expected: [ { name: 'AgentReady', userStatus: null }, {name: 'AgentOnPause', userStatus: null}, { name: 'AgentLoggedOut', userStatus: null } ]
      },
      { state: "AgentReady", expected: onPauseAndLoggedOut },
      { state: "AgentOnCall", expected: onPauseAndLoggedOut },
      { state: "AgentDialing", expected: onPauseAndLoggedOut },
      { state: "AgentRinging", expected: onPauseAndLoggedOut },
      { state: "AgentLoggedOut", expected: [ { name: 'AgentLoggedOut', userStatus: null } ] },
      {
        state: "AgentLogin",
        expected: [ { name: 'AgentReady', userStatus: null }, { name: 'AgentLoggedOut', userStatus: null } ]
      }
    ];

    agentStateCases.forEach( agentStateCase => {

      var evt = {
        agentId: 5,
        name: agentStateCase.state,
        monitorState: "ACTIVE"
      };
      Cti.Topic(Cti.MessageType.AGENTSTATEEVENT).publish(evt);
      $rootScope.$digest();

      expect(xucAgentUser.getPossibleAgentStates()).toEqual(agentStateCase.expected);
    });

  });

  it('unpauses agent on ready state', function() {
    spyOn(Cti, 'unpauseAgent').and.callThrough();

    xucAgentUser.switchAgentState({name:'AgentReady', userStatus: null});
    expect(Cti.unpauseAgent).toHaveBeenCalled();
  });

  it('logs out agent on logout state', function() {
    spyOn(Cti, 'logoutAgent').and.callThrough();

    xucAgentUser.switchAgentState({name:'AgentLoggedOut', userStatus: null});
    expect(Cti.logoutAgent).toHaveBeenCalled();
  });

  it('pause agent on pause state without userStatus', function() {
    spyOn(Cti, 'pauseAgent').and.callThrough();

    xucAgentUser.switchAgentState({name:'AgentOnPause', userStatus: null});
    expect(Cti.pauseAgent).toHaveBeenCalledWith();
  });

  it('pause agent with reason if userStatus is in notReady statuses', function() {
    spyOn(Cti, 'pauseAgent').and.callThrough();
    var state = {userStatus: {name: 'test_reason'}};
    spyOn(xucAgent, 'getNotReadyStatuses').and.returnValue([state.userStatus]);
    
    xucAgentUser.switchAgentState(state);
    expect(Cti.pauseAgent).toHaveBeenCalledWith(undefined, state.userStatus.name);
  });

  it('unpause agent if userStatus is not in notReady statuses', function() {
    spyOn(Cti, 'unpauseAgent').and.callThrough();
    var state = {userStatus: {name: 'ready'}};
    spyOn(xucAgent, 'getNotReadyStatuses').and.returnValue([]);

    xucAgentUser.switchAgentState(state);
    expect(Cti.unpauseAgent).toHaveBeenCalledWith();
  });

});

