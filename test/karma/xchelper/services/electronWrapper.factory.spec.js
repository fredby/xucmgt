'use strict';

describe('Service: Electron wrapper', function () {

  var electronWrapper;
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));


  it('should know if web app is running without electron context', function() {
    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    expect(electronWrapper.isElectron()).toBe(false);
  });

  it('should know if web app is running with electron context', function() {
    window.setElectronConfig = () => {};

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    expect(electronWrapper.isElectron()).toBe(true);
    delete window.setElectronConfig;
  });


  it('should set electron config', function() {
    window.setElectronConfig = () => {};
    spyOn(window,'setElectronConfig');

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    electronWrapper.setElectronConfig(null, null, 'myTitle');
    expect(window.setElectronConfig).toHaveBeenCalledWith({title: 'myTitle'});

    electronWrapper.setElectronConfig(100, 200, null);
    expect(window.setElectronConfig).toHaveBeenCalledWith({size: {w: 100, h: 200, mini: false}});

    electronWrapper.setElectronConfig(100, 200, 'myTitle', true);
    expect(window.setElectronConfig).toHaveBeenCalledWith({size: {w: 100, h: 200, mini: true}, title: 'myTitle'});

    electronWrapper.setElectronConfig(null, 200, null);
    expect(window.setElectronConfig).toHaveBeenCalledWith({});

    delete window.setElectronConfig;
  });

  it('should focus electron window', function() {
    window.setElectronConfig = () => {};
    spyOn(window,'setElectronConfig');

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    electronWrapper.setFocus();
    expect(window.setElectronConfig).toHaveBeenCalledWith({focus: true});

    delete window.setElectronConfig;
  });

});
