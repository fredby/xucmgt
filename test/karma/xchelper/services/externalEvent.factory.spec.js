'use strict';

describe('Service: External event', function () {

  var externalEvent, XucPhoneState, CtiProxy, $rootScope, fakeEvent;
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject(function(_$location_) {
    spyOn(_$location_, 'host').and.returnValue('myxucmgtserver');
  }));

  beforeEach(angular.mock.inject(function(_externalEvent_,_XucPhoneState_, _CtiProxy_, _$rootScope_) {
    externalEvent = _externalEvent_;
    XucPhoneState = _XucPhoneState_;
    CtiProxy = _CtiProxy_;
    $rootScope = _$rootScope_;

    fakeEvent = {
      data: {type:'PHONE_EVENT', value:'123456'},
      origin: 'http://myxucmgtserver',
      source: 'global',
      target: 'global'
    };
    
  }));

  it('should not process event if empty or data is empty', function() {
    spyOn(CtiProxy,'dial');
    spyOn(CtiProxy,'hangup');
    spyOn(CtiProxy,'answer');
    externalEvent.handleEvent(null);
    expect(CtiProxy.dial).not.toHaveBeenCalled();
    expect(CtiProxy.hangup).not.toHaveBeenCalled();
    expect(CtiProxy.answer).not.toHaveBeenCalled();

    fakeEvent = {
      data: null,
      origin: 'http://myxucmgtserver',
      source: 'global',
      target: 'global'
    };
    externalEvent.handleEvent(fakeEvent);
    expect(CtiProxy.dial).not.toHaveBeenCalled();
    expect(CtiProxy.hangup).not.toHaveBeenCalled();
    expect(CtiProxy.answer).not.toHaveBeenCalled();
  });

  it('should not process event if origin or source are different to target ones', function() {
    spyOn(CtiProxy,'dial');
    spyOn(CtiProxy,'hangup');
    spyOn(CtiProxy,'answer');

    fakeEvent = {
      data: {type:'PHONE_EVENT', value:'123456'},
      origin: 'http://somewhere',
      source: 'global',
      target: 'global'
    };
    externalEvent.handleEvent(fakeEvent);
    expect(CtiProxy.dial).not.toHaveBeenCalled();
    expect(CtiProxy.hangup).not.toHaveBeenCalled();
    expect(CtiProxy.answer).not.toHaveBeenCalled();

    var fakeAnotherEvent = {
      data: {type:'PHONE_EVENT', value:'123456'},
      origin: 'http://myxucmgtserver',
      source: 'not global',
      target: 'global'
    };
    externalEvent.handleEvent(fakeAnotherEvent);
    expect(CtiProxy.dial).not.toHaveBeenCalled();
    expect(CtiProxy.hangup).not.toHaveBeenCalled();
    expect(CtiProxy.answer).not.toHaveBeenCalled();
  });

  it('should not process event if Message type in event is not known', function() {
    spyOn(CtiProxy,'dial');
    spyOn(CtiProxy,'hangup');
    spyOn(CtiProxy,'answer');

    fakeEvent = {
      data: {type:'UNKNOWN_EVENT', value:'123456'},
      origin: 'http://myxucmgtserver',
      source: 'global',
      target: 'global'
    };
    externalEvent.handleEvent(fakeEvent);
    expect(CtiProxy.dial).not.toHaveBeenCalled();
    expect(CtiProxy.hangup).not.toHaveBeenCalled();
    expect(CtiProxy.answer).not.toHaveBeenCalled();
  });

  it('should dial when message event is received and no phone state', function(done) {

    spyOn(CtiProxy,'dial').and.callFake(function() {
      expect(CtiProxy.dial).toHaveBeenCalledWith("123456");
      done();
    });

    externalEvent.handleEvent(fakeEvent);
    $rootScope.$digest();
  });

  it('should hangup when message event is received and phone state is off hook', function(done) {
    spyOn(XucPhoneState, "getState").and.returnValue("Dialing");

    spyOn(CtiProxy,'hangup').and.callFake(function() {
      expect(CtiProxy.hangup).toHaveBeenCalled();
      done();
    });

    externalEvent.handleEvent(fakeEvent);
  });

  it('should answer when message event is received and phone state is ringing', function(done) {
    spyOn(XucPhoneState, "getState").and.returnValue("Ringing");

    spyOn(CtiProxy,'answer').and.callFake(function() {
      expect(CtiProxy.answer).toHaveBeenCalled();
      done();
    });

    externalEvent.handleEvent(fakeEvent);
  });

  it('should apply callback when third party event', function() {
    var cb = jasmine.createSpy();
    externalEvent.registerThirdPartyCallback(cb);

    var fakeThirdPartyEvent = {
      data: {type:'THIRD_PARTY_EVENT', value:''},
      origin: 'http://myxucmgtserver',
      source: 'global',
      target: 'global'
    };

    externalEvent.handleEvent(fakeThirdPartyEvent);
    expect(cb).toHaveBeenCalled();
  });


  it('should transform legacy closeThirdParty DOM Event to Message', function() {
    var cbl = jasmine.createSpy();
    externalEvent.registerThirdPartyCallback(cbl);

    var fakeLegacyThirdPartyEvent = {
      data: 'closeThirdParty',
      origin: 'http://myxucmgtserver',
      source: 'global',
      target: 'global'
    };

    externalEvent.handleEvent(fakeLegacyThirdPartyEvent);
    expect(cbl).toHaveBeenCalled();
  });

});
