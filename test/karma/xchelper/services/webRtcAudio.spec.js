describe('webRtcAudio', function() {

  var incomingCallAudioSpy;
  var incomingSecondCallAudioSpy;
  var ringingAudioSpy;
  var onPhoneEvent;
  var hangupAudioSpy;
  var webRtcAudio;
  var XucPhoneState;
  

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(function() {
    angular.mock.module('xcCti');
    angular.mock.module('xcHelper');
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$q_) {
    var audioMethods = ['play', 'pause', 'load', 'setSinkId'];
    incomingCallAudioSpy = jasmine.createSpyObj('incomingCallAudio', audioMethods);
    incomingSecondCallAudioSpy = jasmine.createSpyObj('incomingSecondCallAudio', audioMethods);
    incomingCallAudioSpy.setSinkId.and.returnValue(_$q_.resolve());
    ringingAudioSpy = jasmine.createSpyObj('ringingAudio', audioMethods);
    hangupAudioSpy = jasmine.createSpyObj('hangupAudio', audioMethods);
    document.getElementById = function(id) {
      switch(id) {
      case 'webrtc_audio_incoming_call':
        return incomingCallAudioSpy;
      case 'webrtc_audio_incoming_second_call':
        return incomingSecondCallAudioSpy;
      case 'webrtc_audio_ringing':
        return ringingAudioSpy;
      case 'webrtc_audio_hangup_call':
        return hangupAudioSpy;
      default:
        return null;
      }
    };
  }));

  beforeEach(angular.mock.inject(function(XucPhoneEventListener, xcHelperPreferences) {
    spyOn(XucPhoneEventListener, 'addHandlerCustom').and.callFake(function(handler) {
      onPhoneEvent = handler;
    });

    spyOn(xcHelperPreferences, 'getRingingDeviceId').and.returnValue('05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9');
  }));

  beforeEach(angular.mock.inject(function(_webRtcAudio_, _XucPhoneState_) {
    webRtcAudio = _webRtcAudio_;
    XucPhoneState = _XucPhoneState_;
  }));

  function getEvent(eventType) {
    return {
      "eventType": eventType,
      "DN":"1000",
      "otherDN":"1001",
      "linkedId":"1471858488.233",
      "uniqueId":"1471858490.236",
      "userData": {}
    };
  }

  var expectStopped = function(audioSpy) {
    expect(audioSpy.pause).toHaveBeenCalled();
  };

  var expectPlayed = function(audioSpy) {
    expect(audioSpy.play).toHaveBeenCalled();
  };

  it('can instanciate service', () => {
    expect(webRtcAudio).not.toBeUndefined();
  });

  it('is disabled by default', () => {
    expect(webRtcAudio.isEnabled()).toBe(false);
  });

  it('can be enabled and disabled (with audio stop)', () => {
    webRtcAudio.enable();
    expect(webRtcAudio.isEnabled()).toBe(true);
    webRtcAudio.disable();
    expect(webRtcAudio.isEnabled()).toBe(false);
    expectStopped(incomingCallAudioSpy);
    expectStopped(ringingAudioSpy);
  });

  it('on EventDialing play ringing audio', () => {
    webRtcAudio.enable();
    onPhoneEvent(getEvent('EventDialing'));
    expectStopped(incomingCallAudioSpy);
    expectPlayed(ringingAudioSpy);
  });

  it('on EventRinging play incoming call audio', () => {
    webRtcAudio.enable();
    onPhoneEvent(getEvent('EventRinging'));
    expectStopped(ringingAudioSpy);
    expectPlayed(incomingCallAudioSpy);
  });

  it('on other phone event stop both audio', () => {
    webRtcAudio.enable();
    onPhoneEvent(getEvent('EventEstablished'));
    expectStopped(ringingAudioSpy);
    expectStopped(incomingCallAudioSpy);
  });

  it('on EventReleased play hangup audio', () => {
    webRtcAudio.enable();
    onPhoneEvent(getEvent('EventReleased'));
    expectStopped(incomingCallAudioSpy);
    expectStopped(ringingAudioSpy);
    expectPlayed(hangupAudioSpy);
  });

  it('on EventRinging play second incoming call audio if already ongoing call', () => {
    spyOn(XucPhoneState, 'isPhoneOffHook').and.returnValue(true);
    webRtcAudio.enable();
    onPhoneEvent(getEvent('EventRinging'));
    expectStopped(ringingAudioSpy);
    expectPlayed(incomingSecondCallAudioSpy);
  });
  
  it('set ringing device id from localStorage', angular.mock.inject((webRtcAudio) => {
    expect(incomingCallAudioSpy.setSinkId).toHaveBeenCalledWith('05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9');
  }));

  it('change the ringing device id', angular.mock.inject((webRtcAudio) => {
    webRtcAudio.changeRingingDevice('default');
    expect(incomingCallAudioSpy.setSinkId).toHaveBeenCalledWith('default');
  }));

  it('do not throw exception when setSinkId is not defined', angular.mock.inject((webRtcAudio) => {
    delete incomingCallAudioSpy.setSinkId;
    
    try {
      webRtcAudio.changeRingingDevice('default');
    } catch(e) {
      fail("Got unexpected exception " + e.getMessage());
    }
  }));

});
