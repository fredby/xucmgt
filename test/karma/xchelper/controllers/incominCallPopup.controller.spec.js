describe('Incoming call popup controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var XucPhoneEventListener;
  var establishedHandler;
  var releasedHandler;
  var CtiProxy;
  var modalCloseMethod;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _XucPhoneEventListener_, _CtiProxy_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    XucPhoneEventListener = _XucPhoneEventListener_;
    CtiProxy = _CtiProxy_;
    spyOn(XucPhoneEventListener, 'addEstablishedHandler').and.callFake(function(scope, handler) {
      establishedHandler = handler;
    });
    spyOn(XucPhoneEventListener, 'addReleasedHandler').and.callFake(function(scope, handler) {
      releasedHandler = handler;
    });

    modalCloseMethod = jasmine.createSpy();
    ctrl = $controller('IncomingCallPopup', {
      '$scope' : $scope,
      '$uibModalInstance': {dismiss: modalCloseMethod},
      'incomingCallNumber': '23',
      'incomingCallName': 'incName',
      'callType': xc_webrtc.mediaType.AUDIOVIDEO
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('Registers callbacks on init', function() {
    expect(XucPhoneEventListener.addEstablishedHandler).toHaveBeenCalled();
    expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
  });

  it('closes on established', function() {
    establishedHandler();
    expect(modalCloseMethod).toHaveBeenCalledWith('autoclose');
  });

  it('closes on release', function() {
    releasedHandler();
    expect(modalCloseMethod).toHaveBeenCalledWith('autoclose');
  });

  it('answers and close', function() {
    spyOn(CtiProxy, 'answer');
    $scope.accept();
    expect(CtiProxy.answer).toHaveBeenCalled();
    expect(modalCloseMethod).toHaveBeenCalledWith('accept');
  });

  it('hangup and close', function() {
    spyOn(CtiProxy, 'hangup');
    $scope.decline();
    expect(CtiProxy.hangup).toHaveBeenCalled();
    expect(modalCloseMethod).toHaveBeenCalledWith('decline');
  });

  it('should say if the call is video', function(){
    expect($scope.isVideo()).toBe(true);
  });
});
