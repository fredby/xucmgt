describe('CallAction controller', function() {
  var scope;
  var ctrl;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    ctrl = $controller('CallAction', { '$scope': scope});
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

});
