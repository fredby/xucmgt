describe('Call action controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var xucPhoneState;
  var xucUtils;
  var CtiProxy;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('ui.router'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _XucPhoneState_, _XucUtils_, _$state_, _CtiProxy_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $scope.contact = {
      entry: ['James Bond', "1000", "1001", "1002", "", "jbond@mi6.gov.uk"]
    };
    xucPhoneState = _XucPhoneState_;
    xucUtils = _XucUtils_;
    CtiProxy = _CtiProxy_;

    ctrl = $controller('CallAction', {
      '$scope' : $scope,
      '$rootScope' : $rootScope,
      'xucPhoneState':xucPhoneState,
      'xucUtils': xucUtils,
      '$state': _$state_,
      'CtiProxy': CtiProxy
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('should normalize number on dial', function(){
    spyOn(CtiProxy,'dial');
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue("0033789456212");

    $scope.dial("+33789456212", {}, false);

    expect(xucUtils.normalizePhoneNb).toHaveBeenCalled();
    expect(CtiProxy.dial).toHaveBeenCalledWith("0033789456212", {}, false);
  });
  it('should propagate parameters of dial', function(){
    spyOn(CtiProxy,'dial');
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue("0033789456212");

    $scope.dial("+33789456212", {a: 'test'}, true);

    expect(xucUtils.normalizePhoneNb).toHaveBeenCalled();
    expect(CtiProxy.dial).toHaveBeenCalledWith("0033789456212", {a: 'test'}, true);
  });
  it('should fallback on false when video parameter is omitted', function(){
    spyOn(CtiProxy,'dial');
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue("0033789456212");

    $scope.dial("+33789456212", {a: 'test'});

    expect(xucUtils.normalizePhoneNb).toHaveBeenCalled();
    expect(CtiProxy.dial).toHaveBeenCalledWith("0033789456212", {a: 'test'}, false);
  });


  it('should normalize number on attended Transfer', function(){
    spyOn(CtiProxy,'attendedTransfer');
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue("0033789456212");

    $scope.attendedTransfer("+33789456212");

    expect(xucUtils.normalizePhoneNb).toHaveBeenCalled();
    expect(CtiProxy.attendedTransfer).toHaveBeenCalledWith("0033789456212");
  });
  it('should normalize number on direct Transfer', function(){
    spyOn(CtiProxy,'directTransfer');
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue("0033789456212");

    $scope.directTransfer("+33789456212");

    expect(xucUtils.normalizePhoneNb).toHaveBeenCalled();
    expect(CtiProxy.directTransfer).toHaveBeenCalledWith("0033789456212");
  });
});
