describe('fullscreenable directive', function() {

  var elem, $scope, ctrl;

  var mockRequestFullScreen = function() {};

  beforeEach(angular.mock.module('xcHelper'));

  var digestAndDismissErrors = (scope) => {
    try { scope.$digest(); } catch(err) { angular.noop; }
  };

  beforeEach(angular.mock.inject(function($compile, $rootScope) {
    elem = angular.element('<fullscreenable></fullscreenable>');
    $compile(elem)($rootScope.$new());
    digestAndDismissErrors($scope);
    ctrl = elem.controller('fullscreenable');
    $scope = elem.scope();
  }));

  beforeEach(function() {
    $scope.elem.webkitRequestFullscreen = mockRequestFullScreen;
    $scope.elem.mozRequestFullScreen = mockRequestFullScreen;
  });

  it('can instantiate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('sets element to scope', function() {
    expect($scope.elem).toBe(elem[0]);
  });

  it('toggles fullscreen - go fullscreen', function() {
    $scope._document = [{webkitIsFullScreen: false}];
    spyOn($scope.elem, 'webkitRequestFullscreen');
    $scope.toggleFullscreen();
    expect($scope.elem.webkitRequestFullscreen).toHaveBeenCalled();
  });

  it('toggles fullscreen - exit fullscreen', function() {
    $scope._document = [{webkitIsFullScreen: true, webkitExitFullscreen: angular.noop}];
    spyOn($scope._document[0], 'webkitExitFullscreen');
    $scope.toggleFullscreen();
    expect($scope._document[0].webkitExitFullscreen).toHaveBeenCalled();
  });

});
