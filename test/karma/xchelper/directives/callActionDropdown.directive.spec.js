import _ from 'lodash';

describe('callActionDropdown directive', function() {

  var scope;
  var directiveScope;
  var sampleContact = {
    "status": 0,
    "entry": [
      "aaa2 aaa2",
      "1009",
      "2000",
      "3000",
      false,
      "noreply@avencall.com"
    ],
    "contact_id": "69",
    "source": "internal",
    "favorite": false
  };

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });

    angular.mock.inject(function ($compile, $rootScope) {
      scope = $rootScope.$new();
      scope.contact = sampleContact;
      scope.phoneNumber = "bar";

      var elementStr = angular.element('<call-action-dropdown contact="contact" phoneNumber="123"></call-action-dropdown>');
      var element = $compile(elementStr)(scope);
      scope.$digest();
      directiveScope = element.children().scope();
    });
  });

  it('bind directive', function() {
    expect(_.isArray(directiveScope.phoneNumbers)).toBe(true);
  });

  it('extract phone numbers from contact', function() {
    expect(directiveScope.phoneNumbers).toEqual(["1009", "2000", "3000"]);
  });

  it('extract email from contact', function() {
    expect(directiveScope.email).toEqual('noreply@avencall.com');
  });

});