'use strict';

describe('Service: User rights', function () {

    var userRights;
    beforeEach(angular.mock.module('xcCti'));
    beforeEach(angular.mock.module('xcLogin'));

    beforeEach(angular.mock.inject(function(_userRights_) {
        userRights = _userRights_;
    }));

    it('checks right to ccManager', function() {
      expect(userRights.allowCCManager('Admin')).toEqual(true);
      expect(userRights.allowCCManager('Supervisor')).toEqual(true);
      expect(userRights.allowCCManager('NoRightForUser')).toEqual(false);
    });

});
