import 'xccti/cti-webpack';
import 'xccti/services/XucQueue.service';
import 'xccti/services/XucAgentUser.service';

describe('Service: activities helper', function() {
  var $rootScope;
  var xucQueue;
  var $q;
  var xucAgentUser;
  var allQueues;
  var agentPreferences;
  var agentCallbackHelper;
  var agentActivitiesHelper;
  var xucQueueTotal;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _agentPreferences_, _XucQueue_, _$q_, _XucAgentUser_, _XucQueueTotal_, _localStorageService_, _agentCallbackHelper_, _agentActivitiesHelper_) {
    $rootScope = _$rootScope_;
    xucQueue = _XucQueue_;
    xucQueueTotal = _XucQueueTotal_;
    $q = _$q_;
    xucAgentUser = _XucAgentUser_;
    agentPreferences = _agentPreferences_;
    agentCallbackHelper = _agentCallbackHelper_;
    agentActivitiesHelper = _agentActivitiesHelper_;

    _localStorageService_.clearAll();

    // Create two queues with some fake information
    allQueues = [{
      'id': 1,
      'number': '3000',
      'displayName': 'My queue',
      'associated': false,
      'WaitingCalls': 1,
      'LongestWaitTime': 25,
      'AvailableAgents': 1,
      'EWT': 10
    }, {
      'id': 2,
      'number': '3001',
      'displayName': 'Another queue',
      'associated': true,
      'WaitingCalls': 1,
      'LongestWaitTime': 50,
      'AvailableAgents': 2
    }];

  }));


  it('display all queues when not filtering on "My activities"', function(done) {
    agentActivitiesHelper.setQueueFilter(false);
    var queues = $q.defer();
    queues.resolve(allQueues);

    spyOn(xucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });
    spyOn(xucQueueTotal, 'calculate');

    var p = agentActivitiesHelper.loadQueues();
    $rootScope.$digest();

    p.then(activities => {
      expect(activities[0].id).toEqual(1);
      expect(activities[1].id).toEqual(2);
      expect(xucQueueTotal.calculate).toHaveBeenCalled();
      done();
    });
  });

  it('display only my queues when filtering on "My activities"', function(done) {
    agentActivitiesHelper.setQueueFilter(true);

    var userQueues = $q.defer();
    userQueues.resolve([{
      'id' : 1,
      'number': '3000',
      'displayName': 'My queue',
      'WaitingCalls': 1,
      'LongestWaitTime':25,
      'AvailableAgents':1
    }]);

    spyOn(xucAgentUser, "getQueuesAsync").and.callFake(() => {
      return userQueues.promise;
    });
    spyOn(xucQueueTotal, 'calculate');

    var p = agentActivitiesHelper.loadQueues();
    $rootScope.$digest();

    p.then(activities => {
      expect(activities[0].id).toEqual(1);
      expect(activities.length).toEqual(1);
      expect(xucQueueTotal.calculate).toHaveBeenCalled();
      done();
    });
  });

  it('subscribe or remove one queue', function() {
    spyOn(xucAgentUser, "joinQueueAsync").and.callThrough();
    spyOn(xucAgentUser, "leaveQueueAsync").and.callThrough();

    agentActivitiesHelper.enterOrLeaveQueue(allQueues[0]);
    expect(xucAgentUser.joinQueueAsync.calls.count()).toEqual(1);
    expect(xucAgentUser.leaveQueueAsync).not.toHaveBeenCalled();
    agentActivitiesHelper.enterOrLeaveQueue(allQueues[1]);
    expect(xucAgentUser.leaveQueueAsync.calls.count()).toEqual(1);
    expect(xucAgentUser.joinQueueAsync.calls.count()).toEqual(1);
  });

  it('adds queue to favorites', function() {
    spyOn(agentPreferences, "addFavoriteQueue");
    agentActivitiesHelper.addFavoriteQueue(allQueues[0]);
    expect(agentPreferences.addFavoriteQueue).toHaveBeenCalledWith(1);
  });

  it('removes queue from favorites', function() {
    spyOn(agentPreferences, "removeFavoriteQueue");
    agentActivitiesHelper.removeFavoriteQueue(allQueues[0]);
    expect(agentPreferences.removeFavoriteQueue).toHaveBeenCalledWith(1);
  });

  it('loads callbacks when entering a queue', function() {
    var queue ={
      'id' : 1,
      'number': '3000',
      'displayName': 'My queue',
      'WaitingCalls': 1,
      'LongestWaitTime':25,
      'AvailableAgents':1
    };

    var userQueue = $q.defer();
    userQueue.resolve(queue);

    spyOn(xucAgentUser, "joinQueueAsync").and.callFake(() => {
      return userQueue.promise;
    });
    spyOn(agentCallbackHelper, "loadAllCallbacks");
    spyOn(agentActivitiesHelper, "loadQueues").and.callFake(() => {});

    agentActivitiesHelper.enterOrLeaveQueue(queue);
    $rootScope.$digest();
    expect(agentCallbackHelper.loadAllCallbacks).toHaveBeenCalled();
  });

  it('loads callbacks when leaving a queue', function() {
    var queue ={
      'id' : 1,
      'number': '3000',
      'displayName': 'My queue',
      'WaitingCalls': 1,
      'LongestWaitTime':25,
      'AvailableAgents':1,
      'associated': true,
    };

    var userQueue = $q.defer();
    userQueue.resolve(queue);

    spyOn(xucAgentUser, "leaveQueueAsync").and.callFake(() => {
      return userQueue.promise;
    });
    spyOn(agentCallbackHelper, "loadAllCallbacks");
    spyOn(agentActivitiesHelper, "loadQueues").and.callFake(() => {});

    agentActivitiesHelper.enterOrLeaveQueue(queue);
    $rootScope.$digest();
    expect(agentCallbackHelper.loadAllCallbacks).toHaveBeenCalled();
  });

});
