describe('Third party integration controller', function() {
  var $rootScope;
  var $scope;
  var $controller;
  var $q;
  var configuration;
  var XucThirdParty;
  var externalEvent;
  var $window;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _$q_, _configuration_, _XucThirdParty_, _externalEvent_, _$window_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $controller = _$controller_;
    $q = _$q_;
    configuration = _configuration_;
    XucThirdParty = _XucThirdParty_;
    externalEvent = _externalEvent_;
    $window = _$window_;
  }));

  function createController() {
    return $controller('ThirdPartyController', {
      '$scope': $scope,
      'configuration': configuration,
      'XucThirdParty': XucThirdParty,
      'externalEvent' : externalEvent,
      '$window': $window
    });
  }

  it('can instanciate controller', function() {
    spyOn(XucThirdParty, 'setThirdPartyWs');

    var ctrl = createController();
    expect(ctrl).not.toBeUndefined();
    expect(XucThirdParty.setThirdPartyWs).not.toHaveBeenCalled();
  });

  it('subscribe to third party service if 3rd party url is defined', function() {
    spyOn(XucThirdParty, 'setThirdPartyWs');
    spyOn(XucThirdParty, 'addActionHandler');
    spyOn(XucThirdParty, 'addClearHandler');
    spyOn(externalEvent, 'registerThirdPartyCallback');

    var url = $q.defer();
    url.resolve('localhost');
    spyOn(configuration, 'get').and.returnValue(url.promise);

    createController();
    $scope.$apply();

    expect(XucThirdParty.setThirdPartyWs).toHaveBeenCalledWith('localhost');
    expect(externalEvent.registerThirdPartyCallback).toHaveBeenCalled();
    expect(XucThirdParty.addActionHandler).toHaveBeenCalled();
    expect(XucThirdParty.addClearHandler).toHaveBeenCalled();
  });

  it('set action if open action is received in event', function() {
    var ctrl = createController();
    const ev = {action: 'open'};

    ctrl.onThirdPartyAction({});
    expect($scope.action).toBeUndefined();
    ctrl.onThirdPartyAction(ev);
    expect($scope.action).toBe(ev);
  });

  it('open popup instead of embedding the page when configured to do so ', function() {
    spyOn($window, 'open');
    var ctrl = createController();
    const ev = {action: 'popup', url: 'http://localhost/23678'};

    ctrl.onThirdPartyAction(ev);
    expect($window.open).toHaveBeenCalledWith('http://localhost/23678', 'popup');
  });

  it('open multiple popup when configured to do so ', function() {
    let lastWindowName;
    spyOn($window, 'open').and.callFake((url, windowName) => {
      lastWindowName = windowName;
    });
    
    var ctrl = createController();
    const ev = {action: 'popup', url: 'http://localhost/23678', multitab: true};

    ctrl.onThirdPartyAction(ev);
    let firstWindowName = lastWindowName;
    expect($window.open).toHaveBeenCalled();

    ctrl.onThirdPartyAction(ev);
    let secondWindowName = lastWindowName;
    expect($window.open).toHaveBeenCalled();
    expect(firstWindowName).not.toEqual(secondWindowName);
  });

  it('clear action if received in event(s)', function() {
    spyOn(XucThirdParty, 'clearAction');

    var ctrl = createController();
    const evMsg = {data: 'closeThirdParty'};

    ctrl.onMessageReceived(evMsg);
    expect(XucThirdParty.clearAction).toHaveBeenCalled();

    ctrl.onThirdPartyClearAction();
    expect($scope.action).toBe(null);
  });

});
