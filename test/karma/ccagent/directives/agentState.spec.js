describe('agentState directive', () => {
  var $compile;
  var $rootScope;
  var XucLink;
  var XucAgentUser;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_, _XucLink_, _XucAgentUser_) =>{
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    XucLink = _XucLink_;
    XucAgentUser = _XucAgentUser_;
  }));


  it('should logout when AgentLoggedOut state is received and agent is already logged in ', () => {
    spyOn(XucLink, 'logout');

    var scope = $rootScope.$new();
    $compile('<agent-state></agent-state>')(scope);
    $rootScope.$digest();

    scope.checkLogout({name: 'AgentLoggedOut'});
    expect(scope.loggedIn).toBe(false);
    expect(XucLink.logout).not.toHaveBeenCalled();

    scope.checkLogout({name: 'AgentLogin'});
    expect(scope.loggedIn).toBe(true);
    expect(XucLink.logout).not.toHaveBeenCalled();

    scope.checkLogout({name: 'AgentLoggedOut'});
    expect(scope.loggedIn).toBe(false);
    expect(XucLink.logout).toHaveBeenCalled();
  });

  it('should check if needs to logout when agent switching state', () => {
    spyOn(XucAgentUser,'switchAgentState');

    var scope = $rootScope.$new();
    $compile('<agent-state></agent-state>')(scope);
    $rootScope.$digest();

    spyOn(scope, 'checkLogout');
    scope.switchState({name: 'AgentLoggedOut'});
    expect(scope.checkLogout).toHaveBeenCalled();
  });

  it('should return a countdown object time in state', () => {
    var elem = angular.element('<agent-state></agent-state>');
    var scope = $rootScope.$new();
    $compile(elem)(scope);
    $rootScope.$digest();

    scope.refreshState();
    expect(scope.agentState.timeInState.toString()).toBeDefined();
  });
});
