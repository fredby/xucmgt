import angular from 'angular';
import _ from 'lodash';
import 'ccagent/app';

describe('callConference directive', () => {
  var $compile;
  var $rootScope;
  var $scope;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) =>{
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
  }));

  function checkActionsForState(state, expectedActions, injectedScope) {
    $scope.canRecord = false;
    
    $scope.conference = {
      "calls": [{
        "linkedId": "1498806157.760",
        "direction": "Outgoing",
        "acd": false,
        "state": state,
        "uniqueId": "1498806157.760",
        "DN": "1001",
        "otherDN": "1002",
        "userData": {
          "XIVO_CONTEXT": "default",
          "XIVO_USERID": "2",
          "XIVO_SRCNUM": "1001",
          "XIVO_DST_FIRSTNAME": "user",
          "XIVO_DSTNUM": "1002",
          "XIVO_DST_LASTNAME": "C"
        },
        "otherDName": "userC",
        "startTime": 1498806161961
      }, {
        "linkedId": "1498806165.762",
        "direction": "Outgoing",
        "acd": false,
        "state": state,
        "uniqueId": "1498806165.762",
        "DN": "1001",
        "otherDN": "1000",
        "userData": {
          "XIVO_CONTEXT": "default",
          "XIVO_USERID": "2",
          "XIVO_SRCNUM": "1001",
          "XIVO_DST_FIRSTNAME": "user",
          "XIVO_DSTNUM": "1000",
          "XIVO_DST_LASTNAME": "A"
        },
        "otherDName": "userA",
        "startTime": 1498806167811
      }],
      "active": true,
      "startTime": 1498806171905
    };
    
    var element = $compile('<call-conference></call-conference>')($scope);
    
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();

    if(typeof(injectedScope) !== "undefined") {
      _.merge(isolatedScope, injectedScope);
    }
    
    var actions = _.map(isolatedScope.getCallActions(), (item) => {
      return item.label;
    });
    
    _.forEach(expectedActions, function(item) {
      expect(actions).toContain(item);
    });

    expect(actions.length).toBe(expectedActions.length);
  }
  
  it('should display hold and hangup when conference is established', () => {
    $scope.canRecord = false;
    checkActionsForState('Established', ['hold', 'hangup']);
  });

  it('should display stop record when conference is established and recording is active', () => {
    $scope.canRecord = true;
    $scope.recording = true;
    checkActionsForState('Established', ['hold', 'hangup', 'stoprecording'], { canRecord: true, recording: true });
  });

  it('should display resume record when conference is established and recording is paused', () => {
    $scope.canRecord = true;
    $scope.recording = false;
    checkActionsForState('Established', ['hold', 'hangup', 'startrecording'], { canRecord: true, recording: false });
  });

});
