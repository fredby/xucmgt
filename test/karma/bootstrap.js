var karmaBackend = angular.module('karma-backend', ['ngMockE2E']);

karmaBackend.run(function($httpBackend) {
  var loginPage = '<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="error" use-sso="useSso" title="Login"/>';

  $httpBackend.whenGET('/ccmanager/login').respond(loginPage);
  $httpBackend.whenGET('/ucassistant/login.html').respond(loginPage);
  $httpBackend.whenGET(/^assets\/i18n\//).passThrough();
});
