describe('volumeMeter directive', () => {
  var $compile;
  var $rootScope;
  var processVolume;
  var el;
  var scope;
  var audioDirective = "<volume-meter mic-or-audio='audio' sip-call-id='121.124.45@192.168'></volume-meter>";
  var micDirective = "<volume-meter mic-or-audio='mic' sip-call-id='121.124.45@192.168'></volume-meter>";

  function compile(directive) {
    el = angular.element(directive);
    $compile(el)($rootScope.$new());
    $rootScope.$digest();
    scope = el.isolateScope() || el.scope();
  }

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_, _processVolume_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    processVolume = _processVolume_;
  }));

  beforeEach(function() {
    spyOn(processVolume, 'connect').and.callFake(function(){});
    spyOn(processVolume, 'stopProcess').and.callFake(function(){});
    spyOn(processVolume, 'setVolumeHandler').and.callFake(function(){});
    spyOn(processVolume, 'setEventTypeHandler').and.callFake(function(){});
    spyOn(processVolume, 'setLocalStreamHandler').and.callFake(function(){});
    spyOn(processVolume, 'setRemoteStreamHandler').and.callFake(function(){});
    spyOn(processVolume, 'setProcess').and.callFake(function(){});
  });

  it('should set a stream handler with a sip call id', () => {
    compile(audioDirective);
    expect(processVolume.setRemoteStreamHandler.calls.all()[0].args[0]).toBe(scope.setAndConnect);
    expect(processVolume.setRemoteStreamHandler.calls.all()[0].args[1]).toEqual('121.124.45@192.168');
    processVolume.setRemoteStreamHandler.calls.reset();
    compile(micDirective);
    expect(processVolume.setLocalStreamHandler.calls.all()[0].args[0]).toBe(scope.setAndConnect);
    expect(processVolume.setLocalStreamHandler.calls.all()[0].args[1]).toEqual('121.124.45@192.168');
  });

  it('should set a volume handler with an audio or mic id', () => {
    compile(audioDirective);
    expect(processVolume.setVolumeHandler.calls.all()[0].args[0]).toBe(scope.setVolume);
    expect(processVolume.setVolumeHandler.calls.all()[0].args[1]).toEqual('audio121.124.45@192.168');
    processVolume.setVolumeHandler.calls.reset();
    compile(micDirective);
    expect(processVolume.setVolumeHandler.calls.all()[0].args[0]).toBe(scope.setVolume);
    expect(processVolume.setVolumeHandler.calls.all()[0].args[1]).toEqual('mic121.124.45@192.168');
  });

  it('should set an event type handler with a sip call id', () => {
    compile(audioDirective);
    expect(processVolume.setEventTypeHandler.calls.all()[0].args[0]).toBe(scope.onHoldAndResume);
    expect(processVolume.setEventTypeHandler.calls.all()[0].args[1]).toEqual('121.124.45@192.168');
  });
});
