import 'xccti/cti-webpack';

describe('Factory: Process volume', function () {
  var xcWebrtcMock;
  var oldXcWebrtc;
  var xcWebrtcMethods = ['setHandler', 'getAudioContext', 'setLocalStreamHandler', 'setRemoteStreamHandler'];
  var processVolume;
  var volumePack;

  var context = new AudioContext();
  var sipCallId = "456546-df456@192.168";
  var id = "mic" + sipCallId;
  
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(function() {
    xcWebrtcMock = jasmine.createSpyObj('xc_webrtc', xcWebrtcMethods);
    xcWebrtcMock.MessageType = xc_webrtc.MessageType;
    oldXcWebrtc = xc_webrtc;
    xc_webrtc = xcWebrtcMock;
  });

  beforeEach(angular.mock.inject(function(_processVolume_) {
    processVolume = _processVolume_;
  }));

  beforeEach(function() {
    volumePack = {'processor': context.createScriptProcessor() ,'analyser': context.createAnalyser()};
  });

  afterEach(function() {
    xc_webrtc = oldXcWebrtc;
  });

  it('provides the xc_webrtc\'s audio context', function() {
    processVolume.getAudioContext();
    expect(xc_webrtc.getAudioContext).toHaveBeenCalled();
  });

  it('returns the unregister method of volumeHandlers on registration', function() {
    var handler = jasmine.createSpy();
    var unregisterMethod = processVolume.setVolumeHandler(handler, id);
    var result = unregisterMethod();
    expect(result).toBe(handler);
  });

  it('sets an on audioprocess that retrieves a volume for a right id', function() {
    var handler = jasmine.createSpy();
    var unusedHandler = jasmine.createSpy();
    processVolume.setVolumeHandler(handler, id);
    processVolume.setVolumeHandler(handler, 'mic45@192.168');
    processVolume.setProcess(volumePack, id);
    volumePack.processor.onaudioprocess();
    expect(handler).toHaveBeenCalledWith(0);
    expect(unusedHandler).not.toHaveBeenCalled();
  });

  it('registers xc_webrtc\'s event handler and publish event for a right sipCallId', function(){
    var handler = jasmine.createSpy();
    processVolume.setEventTypeHandler(handler, sipCallId);
    expect(xc_webrtc.setHandler.calls.all()[0].args[0]).toBe(xc_webrtc.MessageType.INCOMING);
    expect(xc_webrtc.setHandler.calls.all()[1].args[0]).toBe(xc_webrtc.MessageType.OUTGOING);
  });

  it('registers xc_webrtc\'s stream handler and publish event for a right sipCallId', function(){
    var handler = jasmine.createSpy();
    processVolume.setLocalStreamHandler(handler, sipCallId);
    expect(xc_webrtc.setLocalStreamHandler.calls.all()[0].args[0]).toBe(handler);
    expect(xc_webrtc.setLocalStreamHandler.calls.all()[0].args[1]).toBe("456546-df456@192.168");
  });
});
