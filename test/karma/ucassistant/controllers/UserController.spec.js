describe('UserController', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var XucAgentUser;
  var $translate;
  var XucLink;
  var CtiProxy;
  var forward;

  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _XucAgentUser_, _$translate_, _XucLink_, _CtiProxy_, _forward_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    XucAgentUser = _XucAgentUser_;
    $translate = _$translate_;
    XucLink = _XucLink_;
    CtiProxy = _CtiProxy_;
    forward = _forward_;
    ctrl = $controller('UserController', {
      '$scope' :      $scope,
      'XucAgentUser':      XucAgentUser,
      '$translate':   $translate,
      'XucLink':      XucLink,
      'CtiProxy':     CtiProxy
    });
  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('on logout stop XucLink and WebRtc', function() {
    spyOn(XucLink, 'logout');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(CtiProxy, 'stopUsingWebRtc');

    $scope.logout();

    expect(XucLink.logout).toHaveBeenCalled();
    expect(CtiProxy.stopUsingWebRtc).toHaveBeenCalled();
  });

  it('should display correct icon when webrtc', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(CtiProxy, 'stopUsingWebRtc');
    expect($scope.getDisplayIcon()).toEqual('webrtc');

    $scope.isQuitting = true;
    expect($scope.getDisplayIcon()).toEqual('blank');
  });

  it('should display correct icon when fixed line', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);
    spyOn(CtiProxy, 'stopUsingWebRtc');
    expect($scope.getDisplayIcon()).toEqual('fixed');

    $scope.isQuitting = true;
    expect($scope.getDisplayIcon()).toEqual('blank');
  });

  it('should say if the WebRtc is used', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);
    expect($scope.isWebRtcActive()).toEqual(false);
  });

  it('should say if the WebRtc is NOT used', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);
    expect($scope.isWebRtcActive()).toEqual(false);
  });

  it('should say if forwarded', function() {
    spyOn(forward, 'isSet');
    $scope.isForwarded();

    expect(forward.isSet).toHaveBeenCalled();
  });

  it('should get forwarded number', function() {
    spyOn(forward, 'getDestination');
    $scope.getForwardNumberOrDnd();

    expect(forward.getDestination).toHaveBeenCalled();
  });

  it('should get na forward icon as fallback if forward on busy is enabled externally', function() {
    $scope.user = UserBuilder('Bob').build();
    $scope.user.busyFwdEnabled = true;

    expect($scope.getDisplayIcon()).toEqual('fixed_forward_na');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    expect($scope.getDisplayIcon()).toEqual('webrtc_forward_na');
  });

});
