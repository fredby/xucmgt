describe('View controller', function() {
  var $rootScope;
  var $scope;
  var $state;
  var XucPhoneEventListener;
  var phoneEventHandler;
  var ctrl;

  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _$state_, _XucPhoneEventListener_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    XucPhoneEventListener = _XucPhoneEventListener_;

    spyOn(XucPhoneEventListener, 'addHandler').and.callFake(function(passedScope, handler) {
      phoneEventHandler = handler;
    });

    ctrl = $controller('ViewController', {
      '$scope' :                  $scope,
      '$rootScope' :              $rootScope,
      '$state':                   $state,
      'XucPhoneEventListener':    XucPhoneEventListener

    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('subscribes to the XucPhoneEvent dialing', function() {
    expect(XucPhoneEventListener.addHandler).toHaveBeenCalled();
    expect(phoneEventHandler).not.toBeUndefined();
  });

  it('switches to the callControl view on phone event dialing', function() {
    spyOn($state,'go');
    phoneEventHandler({eventType: XucPhoneEventListener.EVENT_DIALING});
    expect($state.go).toHaveBeenCalledWith('interface.callControl');
  });

  it('switches to the callControl view on phone event ringing', function() {
    spyOn($state,'go');
    phoneEventHandler({eventType: XucPhoneEventListener.EVENT_RINGING});
    expect($state.go).toHaveBeenCalledWith('interface.callControl');
  });

});
