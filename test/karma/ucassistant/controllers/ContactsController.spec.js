describe('Contacts controller', function() {
  var $rootScope;
  var $scope;
  var $q;
  var enableVideoDefer;
  var remoteConfiguration;
  var CtiProxy;
  var ctrl;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, _$q_, $controller, _remoteConfiguration_, _CtiProxy_) {
    $rootScope =_$rootScope_;
    CtiProxy = _CtiProxy_;
    $scope = $rootScope.$new();
    $q = _$q_;
    $rootScope = _$rootScope_;
    enableVideoDefer = $q.defer();
    remoteConfiguration = _remoteConfiguration_;
    spyOn(remoteConfiguration, 'getBooleanOrElse').and.returnValue(enableVideoDefer.promise);

    ctrl = $controller('ContactsController', {
      '$scope' :                  $scope,
      '$rootScope' :              $rootScope,
      'remoteConfiguration':      remoteConfiguration
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('inits videoEnabled when enabled and using webrtc', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    enableVideoDefer.resolve(true);
    $rootScope.$digest();
    expect($scope.videoEnabled).toBe(true);
  });

  it('inits videoEnabled when enabled and using Cti', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);
    enableVideoDefer.resolve(true);
    $rootScope.$digest();
    expect($scope.videoEnabled).toBe(false);
  });

  it('inits videoEnabled when disabled and using webrtc', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    enableVideoDefer.resolve(false);
    $rootScope.$digest();
    expect($scope.videoEnabled).toBe(false);
  });

  it('inits videoEnabled when disabled and using Cti', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);
    enableVideoDefer.resolve(false);
    $rootScope.$digest();
    expect($scope.videoEnabled).toBe(false);
  });

  it('returns false if not personal contact and contact not hovered', () => {
    const contact = {
      source: 'internal',
      hover: false
    };
    expect($scope.isPersonal(contact)).toBe(false);
  });

  it('returns false if personal contact and contact not hovered', () => {
    const contact = {
      source: 'personal',
      hover: false
    };
    expect($scope.isPersonal(contact)).toBe(false);
  });

  it('returns false if not personal contact and contact hovered', () => {
    const contact = {
      source: 'internal',
      hover: true
    };
    expect($scope.isPersonal(contact)).toBe(false);
  });

  it('returns true if  personal contact and contact hovered', () => {
    const contact = {
      source: 'personal',
      hover: true
    };
    expect($scope.isPersonal(contact)).toBe(true);
  });

});
