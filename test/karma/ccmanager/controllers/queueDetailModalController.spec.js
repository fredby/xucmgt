describe('queue info modal controller', function() {
  var $scope;
  var $rootScope;
  var $controller;
  var ctrl;
  var modalInstance;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_$controller_, _$rootScope_, _$uibModal_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $controller = _$controller_;

    modalInstance = _$uibModal_.open({
      template: '<div></div>'
    });

    var queue = {id: 1};

    ctrl = $controller('queueDetailModalController', {
      '$scope' : $scope,
      '$uibModalInstance' : modalInstance,
      'queue' : queue
    });
  }));


  it('should instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });


  it('should close popup', function() {
    spyOn(modalInstance, 'close');

    ctrl.close();
    expect(modalInstance.close).toHaveBeenCalled();
  });
});
