describe('agent dyn view pref controllers', function() {
  var preferences;
  var $scope;
  var $state;
  var ctrl;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_Preferences_, $controller, _$rootScope_, _$state_) {
    $scope = _$rootScope_.$new();
    preferences = _Preferences_;
    $state = _$state_;

    spyOn($state, 'reload');
    spyOn(preferences, 'setOption');

    ctrl = $controller('ccAgentDynViewPrefController', {
      '$scope' : $scope,
      '$state': $state,
      'preferences' : preferences
    });
  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('should toggle show group option', function() {
    $scope.agentView = {showGroup: true};
    $scope.toggleShowGroup();
    expect(preferences.setOption).toHaveBeenCalledWith("AgentView.showGroup", $scope.agentView);
    expect($state.reload).toHaveBeenCalled();
  });


});
