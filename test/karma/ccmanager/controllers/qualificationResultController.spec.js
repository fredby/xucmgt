describe('qualification result controller', function() {
  var preferences;
  var xucQueue;
  var $scope;
  var ctrl;
  var $window;
  var XucLink;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_XucQueue_,_Preferences_, _XucLink_, _$window_, $controller, $rootScope) {
    $scope = $rootScope.$new();
    xucQueue = _XucQueue_;
    XucLink = _XucLink_;
    preferences = _Preferences_;
    $window = _$window_;
    spyOn(xucQueue, 'getQueues').and.returnValue([{'queueName': 'test', 'number': 1000}]);
    spyOn(XucLink, 'getServerUrl').and.returnValue("http://localhost");

    ctrl = $controller('qualificationResultController', {
      '$scope' : $scope,
      'XucQueue' : xucQueue,
      'Preferences' : preferences
    });
  }));

  it('can instantiate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('closes pickers on init', function() {
    expect($scope.popupFrom.opened).toBe(false);
    expect($scope.popupTo.opened).toBe(false);
  });

  it('loads queues on init', function() {
    expect(xucQueue.getQueues).toHaveBeenCalled();
    expect($scope.queues).toEqual([{'queueName': 'test', 'number': 1000}]);
  });

  it('opens popups when requested', function() {
    $scope.openFrom();
    expect($scope.popupFrom.opened).toBe(true);
    
    $scope.openTo();
    expect($scope.popupTo.opened).toBe(true);
  });

  it('creates url', function() {
    $scope.dateFrom = new Date(2015,5,5);
    $scope.dateTo = new Date(2015,6,6);
    $scope.selectedQueue = { id: 1 };

    expect($scope.getUrl()).toBe('http://localhost/xuc/api/2.0/call_qualification/csv/1/2015-06-04/2015-07-05');
  });

  it('should export all personal contacts', function() {
    spyOn($window, 'open');
    $scope.selectedQueue = { id: 1 };
    $scope.dateFrom = new Date(2015,5,5);
    $scope.dateTo = new Date(2015,6,6);

    $scope.openCsvUrl();
    expect($window.open).toHaveBeenCalledWith('http://localhost/xuc/api/2.0/call_qualification/csv/1/2015-06-04/2015-07-05', '_blank');
  });

});
