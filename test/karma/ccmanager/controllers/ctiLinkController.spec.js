

describe('CCmanager CtiLinkController', function() {
  var $rootScope;
  var $scope;
  var ctrl;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();

    spyOn(Cti,'setHandler');
    ctrl = $controller('ctiLinkController', {
      '$scope' : $scope,
      '$rootScope' : $rootScope
    });
  }));

  it('can instanciate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

});
