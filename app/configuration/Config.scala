package configuration

import javax.inject._

trait Config {
  val hostAndPort: String
  val useSsl: Boolean
  val useSso: Boolean
  val casServerUrl: Option[String]
  val casLogoutEnable: Boolean
}

@Singleton
class XucConfig @Inject() (configuration: play.api.Configuration) extends Config {
  val hostAndPort = s"${configuration.get[String]("xuc.host")}:${configuration.get[String]("xuc.port")}"
  val useSsl: Boolean = configuration.get[Boolean]("xuc.useSsl")
  val useSso: Boolean = configuration.get[Boolean]("xuc.useSso")
  val casServerUrl: Option[String] = configuration.getOptional[String]("xuc.casServerUrl")
  val casLogoutEnable: Boolean = configuration.getOptional[Boolean]("xuc.casLogoutEnable").getOrElse(false)
}

@Singleton
class CcConfig @Inject() (configuration: play.api.Configuration) extends XucConfig(configuration) {
  def showRecordingControls: Boolean = configuration.get[Boolean]("agent.showRecordingControls")
  def showCallbacks: Boolean = configuration.get[Boolean]("agent.showCallbacks")
  def showQueueControls: Boolean = configuration.get[Boolean]("agent.showQueueControls")
  def getAgentConf(param: String): Option[String] = configuration.getOptional[String]("agent." + param)
  val enforceManagerSecurity: Boolean = configuration.get[Boolean]("manager.enforceSecurity")
}

@Singleton
class UcConfig @Inject() (configuration: play.api.Configuration) extends XucConfig(configuration) {
  def getClientConf(param: String): Option[String] = configuration.getOptional[String]("client." + param)
}
