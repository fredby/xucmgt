import 'angular-translate-loader-partial';

export default function config($stateProvider, $urlRouterProvider, $translateProvider, $translatePartialLoaderProvider, localStorageServiceProvider, $logProvider) {

  $logProvider.debugEnabled(false);
  Cti.debugMsg = false;

  $urlRouterProvider.otherwise('/login');

  $stateProvider
    .state('login', {
      url: '/login?error',
      templateUrl: 'assets/javascripts/ccagent/controllers/login.html',
      controller: 'LoginController as ctrl',
      data: {
        requireLogin: false
      }
    })
    .state('content', {
      url: '/main',
      data: {
        requireLogin: true
      },
      views: {
        '': {
          templateUrl: 'assets/javascripts/ccagent/controllers/main.html',
          controller: 'MainController as ctrl',
        },
        'thirdparty': {
          templateUrl: 'assets/javascripts/ccagent/controllers/thirdParty.html',
          controller: 'ThirdPartyController as ctrl'
        }
      }
    })
    .state('content.activities', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentActivities.html',
      controller: 'ContentActivitiesController as ctrl',
      data: {
        requireLogin: true
      }
    })
    .state('content.agents', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentAgents.html',
      controller: 'ContentAgentsController as ctrl',
      data: {
        requireLogin: true
      }
    })
    .state('content.callbacks', {
      abstract: true,
      templateUrl: 'assets/javascripts/ccagent/controllers/contentCallbacks.html',
      controller: 'ContentCallbacksController as ctrl',
      data: {
        requireLogin: true
      }
    })
    .state('content.callbacks.list', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentCallbacks.list.html'
    })
    .state('content.callbacks.details', {
      abstract: true,
      templateUrl: 'assets/javascripts/ccagent/controllers/contentCallbacks.details.html'
    })
    .state('content.callbacks.details.id', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentCallbacks.details.id.html'
    })
    .state('content.callbacks.details.notes', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentCallbacks.details.notes.html'
    })
    .state('content.customer', {
      abstract: true,
      templateUrl: 'assets/javascripts/ccagent/controllers/contentCustomer.html',
      controller: 'ContentCustomerController as ctrl',
      data: {
        requireLogin: true
      }
    })
    .state('content.customer.path', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentCustomer.path.html'
    })
    .state('content.customer.context', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentCustomer.context.html'
    })
    .state('content.history', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentHistory.html',
      controller: 'ContentHistoryController as ctrl',
      data: {
        requireLogin: true
      }
    })
    .state('content.search', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentSearch.html',
      controller: 'ContentSearchController as ctrl',
      data: {
        requireLogin: true
      }
    })
    .state('content.settings', {
      templateUrl: 'assets/javascripts/ccagent/controllers/contentSettings.html',
      controller: 'ContentSettingsController as ctrl',
      data: {
        requireLogin: true
      }
    });
  $translateProvider.useSanitizeValueStrategy('escape');
  $translatePartialLoaderProvider.addPart('ccagent');
  $translateProvider.useLoader('$translatePartialLoader', {
    urlTemplate: 'assets/i18n/{part}-{lang}.json'
  });
  $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
    'en_*': 'en',
    'fr_*': 'fr'
  });
  $translateProvider.preferredLanguage(document.body.getAttribute('data-preferredlang'));
  $translateProvider.fallbackLanguage(['fr']);
  $translateProvider.forceAsyncReload(true);

  localStorageServiceProvider.setPrefix('agent.settings');
  localStorageServiceProvider.setNotify(true,true);
}
