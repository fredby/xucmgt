import {ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT} from '../agent.run';

export default function callControl($translate, $log, XucPhoneState, CtiProxy, electronWrapper, configuration) {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/callControl.html',
    controller: ($scope) => {
      $scope.calls = XucPhoneState.getCalls();
      $scope.conference = XucPhoneState.getConference();

      $scope.showRecordingControls = false;

      configuration.getBoolean("showRecordingControls").then(value => $scope.showRecordingControls = value);

      $scope.isRinging = () => {
        return $scope.calls.some(c => c.state == XucPhoneState.STATE_RINGING);
      };

      $scope.isEstablished = () => {
        return $scope.calls.length > 0 && !$scope.isRinging();
      };

      $scope.triggerAction = () => {
        if ($scope.isRinging()) {
          electronWrapper.setElectronConfig(ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT);
          CtiProxy.answer();
        }
        if ($scope.isEstablished()) {
          CtiProxy.hangup();
        }
      };
    }
  };
}
