export default function callConference($translate, $log, CtiProxy, XucPhoneState, XucAgentUser, callControlHelper) {

  return {
    require: '^^callControl',
    restrict: 'E',
    replace: true,
    templateUrl: 'assets/javascripts/ccagent/directives/callConference.html',
    scope: {},
    controller: ($scope) => {
      $scope.conference = XucPhoneState.getConference();
      $scope.listened = false;
      $scope.recording = false;

      const possibleActions = callControlHelper.getPossibleActions();

      $scope.getCallActions = () => {
        var actions = [];

        if($scope.canRecord) {
          if($scope.recording) {
            actions.push(possibleActions.stoprecording);
          } else {
            actions.push(possibleActions.startrecording);
          }
        }
        actions.push(possibleActions.hold);
        actions.push(possibleActions.hangup);

        return actions;
      };

      $scope.isMonitored = () => {
        XucAgentUser.isMonitored();
      };

      $scope.canMonitor = () => {
        XucAgentUser.canMonitor();
      };

      XucAgentUser.subscribeToListenEvent($scope, (event) => {
        $scope.listened = event.started;
      });

      XucAgentUser.subscribeToAgentState($scope, (state) => {
        $scope.recording = state.monitorState == "ACTIVE";
        $scope.canRecord = state.monitorState == "ACTIVE" || state.monitorState == "PAUSED";
      });

    }
  };
}
