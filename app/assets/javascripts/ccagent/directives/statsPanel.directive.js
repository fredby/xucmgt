
export default function statsPanel($translate, $log, XucAgent, $filter) {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/statsPanel.html',
    scope: {
      display: "@",
      agentId: "@"
    },
    link: (scope, elem, attr) => {
      scope.stats = [9];
      
      scope.updateAgentData = function() {
        if(attr.agentId) {
          XucAgent.getAgentAsync(parseInt(attr.agentId)).then((agent) => {
            scope.stats.length = 0;
            scope.stats[0] = {
              id: "PausedTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.PausedTime || 0), "HH:mm:ss")
            };
            scope.stats[1] = {
              id: "InbAcdCallTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.InbAcdCallTime || 0), "HH:mm:ss")
            };
            scope.stats[2] = {
              id: "InbAverAcdCallTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.InbAverAcdCallTime || 0), "HH:mm:ss")
            };
            scope.stats[3] = {
              id: "OutCallTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.OutCallTime || 0), "HH:mm:ss")
            };
            scope.stats[4] = {
              id: "WrapupTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.WrapupTime || 0), "HH:mm:ss")
            };
            scope.stats[5] = {
              id: "InbAcdCalls",
              value: agent.stats.InbAcdCalls || 0
            };
            scope.stats[6] = {
              id: "InbAnsAcdCalls",
              value: agent.stats.InbAnsAcdCalls || 0
            };
            scope.stats[7] = {
              id: "InbUnansAcdCalls",
              value: agent.stats.InbUnansAcdCalls || 0
            };
            scope.stats[8] = {
              id: "OutCalls",
              value: agent.stats.OutCalls || 0
            };
            scope.stats[9] = {
              id: "InbPercUnansAcdCalls",
              value: $filter('number')(agent.stats.InbPercUnansAcdCalls || 0, 0) + ' %'
            };
          });
        }
      };

      scope.$on('AgentStatisticsUpdated', function(){
        scope.updateAgentData();
      });

      scope.updateAgentData();
    }
  };
}
