import _ from 'lodash';
import moment from 'moment';

export default function agentState($translate, $log, XucAgentUser, XucLink, XucAgent, $state, $interval) {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/agentState.html',
    link: (scope) => {
      var stopTime;
      const stateRefreshTimeout = 1000;
      scope.agentState = {};
      scope.loggedIn = false;

      scope.refreshState = function() {
        scope.agentState.timeInState = moment().countdown(scope.agentState.momentStart);
      };
      stopTime = $interval(scope.refreshState, stateRefreshTimeout);

      scope.$on('$destroy', function() {
        $interval.cancel(stopTime);
      });

      scope.checkLogout = function(state) {
        if(state.name === "AgentLoggedOut") {
          if (scope.loggedIn) {
            XucLink.logout();
            scope.loggedIn = false;
            $state.go("login", {error: "Logout"});
          }
        }
        else {
          scope.loggedIn = true;
        }
      };

      XucAgentUser.subscribeToAgentState(scope, function(agtState) {
        $log.debug("agentState: received "+agtState.name +" AgentStateEvent");
        scope.checkLogout(agtState);
        scope.agentState = agtState;

        let m = XucAgent.buildMoment(agtState.since);
        scope.agentState.momentStart = m.momentStart;
        scope.agentState.timeInState = m.timeInState;

        scope.possibleStates = XucAgentUser.getPossibleAgentStates();
      });

      scope.getAgentStateClassName = function(state) {
        var icon = state.name === "AgentLoggedOut" ? "fa-power-off" : "fa-chevron-down";
        return icon + ' state-' + _.kebabCase(state.name);
      };

      scope.switchState = function(state) {
        XucAgentUser.switchAgentState(state);
        scope.checkLogout(state);
      };

      scope.displayTime = function(){
        return scope.agentState.name !== 'AgentOnCall';
      };
    }
  };
}
