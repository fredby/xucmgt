import _ from 'lodash';

export default function callLine($translate, $log, CtiProxy, XucPhoneState, XucAgentUser, XucQueue, callControlHelper) {

  return {
    require: '^^callControl',
    restrict: 'E',
    replace: true,
    templateUrl: 'assets/javascripts/ccagent/directives/callLine.html',
    scope: {
      call: '=*',
      showRecordingControls: '='
    },
    controller: ($scope) => {
      $scope.listened = false;
      $scope.recording = false;

      $scope.queueDisplayName = '';

      if(typeof($scope.call.queueName) !== "undefined") {
        XucQueue.getQueueByNameAsync($scope.call.queueName).then((q) => {
          $scope.queueDisplayName = q.displayName;
        });
      }

      const possibleActions = callControlHelper.getPossibleActions();

      $scope.getClass = () => {
        var direction = _.toLower($scope.call.direction);
        var acd = $scope.call.acd?'acd':'nonacd';
        var state = _.toLower($scope.call.state);

        return 'call-' + direction +
          ' call-' + acd +
          ' call-' + state +
          ' call-' + direction + '-' + acd + '-' + state;
      };

      $scope.getIconClass = () => {
        switch($scope.call.state) {
        case 'OnHold':
          return 'fa-pause';
        case 'Established':
          return 'fa-play';
        default:
          return 'fa-spinner fa-pulse fa-fw';
        }
      };

      $scope.getCallActions = () => {
        var actions = [];
        var allCalls = XucPhoneState.getCalls();

        switch($scope.call.state) {
        case XucPhoneState.STATE_RINGING:
          if(!CtiProxy.isCustomLine()) {
            actions.push(possibleActions.answer);
            actions.push(possibleActions.hangup);
          }
          break;

        case XucPhoneState.STATE_DIALING:
          actions.push(possibleActions.hangup);
          break;

        case XucPhoneState.STATE_ESTABLISHED:
          if($scope.canRecord && $scope.showRecordingControls) {
            if($scope.recording) {
              actions.push(possibleActions.stoprecording);
            } else {
              actions.push(possibleActions.startrecording);
            }
          }
          if (CtiProxy.isUsingWebRtc()){
            actions.push(possibleActions.dtmfkeypad);
          }
          if(allCalls.length == 1 && !CtiProxy.isCustomLine()) {
            actions.push(possibleActions.hold);
          }
          actions.push(possibleActions.hangup);
          break;
        case XucPhoneState.STATE_ONHOLD:
          if(allCalls.length === 1 && !CtiProxy.isCustomLine()) {
            actions.push(possibleActions.unhold);
          }
          if(allCalls.length == 2 && !CtiProxy.isCustomLine()) {
            actions.push(possibleActions.transfer);
            if (CtiProxy.isConferenceCapable()){
              actions.push(possibleActions.conference);
            }
          }
          break;
        }

        return actions;
      };

      $scope.isMonitored = () => {
        XucAgentUser.isMonitored();
      };

      $scope.canMonitor = () => {
        XucAgentUser.canMonitor();
      };

      XucAgentUser.subscribeToListenEvent($scope, (event) => {
        $scope.listened = event.started;
      });

      XucAgentUser.subscribeToAgentState($scope, (state) => {
        $scope.recording = state.monitorState == "ACTIVE";
        if($scope.showRecordingControls) {
          $scope.canRecord = state.monitorState == "ACTIVE" || state.monitorState == "PAUSED";
        }
      });

    }
  };
}
