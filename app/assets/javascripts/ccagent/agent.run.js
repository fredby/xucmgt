export const ELECTRON_DEFAULT_WIDTH = 360;
export const ELECTRON_DEFAULT_HEIGHT = 800;
export const ELECTRON_MINI_WIDTH = 70;
export const ELECTRON_MINI_HEIGHT = 675;
export const ELECTRON_THIRD_PARTY_MIN_WIDTH = 1000;

export default function run($rootScope, $state, XucLink, XucUser, $log, $translate, $transitions, CtiProxy, externalEvent, electronWrapper){
  electronWrapper.setElectronConfig(ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT, 'CC Agent');
  $rootScope.$state = $state;

  $transitions.onStart({
    to: function(state) {
      return state.data !== null && state.data.requireLogin === true;
    }
  },function(trans) {
    if(!XucLink.isLogged()) {
      $log.info("Login required");
      return trans.router.stateService.target('login');
    }
    return true;
  });

  var unregisterFatalError = $rootScope.$on(CtiProxy.FatalError, () => {
    externalEvent.unRegisterExternalEventListener();
  });
  $rootScope.$on('$destroy', unregisterFatalError);
}
