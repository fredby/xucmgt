export default function configuration($http) {

  var _get = function(name) {
    return $http.get("/ccagent/config/" + name).then(function(resp) {
      return resp.data.value;
    });
  };

  var _getBoolean = function(name) {
    return _get(name).then(function(value) {
      return value === "true";
    });
  };

  return {
    get: _get,
    getBoolean: _getBoolean
  };
}