export default function callControlHelper(CtiProxy, XucAgentUser, keyboard) {

  var _getPossibleActions = () => {
    return {
      hold: {
        icon: 'pause',
        label: 'hold',
        trigger: () => { CtiProxy.hold(); }
      },
      unhold: {
        icon: 'play',
        label: 'unhold',
        trigger: () => { CtiProxy.hold(); }
      },
      answer: {
        icon: 'call_answer',
        label: 'answer',
        trigger: () => { CtiProxy.answer(); }
      },
      hangup: {
        icon: 'call_hangup',
        label: 'hangup',
        trigger: () => { CtiProxy.hangup(); }
      },
      conference: {
        icon: 'conference_start',
        label: 'conference',
        trigger: () => { CtiProxy.conference(); }
      },
      transfer: {
        icon: 'transfer_start',
        label: 'transfer',
        trigger: () => { CtiProxy.completeTransfer(); }
      },
      startrecording: {
        icon: 'rec',
        label: 'startrecording',
        trigger: () => { XucAgentUser.monitorUnpause(); }
      },
      stoprecording: {
        icon: 'rec_selected',
        label: 'stoprecording',
        trigger: () => { XucAgentUser.monitorPause(); }
      },
      dtmfkeypad: {
        icon: 'dtmf',
        label: 'dtmfkeypad',
        trigger: () => { keyboard.show(); }
      }
    };
  };

  return {
    getPossibleActions: _getPossibleActions
  };
}