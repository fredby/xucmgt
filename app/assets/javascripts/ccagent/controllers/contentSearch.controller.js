import _ from 'lodash';

export default class ContentSearchController {

  constructor($scope, XucDirectory, focus) {
    this.$scope = $scope;
    this.XucDirectory = XucDirectory;
    this.$scope.searchResult = [];
    this.focus = focus;

    this.focus('search');
    this.$scope.$on('searchResultUpdated', this.onSearchResult.bind(this));
  }

  onSearchResult() {
    this.$scope.searchResult = _.sortBy(this.XucDirectory.getSearchResult(), (contact) => {
      contact.hover = false;
      return contact.entry[0].toLowerCase();
    });
    _.remove(this.$scope.searchResult, (contact) => {
      return !this.isCallable(contact);
    });
    this.$scope.headers = this.XucDirectory.getHeaders().slice(0, 5);
    this.$scope.searchTerm = this.XucDirectory.getSearchTerm() ? this.XucDirectory.getSearchTerm() : '*';
    if(!this.$scope.$$phase) this.$scope.$apply();
  }

  getPhoneStateLabel(contact) {
    return "USER_STATUS_" + contact.status;
  }

  getPhoneStateTextColor(contact) {
    return "user-status-text" + contact.status;
  }

  getPhoneStateBackColor(contact) {
    return "user-status" + contact.status;
  }

  isCallable(contact) {
    return _.some([1, 2, 3, 5], function(index) {
      return !_.isEmpty(contact.entry[index]);
    });
  }

  isEmpty(field) {
    if (typeof field != 'undefined') {
      if (field.length > 0) {
        return true;
      }
    }
    return false;
  }

  isSearching() {
    return this.XucDirectory.isSearching();
  }
}