import {ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT, ELECTRON_THIRD_PARTY_MIN_WIDTH} from '../agent.run';
import _ from 'lodash';

export default class ThirdPartyController {

  constructor($scope, $log, XucThirdParty, configuration, electronWrapper, externalEvent, $window) {
    this.$scope = $scope;
    this.XucThirdParty = XucThirdParty;
    this.electronWrapper = electronWrapper;
    this.externalEvent = externalEvent;
    this.$window = $window;

    configuration.get("thirdPartyWsUrl").then((value) => {
      if (value.length > 0){
        this.XucThirdParty.setThirdPartyWs(value);

        this.externalEvent.registerThirdPartyCallback(this.onMessageReceived.bind(this));
        this.XucThirdParty.addActionHandler(this.$scope, this.onThirdPartyAction.bind(this));
        this.XucThirdParty.addClearHandler(this.$scope, this.onThirdPartyClearAction.bind(this));
      }
    });
  }

  onMessageReceived() {
    this.XucThirdParty.clearAction();
  }

  onThirdPartyAction(event) {
    switch(event.action) {
    case 'open': {
      this.$scope.action = event;
      this.electronWrapper.setElectronConfig(ELECTRON_THIRD_PARTY_MIN_WIDTH,ELECTRON_DEFAULT_HEIGHT);
      let btn = angular.element('#maxmin-btn');
      if (btn.length) {
        btn.addClass('hide');
      }
      break;
    }
    case 'popup': {
      if(angular.isDefined(event.url)) {
        let windowName = 'popup';
        if(angular.isDefined(event.multitab) && event.multitab) {
          windowName = windowName + _.uniqueId();
        }
        this.$window.open(event.url, windowName);
      }
      break;
    }
    }
  }

  onThirdPartyClearAction() {
    this.$scope.action = null;
    this.electronWrapper.setElectronConfig(ELECTRON_DEFAULT_WIDTH,ELECTRON_DEFAULT_HEIGHT);
    let btn = angular.element('#maxmin-btn');
    if (btn.length) {
      btn.removeClass('hide');
    }
  }
}
