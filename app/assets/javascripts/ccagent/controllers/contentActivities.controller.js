import angular from 'angular';
import _ from 'lodash';

export default class ContentActivitiesController {

  constructor($scope, $q, configuration, agentActivitiesHelper, callContext) {
    this.$scope = $scope;
    this.configuration = configuration;
    this.agentActivitiesHelper = agentActivitiesHelper;
    this.callContext = callContext;
    this.$q = $q;

    this.showQueueControls = false;

    this.$scope.displayMyQueues = agentActivitiesHelper.getQueueFilter();
    this.$scope.activities = [];
    this.$scope.pendingActivities = [];
    this.$scope.activityHovered = null;

    configuration.getBoolean("showQueueControls").then(value => this.showQueueControls = value);

    this.sort = {
      status: function(value) {
        switch (value) {
        case 'grey':
          return 0;
        case 'green':
          return 1;
        case 'orange':
          return 2;
        case 'red':
          return 3;
        default:
          return 0;
        }
      }
    };

    this.$scope.$on('QueueStatsUpdated', this.loadQueues.bind(this));
    this.loadQueues();
  }

  loadQueues() {
    this.agentActivitiesHelper.setQueueFilter(this.$scope.displayMyQueues);
    this.agentActivitiesHelper.loadQueues().then(activities => this.showQueues(activities));
  }

  getState(activity) {
    var state = "grey";
    if (activity) {
      if (!activity.LoggedAgents) {
        state = activity.WaitingCalls ? 'red' : 'grey';
      }
      else {
        state = activity.AvailableAgents ? 'green' : 'orange';
      }
    }
    return state;
  }

  isHovered(activity) {
    return this.$scope.activityHovered == activity.id;
  }

  clearHover() {
    this.$scope.activityHovered = null;
  }

  enterAllQueues() {
    angular.forEach(this.$scope.activities, (activity) => {
      if(!activity.associated) {
        this.enterOrLeaveQueue(activity);
      }
    });
  }

  enterOrLeaveQueue(activity) {
    if (this.$scope.pendingActivities[activity.id]) return;
    this.$scope.pendingActivities[activity.id] = true;
    this.agentActivitiesHelper.enterOrLeaveQueue(activity).finally(() => {
      this.loadQueues();
      delete this.$scope.pendingActivities[activity.id];
    });
  }

  showQueues(activities) {
    this.$scope.activities = activities;

    var promises = [];
    this.someQueueSelected = false;
    this.allQueueSelected = true;

    angular.forEach(activities, activity => {
      promises.push(this.agentActivitiesHelper.isMemberOfQueue(activity).then(isQueueMember => {
        activity.associated = isQueueMember;
        if (activity.associated === true) {
          this.agentActivitiesHelper.addFavoriteQueue(activity);
        }
      }));
    });


    this.$q.all(promises).then(() => {
      let count = _.sumBy(activities, activity => (activity.associated ? 1 : 0));
      if (count == 0) {
        this.allQueueSelected = false;
      }
      if (count < activities.length && count != 0) {
        this.someQueueSelected = true;
        this.allQueueSelected = false;
      }

      this.loadingQueues = false;
    });
  }

  removeFromFavorites(activity) {
    this.agentActivitiesHelper.removeFavoriteQueue(activity);
    this.loadQueues();
  }

  canAssociate() {
    return this.showQueueControls;
  }

  dial(activity) {
    this.callContext.normalizeDialOrAttTrans(activity.number);
  }

  setCounters(activity) {
    this.$scope.activityHovered = activity.id;
    this.$scope.activityCounters = {
      number: activity.number,
      calls: activity.WaitingCalls,
      eta: new Date(0, 0, 0).setSeconds(activity.EWT || 0),
      agents: activity.AvailableAgents
    };
  }

}
