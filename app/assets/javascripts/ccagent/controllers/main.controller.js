import {ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT, ELECTRON_MINI_WIDTH, ELECTRON_MINI_HEIGHT} from '../agent.run';

export default class MainController {

  constructor($scope, $state, $interval, $window, XucAgentUser, XucUser, XucLink, CtiProxy, XucUtils, XucDirectory, XucSheet, callContext,
   agentCallbackHelper, agentActivitiesHelper, electronWrapper, XucCallNotification, focus, onHold, XucQueueTotal) {
    this.$scope = $scope;
    this.$state = $state;
    this.$window = $window;
    this.XucLink = XucLink;
    this.XucUser = XucUser;
    this.XucAgentUser = XucAgentUser;
    this.CtiProxy = CtiProxy;
    this.XucUtils = XucUtils;
    this.XucDirectory = XucDirectory;
    this.XucSheet = XucSheet;
    this.XucQueueTotal = XucQueueTotal;
    this.callContext = callContext;
    this.agentCallbackHelper = agentCallbackHelper;
    this.agentActivitiesHelper = agentActivitiesHelper;
    this.electronWrapper = electronWrapper;
    this.focus = focus;
    this.onHold = onHold;

    this.electronSize = {w: ELECTRON_DEFAULT_WIDTH, h: ELECTRON_DEFAULT_HEIGHT };
    this.toggled = false;

    this.agentActivitiesHelper.init(this.$scope);

    this.agentCallbackHelper.loadAllCallbacks();
    $interval(this.agentCallbackHelper.loadAllCallbacks, 3600000, 0, true); // every hour

    XucAgentUser.loginAgent();
    XucUser.getUserAsync().then((user) => {
      $scope.user = user;
      XucAgentUser.subscribeToAgentState($scope, this.eventReceived.bind(this));
      XucCallNotification.enableNotification();
    });

    XucAgentUser.whenAgentError().then(this.onAgentError.bind(this));

    $scope.$on('linkDisConnected', function() {
      $state.go('login', {'error': 'LinkClosed'});
    });

    this.show('activities');
  }

  onAgentError(event) {
    if (event.Error === "LoggedInOnAnotherPhone") {
      this.XucAgentUser.logoutAgent();
    }
    this.XucLink.logout();
    this.$state.go("login", {error: event.Error});
  }


  onKeyPress(keyCode) {

    var setPhoneClass = () => {
      angular.element('#xuc_search i').removeClass('fa-search');
      angular.element('#xuc_search i').addClass('fa-phone');
    };

    var setSearchClass = () => {
      angular.element('#xuc_search i').removeClass('fa-phone');
      angular.element('#xuc_search i').addClass('fa-search');
    };

    var normalizedValue = this.XucUtils.normalizePhoneNb(this.$scope.searchValue || '');
    (this.XucUtils.isaPhoneNb(normalizedValue)) ? setPhoneClass() : setSearchClass();

    if (keyCode == 13) {
      this.searchOrDial();
    }
  }

  searchOrDial(){
    var isSearch = true;

    if (this.$scope.searchValue) {
      var normalizedValue = this.XucUtils.normalizePhoneNb(this.$scope.searchValue);
      if (this.XucUtils.isaPhoneNb(normalizedValue)) {
        isSearch = false;
        this.callContext.normalizeDialOrAttTrans(this.$scope.searchValue);
      }
    }
    if (isSearch) {
      this.XucDirectory.directoryLookup(this.$scope.searchValue);
      this.show('search');
    }
  }

  eventReceived(event) {
    this.$scope.user.phoneNb = event.phoneNb;
  }

  isActive(contentName) {
    return this.$state.includes('content.'+contentName);
  }

  show(contentName) {
    if (this.toggled){
      this.toggleWindow();
    }
    return this.$state.go('content.'+contentName).then((tResult)=> {
      if(tResult.name === 'content.search') {
        this.focus('search');
      }
    });
  }

  getCallbackStatus() {
    return this.agentCallbackHelper.getStatus();
  }

  getWaitingCalls() {
    let stats = this.XucQueueTotal.getCalcStats();
    return this.$scope.waitingCalls = stats ? stats.sum.WaitingCalls : 0;
  }

  isElectron() {
    return this.electronWrapper.isElectron();
  }

  toggleWindow() {
    const size = this.electronSize;
    if (!this.toggled){
      this.electronWrapper.setElectronConfig(ELECTRON_MINI_WIDTH,ELECTRON_MINI_HEIGHT, null, true);
    }
    else {
      this.electronWrapper.setElectronConfig(size.w,size.h);
    }
    this.toggled = !this.toggled;
  }

  showVersion() {
    return this.$window.appVersion;
  }

  displayAudioSettings() {
    return this.CtiProxy.isUsingWebRtc();
  }

}
