import {ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT} from '../agent.run';

export default class LoginController {

  constructor($state, $stateParams, XucAgentUser, CtiProxy, electronWrapper) {
    this.xucAgentUser = XucAgentUser;
    this.error = $stateParams.error;
    this.$state = $state;
    this.ctiProxy = CtiProxy;
    this.electronWrapper = electronWrapper;

    /* eslint-disable no-undef */
    this.hostAndPort = externalConfig.hostAndPort;
    this.useSso = externalConfig.useSso;
    this.casServerUrl = externalConfig.casServerUrl;
    this.casLogoutEnable = externalConfig.casLogoutEnable;
    /* eslint-enable no-undef */

    electronWrapper.setElectronConfig(ELECTRON_DEFAULT_WIDTH,ELECTRON_DEFAULT_HEIGHT);
  }

  onLogin() {
    this.$state.go('content');
  }
}
