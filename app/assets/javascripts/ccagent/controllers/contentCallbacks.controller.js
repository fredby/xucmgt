export default class ContentCallbacksController {

  constructor($scope, $state, $q, configuration, XucCallback, XucUser, agentCallbackHelper, XucPhoneState) {

    this.XucCallback = XucCallback;
    this.XucPhoneState = XucPhoneState;
    this.XucUser = XucUser;
    this.agentCallbackHelper = agentCallbackHelper;
    this.configuration = configuration;
    this.$q = $q;
    this.$state = $state;

    this.$scope = $scope;
    this.$scope.activeCallback = {};
    this.$scope.displayedCallback = {};
    this.$scope.statuses = XucCallback.statuses;
    this.$scope.dateFormat = 'yyyy-MM-dd';
    this.$scope.dateOptions = { minDate: new Date() };
    this.$scope.calendar = { opened: false };
    this.$scope.openCalendar = () => { $scope.calendar.opened = true; };

    this.$scope.sort = {
      preferredPeriod : function(cb) {
        return cb.preferredPeriod.name;
      },
      dueStatus : function(cb) {
        return (new Date(cb.dueDate)).getTime();
      },
      queueDisplayName : function(cb) {
        return cb.queue.displayName;
      }
    };


    this.initCallbacks = () => {
      this.agentCallbackHelper.loadAllCallbacks().then(() => {
        this.$scope.callbacks = this.agentCallbackHelper.getCallbacks();
        this.XucCallback.getPreferredPeriodsAsync().then((periods) => {
          this.$scope.periods = periods;
        });
        this.XucUser.getUserAsync().then(user => {
          this.agentCallbackHelper.getPendingCallbacks(user.agentId).then(result => {
            if (result.total > 0){
              this.setActiveCallback(result.list[0]);
              if (this.agentCallbackHelper.isOngoingCallback(result.list[0])) {
                this.agentCallbackHelper.setCallbackTicket(result.list[0]);
                this.setActiveMenu('details.notes');
              } else {
                this.setActiveMenu('details.id');
              }
            }
          });
        });
      });
    };

    this.$scope.$on('CallbackTaken', this.loadAllCallbacks.bind(this));
    this.$scope.$on('CallbackStarted', this.onCallbackStarted.bind(this));
    this.$scope.$on('CallbackReleased', this.loadAllCallbacks.bind(this));
    this.$scope.$on('CallbackRequestUpdated', this.loadAllCallbacks.bind(this));

    this.initCallbacks();
  }

  loadAllCallbacks() {
    this.agentCallbackHelper.loadAllCallbacks().then(() => {
      this.$scope.callbacks = this.agentCallbackHelper.getCallbacks();
    });
  }

  isNotInCall() {
    return !this.XucPhoneState.isInCall();
  }

  takeCallback(callback) {
    this.agentCallbackHelper.takeCallback(callback).then(() => {
      this.setActiveCallback(callback);
      this.setActiveMenu('details.id');
    });
  }

  onCallbackStarted(e, cbStarted) {
    if(this.$scope.activeCallback != null && this.$scope.activeCallback.uuid == cbStarted.requestUuid) {
      this.agentCallbackHelper.setCallbackTicket(this.$scope.activeCallback, cbStarted.ticketUuid);
    }
  }

  releaseCallback() {
    this.$scope.activeCallback = this.agentCallbackHelper.releaseCallback(this.$scope.activeCallback);
    this.setActiveMenu('list');
  }

  setActiveMenu(menu) {
    this.$state.go('content.callbacks.'+menu);
  }

  setActiveCallback(callback) {
    this.$scope.activeCallback = callback;
  }

  setDisplayedCallback(callback) {
    this.$scope.displayedCallback = callback;
  }

  startCallbackCall(number) {
    this.setActiveMenu('details.notes');
    this.agentCallbackHelper.startCallbackCall(this.$scope.activeCallback, number);
  }

  isActiveMenu(menu) {
    return this.$state.is('content.callbacks.'+menu);
  }
  
  listenCallbackMessage() {
    this.XucCallback.listenCallbackMessage(this.$scope.activeCallback.voiceMessageRef);
  }

  updateTicket() {
    this.$scope.activeCallback = this.agentCallbackHelper.updateCallbackTicket(this.$scope.activeCallback);
    this.setActiveMenu('list');
  }

  callbackTicketNotDone() {
    return this.$scope.activeCallback.ticket == null || this.$scope.activeCallback.ticket.uuid === null;
  }

}
