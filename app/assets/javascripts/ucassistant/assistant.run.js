export default function run($rootScope, $state, $uibModal, XucLink, XucUser, CtiProxy, $log, $transitions, externalEvent, electronWrapper) {
  electronWrapper.setElectronConfig(470, 750, 'UC Assistant');
  $rootScope.$state = $state;

  $transitions.onStart({
    to: function(state) {
      return state.data !== null && state.data.requireLogin === true;
    }
  }, function(trans) {
    if (!XucLink.isLogged()) {
      $log.info("Login required");
      return trans.router.stateService.target('login');
    }
  });

  $rootScope.showFatalError = function(errorMessage) {
    return $uibModal.open({
      templateUrl: 'ucassistant/fatalError.html',
      size: 'sm',
      windowClass: 'fatal-error',
      controller: 'FatalErrorController',
      resolve: {
        errorMessage: function() {
          return errorMessage;
        }
      }
    });
  };

  var unregisterFatalError = $rootScope.$on(CtiProxy.FatalError, function(event, errorCode) {
    $rootScope.showFatalError(errorCode);
    externalEvent.unRegisterExternalEventListener();
  });
  $rootScope.$on('$destroy', unregisterFatalError);
}