import $ from 'jquery';

(function(){
  'use strict';
  angular
    .module('ucAssistant')
    .controller('InitController',initController);

  initController.$inject= ['$scope', '$rootScope', '$state', '$window', '$log', 'XucUtils', '$translate', 'XucPhoneState', 'CtiProxy', 'callContext', 'XucLink'];

  function initController($scope, $rootScope, $state, $window, $log, xucUtils, $translate, XucPhoneState, CtiProxy, callContext, XucLink) {

    $scope.stripSpaces = function(str) {
      return str.replace(/[ ()\.]/g, "");
    };

    $scope.getPhoneStateLabel = function(state) {
      return "USER_STATUS_" + state;
    };

    var _dialOrSearch = function(){
      var providedValue = $scope.stripSpaces($("#numberTodial").val());
      var normalizedValue = xucUtils.normalizePhoneNb(providedValue);
      if (xucUtils.isaPhoneNb(normalizedValue)) {
        callContext.dialOrAttTrans(normalizedValue);
        $state.go("interface.callControl");
      }
      else {
        $state.go('interface.search', {'showFavorites': false, 'search': providedValue});
      }
    };

    $("#numberTodial").keypress(function(event) {
      if ( event.which == 13 ) {
        event.preventDefault();
        _dialOrSearch();
      }
    })
      .keyup(function() {
        var input = $('#numberTodial').val();
        var rewritten = $scope.stripSpaces(input);
        if(rewritten !== input) {
          $('#numberTodial').val(rewritten);
        }
        var normalizedValue = xucUtils.normalizePhoneNb(rewritten);
        if (xucUtils.isaPhoneNb(normalizedValue)) {
          $('#xuc_search i').removeClass('fa-search');
          $('#xuc_search i').addClass('fa-phone');
        } else {
          $('#xuc_search i').removeClass('fa-phone');
          $('#xuc_search i').addClass('fa-search');
        }
      });

    $('#xuc_search').click(function(){
      _dialOrSearch();
    });


    $scope.$on('linkDisConnected', function() {
      $log.warn('InitController - Received linkDisConnected');
      if (CtiProxy.isUsingWebRtc() && XucPhoneState.getCalls().length > 0) {
        $log.warn('CTI WS closed when CtiProxy is using WebRtc');
        $scope.showFatalError('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
      } else {
        $window.location.search = '';
        if(CtiProxy.isUsingWebRtc()) {
          CtiProxy.stopUsingWebRtc();
        }
        XucLink.logout();
        $state.go('login', {'error': 'LinkClosed'});
      }
    });
  }
})();
