import moment from 'moment';

(function() {
  'use strict';
  angular
    .module('ucAssistant')
    .controller('CallHistoryController', callHistoryController);

  callHistoryController.$inject = ['$scope', 'XucUtils', '$state', 'XucPhoneState', 'CtiProxy', 'XucCallHistory', '$translate', 'callContext'];

  function callHistoryController($scope, XucUtils, $state, XucPhoneState, CtiProxy, XucCallHistory, $translate, callContext) {
    $scope.history = [];

    $scope.statusImage = function(status) {
      var baseUrl = '/assets/images/ucassistant/call_statut_';
      if (status == 'emitted') return baseUrl + 'outgoing.svg';
      else if (status == 'answered') return baseUrl + 'incoming.svg';
      else if (status == 'missed') return baseUrl + 'missed.svg';
      return '';
    };

    $scope.dial = function(number) {
      callContext.normalizeDialOrAttTrans(number);
      $state.go('interface.callControl');
    };

    $scope.createContact = (number, firstname, lastname) => {
      let contact = {
        "firstname": firstname,
        "lastname": lastname,
        "number": number
      };
      $state.go('interface.personalContact', {"contact": contact });
    };

    $scope.formatHistoryDay = function(date) {
      return moment(date).calendar(null, {
        sameDay: '[' + $translate.instant('CALENDAR_SAME_DAY') + ']',
        lastDay: '[' + $translate.instant('CALENDAR_LAST_DAY') + ']',
        lastWeek: 'dddd [' + $translate.instant('CALENDAR_LAST_WEEK') + ']',
        sameElse: 'DD/MM/YYYY'
      });
    };

    $scope.formatHistoryTime = function(date) {
      return moment(date).format('HH:mm');
    };

    $scope.formatHistoryDuration = function(duration) {
      return duration;
    };

    $scope.init = function() {
      XucCallHistory.subscribeToUserCallHistory($scope, function(callHistory) {
        $scope.history = callHistory;
      });
      XucCallHistory.updateUserCallHistory();
    };
    $scope.init();
  }

})();
