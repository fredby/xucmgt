import 'jquery';
import 'jquery-ui';
import 'angular';
import 'angular-messages';
import 'angular-cookies';
import 'angular-local-storage';
import 'angular-translate';
import 'angular-translate-loader-partial';
import 'angular-ui-bootstrap';
import 'angular-ui-slider';
import '@uirouter/angularjs';
import moment from 'moment';

window.moment = moment;

import 'xchelper/helper.module';

/* Old libraries */
import 'xccti/cti-webpack';
import 'xclogin/login-webpack';

/* Application */
import config from './assistant.config';
import run from './assistant.run';
import './assistant.module.js';
import './controllers/InitController.controller.js';
import './controllers/CallHistory.controller.js';
import './controllers/Contact.controller.js';
import './controllers/User.controller.js';
import menuCtrl from './controllers/Menu.controller';
import personalContactCtrl from './controllers/PersonalContact.controller';
import confirmationModalCtrl from './controllers/ConfirmationModal.controller';
import forward from './services/forward.factory';
import personalContact from './services/personalContact.factory';
import processVolume from './services/processVolume.factory';
import volumeMeter from './directives/volumeMeter.directive';
import videoWrapper from './directives/videoWrapper.directive';


angular.module('ucAssistant')
  .config(config)
  .controller('MenuController',menuCtrl)
  .controller('PersonalContactController', personalContactCtrl)
  .controller('ConfirmationModalController', confirmationModalCtrl)
  .service('forward', forward)
  .service('personalContact', personalContact)
  .service('processVolume', processVolume)
  .directive('volumeMeter', volumeMeter)
  .directive('videoWrapper', videoWrapper)
  .run(run);
