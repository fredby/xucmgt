import 'angular-translate-loader-partial';

export default function config($stateProvider, $urlRouterProvider, $translateProvider, $translatePartialLoaderProvider, localStorageServiceProvider) {
  $stateProvider
    .state('login', {
      url: '/login?error',
      templateUrl: '/ucassistant/login.html',
      controller: 'LoginController',
      data: {
        requireLogin: false
      }
    })
    .state('interface', {
      url: '/ucassistant',
      templateUrl: '/ucassistant/main.html',
      controller: 'InitController',
      data: {
        requireLogin: true
      }
    })
    .state('interface.menu', {
      url: '/menu',
      onEnter: ['$state', '$uibModal', function($state, $uibModal) {
        $uibModal.open({
          templateUrl: "/ucassistant/menu.html",
          controller: 'MenuController as ctrl',
          windowClass: 'modal-right',
          keyboard: false
        }).result.then(angular.noop, angular.noop);
      }]
    })
    .state('interface.personalContact', {
      url: '/menu/personal-contact?id',
      params: {
        contact: {value: null, squash: true}
      },
      onEnter: ['$state', '$stateParams', '$uibModal', function($state, $stateParams, $uibModal) {
        $uibModal.open({
          templateUrl: "/ucassistant/personalContact.html",
          resolve: {
            contactId: () => {
              return $stateParams.id;
            },
            contactData: () => {
              return $stateParams.contact;
            }
          },
          controller: 'PersonalContactController as ctrl',
          windowClass: 'modal-right greyed',
          keyboard: false
        }).result.then(angular.noop, angular.noop);
      }]
    })
    .state('interface.favorites', {
      url: '/favorites',
      templateUrl: '/ucassistant/contacts.html',
      controller: "ContactsController",
      params: {
        showFavorites: true
      }
    })
    .state('interface.search', {
      url: '/search',
      templateUrl: '/ucassistant/contacts.html',
      controller: "ContactsController",
      params: {
        showFavorites: false,
        search: ''
      }
    })
    .state('interface.conferences', {
      url: '/conferences',
      templateUrl: '/ucassistant/conferences.html',
      controller: "ConfRoomController"
    })
    .state('interface.callControl', {
      url: '/callControl',
      templateUrl: '/ucassistant/callControl.html',
      controller: "CallControlController"
    })
    .state('interface.audioMeter', {
      url: '/callControl',
      templateUrl: '/ucassistant/audioMeter.html',
      controller: "CallControlController"
    })
    .state('interface.micMeter', {
      url: '/callControl',
      templateUrl: '/ucassistant/micMeter.html',
      controller: "CallControlController"
    })
    .state('interface.history', {
      url: '/history',
      templateUrl: '/ucassistant/history.html',
      controller: "CallHistoryController"
    });

  $urlRouterProvider.otherwise('/login');

  $translatePartialLoaderProvider.addPart('ucassistant');
  $translateProvider.useLoader('$translatePartialLoader', {
    urlTemplate: 'assets/i18n/{part}-{lang}.json'
  });
  $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
    'en_*': 'en',
    'fr_*': 'fr'
  });
  $translateProvider.preferredLanguage(document.body.getAttribute('data-preferredlang'));
  $translateProvider.fallbackLanguage(['fr']);
  $translateProvider.forceAsyncReload(true);

  localStorageServiceProvider.setPrefix('pratix');
  localStorageServiceProvider.setNotify(true, true);
}
