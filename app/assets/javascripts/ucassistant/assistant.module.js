import moment from 'moment';
import _ from 'lodash';

var assistant = angular.module('ucAssistant', ['xcCti', 'xcHelper', 'xcLogin', 'ui.bootstrap', 'pascalprecht.translate', 'ui.router', 'LocalStorageModule', 'ngMessages']);

assistant.controller('LoginController', ['$scope', '$state', '$stateParams', '$window', 'XucLink', 'XucVoiceMail', 'CtiProxy',
  function($scope, $state, $stateParams, $window, xucLink, XucVoiceMail, CtiProxy) {

    $scope.error = $stateParams.error;
    CtiProxy.stopUsingWebRtc();

    $scope.onLogin = function() {
      $state.go('interface.favorites', {
        'showFavorites': true
      });
    };

    $scope.$on('$locationChangeStart', function() {
      xc_webrtc.stop();
    });
    /* eslint-disable no-undef */
    $scope.hostAndPort = externalConfig.hostAndPort;
    $scope.useSso = externalConfig.useSso;
    $scope.casServerUrl = externalConfig.casServerUrl;
    $scope.casLogoutEnable = externalConfig.casLogoutEnable;
    /* eslint-enable no-undef */
  }
]);

assistant.controller('VoiceMailController', ['$scope', 'XucVoiceMail', 'CtiProxy', function($scope, XucVoiceMail, CtiProxy) {
  $scope.voiceMail = XucVoiceMail.getVoiceMail();

  $scope.$on('voicemailUpdated', function() {
    $scope.voiceMail = XucVoiceMail.getVoiceMail();
    $scope.$digest();
  });

  $scope.callMailBox = function() {
    CtiProxy.dial('*98');
  };

}]);

assistant.controller('ConfRoomController', ['$scope', 'XucConfRoom', function($scope, XucConfRoom) {
  $scope.confRooms = [];

  $scope.$on('ConfRoomsUpdated', function() {
    $scope.confRooms = XucConfRoom.getConferenceRooms();
    $scope.$digest();
  });

  $scope.getStartTime = function(confRoom) {
    if (confRoom.members.length > 0) {
      return moment(confRoom.startTime).format('HH:mm:ss');
    } else {
      return '-';
    }
  };

  $scope.init = function() {
    $scope.confRooms = XucConfRoom.requestConferenceRooms();
  };

  $scope.init();
}]);

assistant.controller('ForwardController', ['$scope', '$uibModal', function($scope, $uibModal) {
  var ModalInstanceCtrl = function($scope, $uibModalInstance, user, XucUtils, forward) {
    $scope.user = angular.copy(user);
    $scope.forwardTypes = forward.listFwdTypes();

    $scope.getForwardLabel = function(type) {
      return forward.getFwdLabel(type);
    };

    $scope.processForward = function() {
      forward.setForward($scope.user);
      $scope.close();
    };

    $scope.processDnd = function() {
      forward.setDnd($scope.user);
      $scope.close();
    };

    $scope.close = function() {
      $uibModalInstance.close();
    };

    $uibModalInstance.result.then(function () {}, function () {
      $scope.close();
    });

  };

  $scope.showForward = function() {

    $uibModal.open({
      templateUrl: 'forwardDialog.html',
      size: 'sm',
      controller: ModalInstanceCtrl,
      resolve: {
        user: function() {
          return $scope.user;
        },

      }
    });
  };
}]);

assistant.controller('ViewController', ['$scope', '$rootScope', '$state', 'localStorageService',
  'XucCallHistory', 'XucPhoneEventListener', '$transitions',
  function($scope, $rootScope, $state, localStorageService, XucCallHistory, XucPhoneEventListener, $transitions) {

    $scope.missedCalls = 0;

    var callHistoryCheckedAtKey = 'ViewController.callHistoryCheckedAt';
    var callHistoryCheckedAt;
    var callHistory = [];

    $scope.isActive = function(viewName) {
      return $state.is(viewName);
    };

    var callHistoryUpdated = function(_callHistory) {
      callHistory = _callHistory;
      updateMissedCalls();
    };

    var historyChecked = function() {
      callHistoryCheckedAt = moment();
      localStorageService.set(callHistoryCheckedAtKey, moment(callHistoryCheckedAt).toISOString());
      updateMissedCalls();
    };

    var updateMissedCalls = function() {
      $scope.missedCalls = _.sumBy(callHistory, function(day) {
        return _.sumBy(day.details, function(call) {
          return (call.status === 'missed' && moment(call.start).isAfter(callHistoryCheckedAt)) ? 1 : 0;
        });
      });
    };

    var processPhoneEvent = function(event) {
      switch (event.eventType) {
      case XucPhoneEventListener.EVENT_DIALING:
      case XucPhoneEventListener.EVENT_RINGING:
        $state.go('interface.callControl');
        break;
      }
    };

    var init = function() {
      var callHistoryCheckedAtStr = localStorageService.get(callHistoryCheckedAtKey) || "1970-01-01";
      callHistoryCheckedAt = moment(callHistoryCheckedAtStr);
      XucCallHistory.subscribeToUserCallHistory($scope, callHistoryUpdated);
      XucCallHistory.updateUserCallHistory();
      XucPhoneEventListener.addHandler($scope, processPhoneEvent);
      $transitions.onSuccess({}, trans => {
        if (trans.$to().name == 'interface.history') {
          historyChecked();
        }
      });
    };
    init();

  }
]);

assistant.controller('PhoneController', function($rootScope, $scope, $timeout, CtiProxy) {

  $scope.attendedTransfer = function() {
    CtiProxy.attendedTransfer($scope.destination);
  };
  $scope.completeTransfer = function() {
    CtiProxy.completeTransfer();
  };
  $scope.cancelTransfer = function() {
    CtiProxy.cancelTransfer();
  };
  $scope.answer = function() {
    CtiProxy.answer();
  };
  $scope.hangup = function() {
    CtiProxy.hangup();
  };
  $scope.conference = function() {
    CtiProxy.conference();
  };
  $scope.hold = function() {
    CtiProxy.hold();
  };
});

assistant.controller('CallControlController', function($scope, $interval, $uibModal, $timeout, XucPhoneState, incomingCall, onHold, CtiProxy, keyboard) {
  $scope.calls = XucPhoneState.getCalls();
  $scope.conference = XucPhoneState.getConference();
  
  $scope.hold = function() {
    CtiProxy.hold();
  };

  $scope.hangup = function() {
    CtiProxy.hangup();
  };

  $scope.answer = function() {
    CtiProxy.answer();
  };

  $scope.makeConference = function() {
    CtiProxy.conference();
  };

  $scope.transfer = function() {
    CtiProxy.completeTransfer();
  };

  $scope.conferenceMuteMe = function(conferenceNumber) {
    CtiProxy.conferenceMuteMe(conferenceNumber);
  };

  $scope.conferenceUnmuteMe = function(conferenceNumber) {
    CtiProxy.conferenceUnmuteMe(conferenceNumber);
  };

  $scope.conferenceMuteAll = function(conferenceNumber) {
    CtiProxy.conferenceMuteAll(conferenceNumber);
  };

  $scope.conferenceUnmuteAll = function(conferenceNumber) {
    CtiProxy.conferenceUnmuteAll(conferenceNumber);
  };

  $scope.conferenceMute = function(conferenceNumber, index) {
    CtiProxy.conferenceMute(conferenceNumber, index);
  };

  $scope.conferenceUnmute = function(conferenceNumber, index) {
    CtiProxy.conferenceUnmute(conferenceNumber, index);
  };

  $scope.conferenceKick = function(conferenceNumber, participant) {
    participant.kicked = true;
    CtiProxy.conferenceKick(conferenceNumber, participant.index);
  };

  $scope.canMuteAll = function(conference) {
    if(conference.currentUserRole == 'Organizer') {
      var nbUnmuted = _.filter(conference.participants, {'isMe': false, 'isMuted': false}).length;
      if(nbUnmuted > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  };

  $scope.canUnmuteAll = function(conference) {
    if(conference.currentUserRole == 'Organizer' && !$scope.canMuteAll(conference)) {
      var nbMuted = _.filter(conference.participants, {'isMe': false, 'isMuted': true}).length;
      if(nbMuted > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  };

  $scope.displayParticipantActions = function(conference, participant) {
    if(!participant.kicked && (participant.isMe || conference.currentUserRole == 'Organizer')) {
      return true;
    } else {
      return false;
    }
  };

  $scope.faClassFromState = function(state) {
    switch (state) {
    case XucPhoneState.STATE_RINGING:
    case XucPhoneState.STATE_DIALING:
      return "fa-spinner fa-pulse fa-fw";
    case XucPhoneState.STATE_ESTABLISHED:
      return "fa-play";
    case XucPhoneState.STATE_ONHOLD:
      return "fa-pause";
    }
    return "fa-exclamation-triangle";
  };

  $scope.isWebRtcActive = function() {
    return CtiProxy.isUsingWebRtc();
  };

  $scope.isVideo = function(call) {
    return call.mediaType === xc_webrtc.mediaType.AUDIOVIDEO;
  };

  $scope.isAnyCallVideo = function(calls) {
    return typeof _.find(calls, function(call) { return call.mediaType === xc_webrtc.mediaType.AUDIOVIDEO; }) !== 'undefined';
  };

  $scope.isConferenceCapable = function() {
    return CtiProxy.isConferenceCapable();
  };

  $scope.showKeyboard = function() {
    return keyboard.show();
  };
});
