export default class ccQueueDynViewController {

  constructor($scope, NgTableParams, $filter, $log, XucQueueTotal, ViewColBuilder, Preferences, Thresholds, XucQueueRecording) {
    this.preferences = Preferences;
    this.thresholds = Thresholds;
    this.$scope = $scope;

    XucQueueRecording.getState().then(response => {
      $scope.option = {
        recordingQueue: this.preferences.addRecordingOption($scope,"SWITCH_QUEUES_RECORDING", this.preferences.CheckboxFreeTypeOption, response)
      };
    }, (error) => {
      XucQueueRecording.handleError(error);
    });

    XucQueueRecording.subscribeToQueueConfig(this.$scope, this.onQueueConfigUpdate.bind(this));

    var availCols = [];
    ViewColBuilder.setPrefix('COL');
    availCols.push(ViewColBuilder.col('number').withFieldClass('badge').build());
    availCols.push(ViewColBuilder.col('displayName').build());
    availCols.push(ViewColBuilder.col('WaitingCalls').build());
    availCols.push(ViewColBuilder.col('LongestWaitTime').withDisplayFilter('queueTime').build());
    availCols.push(ViewColBuilder.col('EWT').withDisplayFilter('queueTime').build());
    availCols.push(ViewColBuilder.col('AvailableAgents').build());
    availCols.push(ViewColBuilder.col('TalkingAgents').build());
    availCols.push(ViewColBuilder.col('TotalNumberCallsEntered').build());
    availCols.push(ViewColBuilder.col('TotalNumberCallsAnswered').build());
    availCols.push(ViewColBuilder.col('PercentageAnsweredBefore15').withDisplayFilter('number:1').build());
    availCols.push(ViewColBuilder.col('TotalNumberCallsAbandonned').build());
    availCols.push(ViewColBuilder.col('PercentageAbandonnedAfter15').withDisplayFilter('number:1').build());
    availCols.push(ViewColBuilder.col('TotalNumberCallsClosed').build());
    availCols.push(ViewColBuilder.col('TotalNumberCallsTimeout').build());

    $scope.totals = {};
    $scope.totals.WaitingCalls = {value:'stats.sum.WaitingCalls'};
    $scope.totals.LongestWaitTime = {value:'stats.max.LongestWaitTime | queueTime'};
    $scope.totals.EWT = {value:'stats.max.EWT | queueTime'};
    $scope.totals.TotalNumberCallsEntered = {value:'stats.sum.TotalNumberCallsEntered'};
    $scope.totals.TotalNumberCallsAnswered = {value:'stats.sum.TotalNumberCallsAnswered'};
    $scope.totals.PercentageAnsweredBefore15 = {value:'stats.global.PercentageAnsweredBefore15 | number:1'};
    $scope.totals.TotalNumberCallsAbandonned = {value:'stats.sum.TotalNumberCallsAbandonned'};
    $scope.totals.PercentageAbandonnedAfter15 = {value:'stats.global.PercentageAbandonnedAfter15 | number:1'};
    $scope.totals.TotalNumberCallsClosed = {value:'stats.sum.TotalNumberCallsClosed'};
    $scope.totals.TotalNumberCallsTimeout = {value:'stats.sum.TotalNumberCallsTimeout'};



    $scope.init = () => {
      $scope.cols = this.preferences.getViewColumns(availCols, this.preferences.QueueViewColumnsKey);
      $scope.stats = XucQueueTotal.getCalcStats();
    };

    $scope.tableSettings = function() {
      var calculatePage = function(params) {
        if (params.total() <= (params.page() - 1)  * params.count()) {
          var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
          params.page(setPage);
        }
      };
      return {
        total: $scope.queues.length, // length of data
        counts:[],
        getData: function(params) {
          var orderedData = params.sorting() ?
          $filter('orderBy')($scope.queues, params.orderBy()) :
          $scope.queues;
          params.total(orderedData.length);
          XucQueueTotal.calculate(orderedData);
          $scope.stats = XucQueueTotal.getCalcStats();
          calculatePage(params);
          return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
        }
      };
    };

    $scope.queueDynTable = new NgTableParams({
      page: 1,
      count: 50,
      sorting: {
        displayName: 'asc'
      }
    }, $scope.tableSettings()
    );

    $scope.$watch('queues', function(newQueues) {
      if (newQueues.length>0) $scope.queueDynTable.reload();
    },true);

    $scope.$on(this.preferences.QueueViewColumnsChanged, () => {
      $scope.cols = this.preferences.getViewColumns(availCols, this.preferences.QueueViewColumnsKey);
    });

    $scope.$on('moveTableCol', (event,args) => {
      this.preferences.moveCol(args.colRef, args.beforeColRef, $scope.cols, this.preferences.QueueViewColumnsKey);
    });

    $scope.thresholdClass = (field, value) => {
      return this.thresholds.thresholdClass(field,value);
    };

    $scope.hasError = () => {
      return XucQueueRecording.hasConfigManagerError();
    };

    $scope.init();
  }

  onQueueConfigUpdate(value) {
    this.preferences.setRecordingOptionValue(this.$scope.option.recordingQueue, value);
  }
}
