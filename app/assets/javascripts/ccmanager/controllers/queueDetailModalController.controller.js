export default class queueDetailModalController {

  constructor($uibModalInstance, queue) {
    this.queue = queue;
    this.$uibModalInstance = $uibModalInstance;
    this.$uibModalInstance.result.then(() => {}, () => {
      this.close();
    });
  }

  close() {
    this.$uibModalInstance.close();
  }
}