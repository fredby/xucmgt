import _ from 'lodash';

(function() {
  'use strict';

  angular.module('ccManager').controller('agentsCreateDefaultConfigurationController', agentsCreateDefaultConfigurationController);

  agentsCreateDefaultConfigurationController.$inject = ['$scope', '$uibModalInstance', 'NgTableParams', 'XucQueue', 'XucTableHelper', 'XucMembership', 'selectedAgents', 'selectedQueues'];

  function agentsCreateDefaultConfigurationController($scope, $uibModalInstance, NgTableParams, xucQueue, XucTableHelper, xucMembership, selectedAgents, selectedQueues) {

    $scope.selectedAgents = selectedAgents;
    $scope.queues = selectedQueues;
    $scope.addableQueues = [];
    $scope.queuesTable = new NgTableParams({count: XucTableHelper.POPUP_TABLE_ROWS}, XucTableHelper.createSettings($scope.queues));

    refreshQueues();

    function refreshQueues() {
      $scope.newQueue = {queue: null, penalty:0};
      $scope.addableQueues = xucQueue.getQueuesExcept($scope.queues);
      $scope.queuesTable.reload();
    }

    $scope.classPenalty = function(penalty) {
      if (typeof penalty === 'undefined' || penalty === null) return 'no-penalty';
      return '';
    };

    $scope.addQueue = function(queue, penalty) {
      var q = _.clone(queue);
      q.penalty = penalty;
      $scope.queues.push(q);
      refreshQueues();
    };

    $scope.removeQueue = function(queue) {
      $scope.queues.splice($scope.queues.indexOf(queue), 1);
      refreshQueues();
    };

    $scope.ok = function() {
      var userIds = _.map(selectedAgents, 'userId');
      var membership = _.map($scope.queues, function(q) {
        return {queueId: q.id, penalty: q.penalty};
      });

      xucMembership.setUsersDefaultMembership(userIds, membership);
      $uibModalInstance.dismiss('ok');
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };
  }

})();
