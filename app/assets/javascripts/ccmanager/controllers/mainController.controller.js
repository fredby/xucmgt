(function() {
  'use strict';

  angular.module('ccManager').controller('mainController', mainController);

  mainController.$inject = [ '$scope', 'XucLink', 'XucUser', '$state', 'Preferences'];
  function mainController($scope, xucLink, xucUser, $state, preferences) {

    $scope.logout = function() {
      xucLink.logout();
      preferences.clearOptions();
      $state.go("login", {error: "Logout"});
    };

    $scope.$on('linkDisConnected', function() {
      preferences.clearOptions();
      $state.go('login', {'error': 'LinkClosed'});
    });

    xucUser.getUserAsync().then(function(user) { $scope.user = user; });
  }
})();
