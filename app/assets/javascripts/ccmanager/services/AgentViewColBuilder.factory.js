import _ from 'lodash';

(function(){
  'use strict';

  angular
  .module('ccManager')
  .factory('AgentViewColBuilder',AgentViewColBuilder);

  AgentViewColBuilder.$inject= ['ViewColBuilder','XucAgent', 'AgentStates', 'XucGroup', '$q', '$compile', '$translate'];

  function AgentViewColBuilder(viewColBuilder, xucAgent, agentStates, XucGroup, $q, $compile, $translate) {
    var _deferredGroupData = $q.defer();

    var _getGroupData = function() {
      return _deferredGroupData.promise;
    };

    var _resolveGroupData = function() {
      var groups = _.map(XucGroup.getGroups(), function (g) {
        return {id: g.name, label: g.name};
      });
      _deferredGroupData.resolve(groups);
    }; 

    var _getMatchDefaultMembershipData = function() {
      return [
        {id: 'NO_DEFAULT_MEMBERSHIP', label: $translate.instant('NO_DEFAULT_MEMBERSHIP')},
        {id: 'MATCH_DEFAULT_MEMBERSHIP', label: $translate.instant('MATCH_DEFAULT_MEMBERSHIP')},
        {id: 'DOES_NOT_MATCH_DEFAULT_MEMBERSHIP', label: $translate.instant('DOES_NOT_MATCH_DEFAULT_MEMBERSHIP')}
      ];
    };

    var _multiSelectTemplate = "assets/javascripts/ccmanager/services/dropdownMultiSelect.html";
    var _buildFirstCols = function() {
      var firstCols = [];
      firstCols.push({id:'emptyCol', show: true, showTotal: false});
      firstCols.push({id:'selectCol', type:'check', field:'selected', show: true, showTotal: true, filter:{name : 'assets/javascripts/ccmanager/services/selectAgent.html'}, cellClass:'AgentSelectBox'});
      firstCols.push({id:'editCol', type:'edit', cellClass:"actionIcon text-center", show: true, showTotal: true, filter:{name : 'assets/javascripts/ccmanager/services/editAgent.html'}});
      return firstCols;
    };

    var _buildCols = function() {
      viewColBuilder.setPrefix('COL_AGENT');
      var availCols = [];

      availCols.push(viewColBuilder
        .col('matchDefaultMembership')
        .withDisplayFilter('translate')
        .withFilter(_multiSelectTemplate)
        .withFilterData(_getMatchDefaultMembershipData)
        .withCellClass("colDefaultMembership")
        .build());

      availCols.push(viewColBuilder.col('number').withFieldClass('badge').withFieldDynClass('state').build());
      availCols.push(viewColBuilder.col('groupName').withFilter(_multiSelectTemplate).withFilterData(_getGroupData).hide().build());
      availCols.push(viewColBuilder.col('firstName').build());
      availCols.push(viewColBuilder.col('lastName').build());
      availCols.push(viewColBuilder.col('stats.LoginDateTime').withDisplayFilter('dateTime').withNoFilter().build());
      availCols.push(viewColBuilder.col('stats.LogoutDateTime').withDisplayFilter('dateTime').withNoFilter().build());
      availCols.push({id:'actionCol', type:'action', cellClass:"actionIcon", show: true, showTotal: true});
      var stateCol = viewColBuilder.col('stateName').withCellClass('T{{agent.state}}').withFilter(_multiSelectTemplate)
          .withDisplayFilter('translate').withFieldDynClass('state').withFilterData(agentStates.buildStates().promise).build();
      stateCol.fieldClass = 'state';
      availCols.push(stateCol);
      availCols.push(viewColBuilder.col('timeInState').withNoFilter().withDisplayFilter('timeInState').build());
      availCols.push(viewColBuilder.col('phoneNb').build());
      availCols.push(viewColBuilder.col('stats.PausedTime').withDisplayFilter('totalTime').withNoFilter().build());

      var pauseStatuses = xucAgent.getNotReadyStatuses();
      angular.forEach(pauseStatuses, function(status){
        availCols.push(viewColBuilder.col('stats.'+status.name).withDisplayFilter('totalTime').withNoFilter().withTitle(status.longName).build());
      });
      availCols.push(viewColBuilder.col('stats.WrapupTime').withDisplayFilter('totalTime').withNoFilter().build());

      availCols.push(viewColBuilder.col('stats.InbCalls').withNoFilter().hide().build());
      availCols.push(viewColBuilder.col('stats.InbAcdCalls').withNoFilter().build());
      availCols.push(viewColBuilder.col('stats.InbAnsCalls').withNoFilter().hide().build());
      availCols.push(viewColBuilder.col('stats.InbAnsAcdCalls').withNoFilter().build());
      availCols.push(viewColBuilder.col('stats.InbAverCallTime').withDisplayFilter('totalTime').withNoFilter().hide().build());
      availCols.push(viewColBuilder.col('stats.InbAverAcdCallTime').withDisplayFilter('totalTime').withNoFilter().build());
      availCols.push(viewColBuilder.col('stats.InbCallTime').withDisplayFilter('totalTime').withNoFilter().hide().build());
      availCols.push(viewColBuilder.col('stats.InbAcdCallTime').withDisplayFilter('totalTime').withNoFilter().build());
      availCols.push(viewColBuilder.col('stats.InbUnansCalls').withNoFilter().hide().build());
      availCols.push(viewColBuilder.col('stats.InbUnansAcdCalls').withNoFilter().build());
      availCols.push(viewColBuilder.col('stats.InbPercUnansCalls').withNoFilter().withDisplayFilter('number:1').hide().build());
      availCols.push(viewColBuilder.col('stats.InbPercUnansAcdCalls').withNoFilter().withDisplayFilter('number:1').build());
      availCols.push(viewColBuilder.col('stats.OutCalls').withNoFilter().build());
      availCols.push(viewColBuilder.col('stats.OutCallTime').withDisplayFilter('totalTime').withNoFilter().build());

      return availCols;
    };

    var _buildTable = function($scope) {
      if(XucGroup.getGroups().length > 0) {
        _resolveGroupData();
      } else {
        $scope.$on("GroupsLoaded", _resolveGroupData);
      }
      var colType  = function(type) {
        return " ng-if=col.type==='"+type+"' ";
      };
      var actionItem = function(canFn, actionFn, icon, label) {
        return '<li ng-show="'+canFn+'(agent.id)" ng-click="'+actionFn+'(agent.id)" role="presentation" class="AgentMenu">' +
                '<i class="glyphicon glyphicon-'+icon+' glyphmenu"></i>{{"'+label+'" | translate}}</li>';
      };

      var colAction ='<div '+ colType('action') + 'ng-controller="agentActionController" uib-dropdown class="dropdown dropdown-menu-right">' +
                              '<a uib-dropdown-toggle href class="pull-right" id="dLabel">' +
                              '<i class="glyphicon glyphicon-wrench"></i>' +
                              '</a>' +
                              '<ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="dLabel">' +
                              actionItem("canLogIn", "login", "log-in","ACTION_LOGIN") +
                              actionItem("canLogOut", "logout", "log-out","ACTION_LOGOUT") +
                              actionItem("canPause", "pause", "pause","ACTION_PAUSE") +
                              actionItem("canUnPause", "unpause", "play","ACTION_UNPAUSE") +
                              actionItem("canListen", "listen", "bullhorn","ACTION_LISTEN") +
                              actionItem("canBeCalled", "callAgent", "earphone","ACTION_CALL") +
                          '</div>';


      var colEdit = '<span ' + colType('edit') + 'ng-controller="agentEditController" class="glyphicon glyphicon-pencil" ng-click="editAgent(agent.id)"></span>';
      var colField = '<span ' + colType('field') + 'class="{{col.fieldClass}} T{{agent[col.fieldDynClass]}}" ng-bind="agent[col.field] | editableFilter:col.displayFilter"></span>';
      var colStat  = '<span ' + colType('stat') + 'class="{{col.fieldClass}} T{{agent[col.fieldDynClass]}}" ng-bind="agent.stats[col.field]  | editableFilter:col.displayFilter"></span>';
      var colCheck = '<span ' + colType('check') + 'class="agent-check" ><input type="checkbox" ng-model="agent[col.field]"></span>';
      var agentField = colCheck + colEdit + colField + colStat + colAction;

      var agentViewTableWithGroup = '<table ng-table-dynamic="agentDynTable with cols" cols="cols" show-filter="true" class="table table table-condensed flex-layout">' +
              '<tbody ng-repeat="group in $groups | orderBy:'+"'"+'value'+"'"+'" ng-init="$groupRow.show = false">' +
                  '<tr class="ng-table-group agent-table-group-data">' +
                      '<td>' +
                          '<a href="" ng-click="group.$hideRows = !group.$hideRows">' +
                              '<span class="glyphicon" ng-class="{ '+"'glyphicon-chevron-right'"+': group.$hideRows, '+"'glyphicon-chevron-down'"+': !group.$hideRows }"></span>' +
                              '<strong> {{ group.value }} ({{group.data.length}})</strong>' +
                          '</a>' +
                      '</td>' +
                      '<td ng-if="(col.showTotal && col.show)" ng-repeat="col in cols">{{totals[group.value][col.field] | editableFilter:col.displayFilter}}</td>' +
                  '</tr>' +
                  '<tr ng-if="!group.$hideRows" ng-repeat="agent in group.data">' +
                      '<td class="{{col.cellClass}} T{{agent[col.fieldClass]}}" data-header-class="'+"'tableTitle'"+'" ng-repeat="col in $columns">'+
                         agentField +
                      '</td>' +
                  '</tr>' +
              '</tbody>' +
          '</table>';


      var agentViewTable = '<table ng-table-dynamic="agentDynTable with cols" show-filter="true" class="table table-condensed table-container" fixed-header>' +
                              '<tr ng-repeat="agent in $data">' +
                                   '<td class="{{col.cellClass}} T{{agent[col.fieldClass]}}" data-header-class="" ng-repeat="col in $columns">' +
                                      agentField +
                                   '</td>' +
                              '</tr>' +
                            '</table>';

      var newDirective = angular.element($scope.showGroup ? agentViewTableWithGroup : agentViewTable);
      var element = angular.element( document.querySelector( '#agtView' ) );
      element.empty();
      element.append(newDirective);
      $compile(newDirective)($scope);
    };
    return {
      buildCols: _buildCols,
      buildFirstCols: _buildFirstCols,
      buildTable: _buildTable
    };
  }
})();
