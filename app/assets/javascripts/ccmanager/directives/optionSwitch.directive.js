export default function optionSwitch() {
  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccmanager/directives/optionSwitch.html',
    scope: {
      groupTitle: '@',
      list: '=',
      onSwitch: '='
    }
  };
}