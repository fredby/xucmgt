var ccManager = angular.module('ccManager');
ccManager.directive('multiSelect', function(){
  return {
    restrict: 'E',
    template: '<div ng-dropdown-multiselect="" options="options" selected-model="modelWrapper" translation-texts="mstranslation"></div>',
    scope: {
      selectedModel: "=",
      options: "="
    },
    controller: function($scope){
      $scope.modelWrapper = [];
      $scope.$watchCollection('modelWrapper', (newValue) => {
        $scope.selectedModel = newValue;
      });
    }

  };
});
