export default function config(localStorageServiceProvider, $logProvider, $translateProvider, $translatePartialLoaderProvider, $httpProvider, $stateProvider, $urlRouterProvider) {

  /*jshint -W087*/
  //debugger;
  localStorageServiceProvider.setPrefix('ccManager.default');
  localStorageServiceProvider.setNotify(true, true);
  $logProvider.debugEnabled(false);
  Cti.debugMsg = false;

  $translatePartialLoaderProvider.addPart('ccmanager');
  $translateProvider.useLoader('$translatePartialLoader', {
    urlTemplate: 'assets/i18n/{part}-{lang}.json'
  });
  $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
    'en_*': 'en',
    'fr_*': 'fr'
  });
  $translateProvider.preferredLanguage(document.body.getAttribute('data-preferredlang'));
  $translateProvider.fallbackLanguage(['fr']);
  $translateProvider.forceAsyncReload(true);

  $stateProvider
    .state('login', {
      url: '/login?error',
      templateUrl: '/ccmanager/login',
      controller: 'LoginController',
      data: {
        requireLogin: false
      }
    })
    .state('interface', {
      url: '/interface',
      templateUrl: '/ccmanager/content',
      //controller: 'InitController',
      data: {
        requireLogin: true
      }
    });
  $urlRouterProvider.otherwise('/login');
}
