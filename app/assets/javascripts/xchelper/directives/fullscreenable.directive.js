export default function fullscreenable() {

  return {
    restrict: 'EA',
    transclude: false,
    controller: ($scope, $element, $log, $document) => {

      $scope.elem = $element[0];
      $scope._document = $document;
      $log.debug('elem: ', $scope.elem);

      $scope._requestFullScreen = function() {
        $log.debug('Going fullscreen');
        $scope.elem.webkitRequestFullscreen();
      };

      $scope._exitFullScreen = function() {
        $log.debug('Leaving fullscreen');
        $scope._document[0].webkitExitFullscreen();
      };

      $scope.toggleFullscreen = function() {
        if (!$scope._document[0].webkitIsFullScreen) {
          $scope._requestFullScreen();
        } else {
          if ($scope._document[0].webkitExitFullscreen) {
            $scope._exitFullScreen();
          }
          else {
            $log.error('Unable to exit fullscreen, webkit method unavailable');
            alert('Unable to exit fullscreen, use Esc key');
          }
        }
      };
    }
  };
}
