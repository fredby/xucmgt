import _ from 'lodash';

export default function callActionDropdown(){
  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/xchelper/directives/callActionDropdown.html',
    scope: {
      contact: "=",
      phoneNumber: "=",
      stacked: "=",
      emailMaxChars: '@'
    },
    link: function(scope, elem, attrs) {
      scope.$eval(attrs.contact);
      scope.icon = angular.isDefined(attrs.icon) ? attrs.icon : 'fa-phone';
      scope.type = angular.isDefined(attrs.type) ? attrs.type : 'audio';

      scope.phoneNumbers = [];

      var init = function() {
        if(_.isObject(scope.contact)) {
          _.forEach([1, 2, 3], function (index) {
            if (!_.isEmpty(scope.contact.entry[index])) {
              scope.phoneNumbers.push(scope.contact.entry[index]);
            }
          });

          if (!_.isEmpty(scope.contact.entry[5])) {
            scope.email = scope.contact.entry[5];
          }
        }
      };
      init();
    }
  };
}
