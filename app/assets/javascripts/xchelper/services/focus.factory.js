export default function focus($timeout, $window) {

  return function(id) {
    $timeout(() => {
      let element = $window.document.getElementById(id);
      if(element)
        element.focus();
    },100);
  };
}