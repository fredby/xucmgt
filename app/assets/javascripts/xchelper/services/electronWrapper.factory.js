import _ from 'lodash';

export default function electronWrapper($window) {
  const obj = $window;
  const path = 'setElectronConfig';
  const isElectron = _.isFunction(_.get(obj, path));

  const _isElectron = () => {
    return isElectron;
  };

  const _setFocus = () => {
    if (isElectron) {
      _execFn({focus : true});
    }
  };

  const _setElectronConfig = (width, height, title, minimalist = false) => {
    if (isElectron) {
      let data = {};
      Object.assign(data,
        title && {title: title},
        (width && height) && {size: {w: width, h: height, mini: minimalist}}
      );
      _execFn(data);
    }
  };

  let _execFn = function() {
    return _.spread(_.bindKey(obj, path))(arguments);
  };

  return {
    isElectron: _isElectron,
    setElectronConfig: _setElectronConfig,
    setFocus: _setFocus
  };
}