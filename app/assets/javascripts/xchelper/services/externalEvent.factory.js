class Message {
  constructor(type, value) {
    this.type = type;
    this.value = value;
  }
  toJSON() {
    return {
      type: this.type,
      value: this.value
    };
  }
}

export default function externalEvent($window, $location, $log, XucPhoneState, CtiProxy, XucUtils, debounce) {
  var targetLocation = '//'+($location.host() ? $location.host() : 'localhost');
  var thirdPartyCallback;
  const thirdPartyEvent = {type: 'THIRD_PARTY_EVENT', value: {}};
  const unserializableMessage = new Message('ERROR', 'Unable to serialize data');

  var _registerEventListener = function() {
    angular.element($window).on('message', function (event) {
      _handleEvent(event.originalEvent || event);
    });
  };

  var _unRegisterEventListener = function(){
    angular.element($window).off('message', _registerEventListener);
  };

  var _registerThirdPartyCallback = function(cb) {
    thirdPartyCallback = cb;
  };

  var _triggerAction = function (data) {
    var result = 'External action triggered: ';
    var state = XucPhoneState.getState();

    if (XucPhoneState.isPhoneOffHook(state)) {
      CtiProxy.hangup();
      result += 'Hangup';
    }
    else if (XucPhoneState.isPhoneRinging(state)){
      CtiProxy.answer();
      result += 'Answer';
    }
    else {
      CtiProxy.dial(data);
      result += 'Dial ' + data;
    }
    $log.info(result);
  };

  var _handleEvent = (function (event) {
    let data;

    if(angular.isUndefined(event) || event === null || event.data === null || event.origin === null) {
      $log.error('Cannot handle event received, content or origin is empty');
      return;
    }

    if(event.data === "closeThirdParty") { data = thirdPartyEvent; }
    else {
      if (_globalKeyEventChecks(event)) { data = event.data; }
      else {
        $log.error('Cannot handle event received, source or target is not reliable' + JSON.stringify(event));
        return;
      }
    }

    var message = _deserializeData(data);

    switch (message.type) {
    case 'PHONE_EVENT':
      _handleEventDebounced(XucUtils.normalizePhoneNb(message.value));
      break;
    case 'THIRD_PARTY_EVENT':
      if (thirdPartyCallback) {
        thirdPartyCallback();
      }
      else {
        $log.error('No callback function is defined for third party event');
      }
      break;
    case 'ERROR':
      $log.error(message.value);
      break;
    default:
      $log.error('Message type from event is not handled by this service' + message.type);
    }
  });

  var _globalKeyEventChecks = (function(event) {
    return !(event.source !== event.target || targetLocation != event.origin.split(':')[1]);
  });

  var _handleEventDebounced = debounce(_triggerAction);

  var _deserializeData = (function(data) {
    try {
      let item = angular.fromJson(data);
      return new Message(item.type, item.value);
    }
    catch (exception) {
      var message = unserializableMessage;
      message.value += data;
      return message;
    }
  });

  //instantiate factory
  _registerEventListener();
  return {
    registerExternalEventListener: _registerEventListener,
    unRegisterExternalEventListener: _unRegisterEventListener,
    registerThirdPartyCallback: _registerThirdPartyCallback,
    handleEvent: _handleEvent
  };
}
