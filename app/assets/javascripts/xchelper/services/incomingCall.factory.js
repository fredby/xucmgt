export default function incomingCall($rootScope, $uibModal, $log, XucPhoneEventListener, XucCallNotification) {
  var _modalInstance = null;
  XucCallNotification.enableNotification();

  var _init = function() {};

  var _showModal = function(number, name, callType) {
    return $uibModal.open({
      templateUrl: 'assets/javascripts/xchelper/services/incomingcall.html',
      size: 'sm',
      controller: 'IncomingCallPopup',
      resolve: {
        incomingCallNumber: function() {
          return number;
        },
        incomingCallName: function() {
          return name.trim();
        },
        callType: function() {
          return callType;
        }

      }
    });
  };

  var _onCallRinging = function(event) {
    if(_modalInstance !== null) {
      return;
    }
    _modalInstance = _showModal(event.otherDN, event.otherDName, event.callType);
    _modalInstance.result.then(
      function() {
        _modalInstance = null;
        $log.debug("Closed");
      },
      function() {
        _modalInstance = null;
        $log.debug("Closed");
      }
    );
  };

  XucPhoneEventListener.addRingingHandler($rootScope, _onCallRinging);

  return {init: _init};

}
