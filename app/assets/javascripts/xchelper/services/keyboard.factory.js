export default function keyboard($rootScope, $uibModal) {

  var _show = () => {
    return $uibModal.open({
      templateUrl: 'assets/javascripts/xchelper/services/keyboard.html',
      size: 'keyboard',
      controller: 'KeyboardPopup',
      windowClass: 'phone-keyboard',
    });
  };

  return {show: _show};

}
