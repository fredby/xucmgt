import angular from 'angular';
import uibootstrap from 'angular-ui-bootstrap';
import uirouter from '@uirouter/angularjs';
import angulartranslate from 'angular-translate';
import ngclipboard from 'ngclipboard';
import uiselect from 'ui-select';
import 'ui-select/dist/select.css';
import localStorageService from 'angular-local-storage';

import config from './helper.config';
import dateInput from './directives/dateInput.directive.js';
import longText from './directives/longText.directive.js';
import fixedHeader from './directives/fixedTableHeader.directive.js';
import validPhoneNumber from './directives/validPhoneNumber.directive.js';
import elapsedTimeFrom from './directives/elapsedTimeFrom.directive.js';
import fullscreenable from './directives/fullscreenable.directive.js';
import scrollText from './directives/scrollText.directive.js';
import callActionDropdown from './directives/callActionDropdown.directive';
import indeterminate from './directives/checkboxIndeterminate.directive.js';
import fileInput from './directives/fileInput.directive.js';
import audioConfiguration from './directives/audioConfiguration.directive';
import debounce from './services/debounce.factory.js';
import externalEvent from './services/externalEvent.factory.js';
import webRtcAudio from './services/webRtcAudio.factory.js';
import incomingCall from './services/incomingCall.factory';
import callContext from './services/callContext.factory';
import keyboard from  './services/keyboard.factory';
import electronWrapper from './services/electronWrapper.factory';
import focus from './services/focus.factory';
import dragDrop from './services/dragDrop.factory';
import fileReader from './services/fileReader.factory';
import onHold from './services/onHold.factory';
import onHoldNotifier from './services/onHoldNotifier.factory';
import mediaDevices from './services/mediaDevices.factory';
import xcHelperPreferences from './services/xcHelperPreferences.factory';
import hour from './filters/hour.filter.js';
import trusted from './filters/trusted.filter.js';
import CallAction from './controllers/callAction.controller';
import IncomingCallPopup from './controllers/incomingCallPopup.controller';
import OnHoldPopup from './controllers/onHoldPopup.controller';
import KeyboardPopup from  './controllers/keyboardPopup.controller';


angular.module('xcHelper', [uirouter, uibootstrap, angulartranslate, ngclipboard, uiselect, localStorageService])
  .config(config)
  .directive('dateInput', dateInput)
  .directive('longText', longText)
  .directive('fixedHeader', fixedHeader)
  .directive('validPhoneNumber', validPhoneNumber)
  .directive('elapsedTimeFrom', elapsedTimeFrom)
  .directive('fullscreenable', fullscreenable)
  .directive('scrollText', scrollText)
  .directive('callActionDropdown', callActionDropdown)
  .directive('indeterminate', indeterminate)
  .directive('fileInput', fileInput)
  .directive('audioConfiguration', audioConfiguration)
  .filter('hour', hour)
  .filter('trusted', trusted)
  .service('debounce', debounce)
  .service('externalEvent', externalEvent)
  .service('webRtcAudio', webRtcAudio)
  .service('callContext', callContext)
  .service('incomingCall', incomingCall)
  .service('keyboard', keyboard)
  .service('mediaDevices', mediaDevices)
  .factory('electronWrapper', electronWrapper)
  .factory('focus', focus)
  .factory('dragDrop', dragDrop)
  .factory('fileReader', fileReader)
  .factory('onHold', onHold)
  .factory('onHoldNotifier', onHoldNotifier)
  .factory('xcHelperPreferences', xcHelperPreferences)
  .controller('CallAction', CallAction)
  .controller('IncomingCallPopup', IncomingCallPopup)
  .controller('OnHoldPopup', OnHoldPopup)
  .controller('KeyboardPopup', KeyboardPopup);

