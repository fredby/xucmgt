angular.module('xcLogin').controller('FatalErrorController', function($scope, $state, $uibModalInstance, errorMessage, XucLink, CtiProxy) {
    $scope.errorMessage = errorMessage;

    var logout = function() {
        CtiProxy.stopUsingWebRtc();
        XucLink.logout();
        if(errorMessage === 'CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC') {
          $state.go('login', {error: 'LinkClosed'});
        } else {
          $state.go('login', {error: 'Logout'});
        }
    };

    $uibModalInstance.result.then(function (selectedItem) {
        logout();
    }, function () {
        logout();
    });

    $scope.close = function() {
        $uibModalInstance.close();
        logout();
    };
});

