(function() {
    'use strict';

    angular.module('xcLogin').factory('userRights', UserRights);

    function UserRights() {
        var ADMIN_ROLE_NAME = 'Admin';
        var SUPERVISOR_ROLE_NAME = 'Supervisor';

        var _allowCCManager= function(profile) {
            return (profile === ADMIN_ROLE_NAME || profile === SUPERVISOR_ROLE_NAME);
        };

        return {
            allowCCManager: _allowCCManager
        };
    }
})();

