package controllers

import configuration.CcConfig
import controllers.helpers.PrettyController
import javax.inject.Inject
import play.api.Logger


class CcManager @Inject()(config: CcConfig, prettyCtrl: PrettyController) extends MainController {
  override val log = Logger(getClass.getName)
  override val isAgent = false
  val title = "XiVO CC Manager"

  override def connect = Action { implicit request =>
    log.debug(s"New connection to CC Manager from ${request.remoteAddress}")
    Ok(prettyCtrl.prettify(views.html.ccmanager.ccmanager(title, config)(request.lang)))
  }

  def content = Action { implicit request =>
    Ok(prettyCtrl.prettify(views.html.ccmanager.managerContent(config)))
  }

  def login = Action { implicit request =>
    Ok(prettyCtrl.prettify(views.html.ccmanager.login()))
  }
}
